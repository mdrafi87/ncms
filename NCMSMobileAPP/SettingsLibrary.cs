using System;
using System.Web;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Globalization;
using System.Configuration;

namespace SettingsLibrary
{
    public static class SystemValues
    {
        public static string ConnectionString;
        public static string DatabaseServerName;
        public static string DatabaseName;
        public static string DatabaseUserID;
        public static string DatabaseUserPassword;
        public static DateFormats DateFormat = DateFormats.DD_MM_YYYY;
        public static string CompanyCode;
        public static string UserGroup;
        public static string UserID;
        public static string PINID;
        public static long sessionNumber;
        public static int AgeLimite;
        public static int CurrentYear;

        static SystemValues()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["DBConnectionString"].ToString();
        }


        public enum DateFormats        
        {
            None = 0,
            MM_DD_YYYY = 1,
            DD_MM_YYYY = 2,
            MMM_DD_YYYY = 3,
            DD_MMM_YYYY = 4
        }

        public static bool isValidDataset(ref DataSet dataset)
        {
            if (dataset == null)
            {
                return false;
            }
            if (dataset.Tables.Count <= 0)
            {
                return false;
            }
            if (dataset.Tables[0].Rows.Count <= 0)
            {
                return false;
            }
            return true;
        }

        public static bool isValidDatatable(ref DataTable dataTable)
        {
            if (dataTable == null)
            {
                return false;
            }

            if (dataTable.Rows.Count <= 0)
            {
                return false;
            }
            return true;
        }

        
        public static string GetCurrentPageName()
        {
            string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
            System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
            string sRet = oInfo.Name;
            return sRet;
        }

        public static string GetCurrentPagePath()
        {
            string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath  ;
            System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
            return sPath;
        }

        public static int Duration(string StrtDate, string EndDate)
        {

            DateTime dt1 = Convert.ToDateTime(StrtDate);
            DateTime dt2 = Convert.ToDateTime(EndDate);
            int days;

            if (dt2 < dt1)
            {
                return days = 0;
            }
            else
            {
                TimeSpan ts = dt2 - dt1;
                days = ts.Days +1 ;
                return days;
            }


        }

        public static bool sendEmail(string mailfrom,string mailTo,string Msg,string Subject)
        {
            bool returnCode = false;
            try
            {
                MailMessage MailMessage = new MailMessage(mailfrom, mailTo, Subject, Msg);
                MailMessage.IsBodyHtml = true;
                MailMessage.Priority = MailPriority.Normal;
                SmtpClient smtpclint = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SMTPServer"], 
                                                       Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["EmailPortNo"]));
                smtpclint.UseDefaultCredentials = true;               
                smtpclint.Send(MailMessage);
            }
            catch (Exception ex)
            {
                // Whatever
            }    
            return returnCode;
        }

        public static bool sendEmail(string mailfrom, string mailTo, string Msg, string Subject,string ccList)
        {
            bool returnCode = false;

            try
            {
                MailMessage MailMessage = new MailMessage(mailfrom, mailTo, Subject, Msg);
                MailMessage.IsBodyHtml = true;
                MailMessage.CC.Add(ccList);              
                MailMessage.Priority = MailPriority.Normal;
                SmtpClient smtpclint = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SMTPServer"],
                                             Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["EmailPortNo"]));
                smtpclint.UseDefaultCredentials = true;
                smtpclint.Send(MailMessage);
            }

            catch (Exception ex)
            {
                // Whatever
            }
          
            return returnCode;
        }


        public static bool ValidateDate(string val) 
        { 
            int year = 0; 
            int month = 0; 
            int day = 0; 
            string[] date;
            try 
            { date = val.Split('/'); if (date.Length != 3)			
                return (false);
                month = int.Parse(date[0]); 
                day = int.Parse(date[1]);
                year = int.Parse(date[2]); 
                if (!(year >= 0 && year <= 9999))		
                    return (false); 
                if (!(month >= 01 && month <= 12))			
                    return (false); if (!(day >= 01 && day <= 31))				
                        return (false); 
            } 
            
            
            catch (Exception ex)
            { 
                            return (false); }
            return (true); }


       


        public static List<string> removeDuplicates(List<string> input)
        {
            Dictionary<string, int> tmp = new Dictionary<string, int>();

            foreach (string s in input)
                tmp[s] = 1;

            return new List<string>(tmp.Keys);
        }

      
        public static int GetWeekNumber(DateTime dtPassed)
        {
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(dtPassed, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Saturday);
            return weekNum;
        }
        public static int GetWeekNumberOther(DateTime dtPassed)
        {
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(dtPassed, CalendarWeekRule.FirstFullWeek, DayOfWeek.Monday);
            return weekNum;
        }
       
    }

    
    

}
