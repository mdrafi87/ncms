﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using SettingsLibrary;
using LibraryCore.DAL;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saplin.Controls;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Data.SqlClient;

namespace NCMSMobileAPP
{    
    public partial class DefaultTest : System.Web.UI.Page
    {
        DataTable dtgridview = null;
        HiddenField hdnViewstate = new HiddenField();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                btnRefresh.Visible = false;
               try
               {
                   Session.Remove("UserDetails");
                   Session.Remove("RequestData");
                   DataSet ds = new DataSet();
                   DataTable dt = new DataTable();

                   DataColumn ID = new DataColumn("ID");
                   DataColumn first_name = new DataColumn("First_Name");
                   DataColumn Second_Name = new DataColumn("Second_Name");
                   DataColumn Third_Name = new DataColumn("Third_Name");
                   DataColumn last_Name = new DataColumn("Last_Name");
                   DataColumn EmailAddress = new DataColumn("EmailAddress");
                   DataColumn MobileNumber = new DataColumn("MobileNumber");
                   DataColumn scientific_qualification = new DataColumn("scientific_qualification");
                   DataColumn Gradscientific_qualification = new DataColumn("Gradscientific_qualification");
                   DataColumn lastcumulative = new DataColumn("lastcumulative");
                   DataColumn cumulative_other = new DataColumn("cumulative_other");
                   DataColumn GPA = new DataColumn("GPA");
                   DataColumn Formerscientific_qualification = new DataColumn("Formerscientific_qualification");
                   DataColumn FormerGradscientific_qualification = new DataColumn("FormerGradscientific_qualification");
                   DataColumn Formercumulative_other = new DataColumn("Formercumulative_other");
                   DataColumn FormerGPA = new DataColumn("FormerGPA");
                   DataColumn lastdegree = new DataColumn("lastdegree");
                   DataColumn formerdegree = new DataColumn("formerdegree");                   
                   DataColumn formercumulative = new DataColumn("formercumulative");
                   DataColumn CreatedOn = new DataColumn("CreatedOn");

                   dt.Columns.Add(ID);
                   dt.Columns.Add(first_name);
                   dt.Columns.Add(Second_Name);
                   dt.Columns.Add(Third_Name);
                   dt.Columns.Add(last_Name);
                   dt.Columns.Add(EmailAddress);
                   dt.Columns.Add(MobileNumber);
                   dt.Columns.Add(scientific_qualification);
                   dt.Columns.Add(Gradscientific_qualification);
                   dt.Columns.Add(lastcumulative);
                   dt.Columns.Add(cumulative_other);
                   dt.Columns.Add(GPA);
                   dt.Columns.Add(Formerscientific_qualification);
                   dt.Columns.Add(FormerGradscientific_qualification);
                   dt.Columns.Add(Formercumulative_other);
                   dt.Columns.Add(FormerGPA);
                   dt.Columns.Add(lastdegree);
                   dt.Columns.Add(formerdegree);
                   dt.Columns.Add(formercumulative);
                   dt.Columns.Add(CreatedOn);
                   ds.Tables.Add(dt);

                   Session["UserDetails"] = ds;
                   
                   gvwncms.DataSource = GetUserDetails();
                   gvwncms.DataBind();

                   if (gvwncms.Rows.Count > 0)
                   {
                       DropDownChckBoxBinding();                       
                       //fn = 0; mn = 0; ea = 0; ld = 0; sq = 0; gq = 0; co = 0; gpa = 0; fsq = 0; fgq = 0; fd = 0; lc = 0; fgq = 0;
                       fn = 0; ld = 0; sq = 0; ea = 0; gq = 0; gpa = 0; fsq = 0; fgq = 0; fd = 0; lc = 0; co = 0; mn = 0; fgpa = 0;
                       sn = 0; tn = 0; ln = 0; fc = 0;
                   }
               }
                catch(Exception ex)
               {
               }
            }
        }
        public void DropDownChckBoxBinding()
        {
            if (Session["RequestData"] == null)
            {
                gvwncms.DataSource = GetUserDetails();
                gvwncms.DataBind();
                dtgridview = (DataTable)Session["RequestData"];
            }
            else
            {
                dtgridview = (DataTable)Session["RequestData"];
            }
            int child = dtgridview.DefaultView.Count;
            lblRequestsCount.Text = child.ToString();


            DataTable dtfirst_Name = new DataTable();
            DataTable dtSecond_Name = new DataTable();
            DataTable dtThird_Name = new DataTable();
            DataTable dtlast_Name = new DataTable();
            DataTable dtmobile = new DataTable();
            DataTable dtEmail = new DataTable();
            DataTable dtDegree = new DataTable();
            DataTable dtSQ = new DataTable();           
            DataTable dtGq = new DataTable();
            DataTable dtlastcumulative = new DataTable();
            DataTable dtGpa = new DataTable();
            DataTable dtfgq = new DataTable();
            DataTable dtfsq = new DataTable();
            DataTable dtformercumulative = new DataTable();
            DataTable dtFormarDegree = new DataTable();            
            DataTable dtCreatedOn = new DataTable();
            DataTable dtfgpa = new DataTable();

            dtfirst_Name = dtgridview.DefaultView.ToTable(true, "First_Name");
            dtSecond_Name = dtgridview.DefaultView.ToTable(true, "Second_Name");
            dtThird_Name = dtgridview.DefaultView.ToTable(true, "Third_Name");
            dtlast_Name = dtgridview.DefaultView.ToTable(true, "Last_Name");
            dtmobile = dtgridview.DefaultView.ToTable(true, "MobileNumber");
            dtEmail = dtgridview.DefaultView.ToTable(true, "EmailAddress");
            dtDegree = dtgridview.DefaultView.ToTable(true, "lastdegree");
            dtSQ = dtgridview.DefaultView.ToTable(true, "scientific_qualification");            
            dtGq = dtgridview.DefaultView.ToTable(true, "Gradscientific_qualification");
            dtlastcumulative = dtgridview.DefaultView.ToTable(true, "lastcumulative");
            dtGpa = dtgridview.DefaultView.ToTable(true, "GPA");
            dtfsq = dtgridview.DefaultView.ToTable(true, "Formerscientific_qualification");
            dtfgq = dtgridview.DefaultView.ToTable(true, "FormerGradscientific_qualification");
            dtformercumulative = dtgridview.DefaultView.ToTable(true, "formercumulative");
            dtFormarDegree = dtgridview.DefaultView.ToTable(true, "formerdegree");            
            dtCreatedOn = dtgridview.DefaultView.ToTable(true, "CreatedOn");
            dtfgpa = dtgridview.DefaultView.ToTable(true, "FormerGPA");

            dtfirst_Name.DefaultView.Sort = "First_Name asc";
            dtSecond_Name.DefaultView.Sort = "Second_Name asc";
            dtThird_Name.DefaultView.Sort = "Third_Name asc";
            dtlast_Name.DefaultView.Sort = "Last_Name asc";
            dtmobile.DefaultView.Sort = "MobileNumber asc";
            dtEmail.DefaultView.Sort = "EmailAddress asc";
            dtDegree.DefaultView.Sort = "lastdegree asc";
            dtSQ.DefaultView.Sort = "scientific_qualification asc";            
            dtGq.DefaultView.Sort = "Gradscientific_qualification asc";
            dtlastcumulative.DefaultView.Sort = "lastcumulative asc";
            dtGpa.DefaultView.Sort = "GPA asc";
            dtfsq.DefaultView.Sort = "Formerscientific_qualification asc";
            dtfgq.DefaultView.Sort = "FormerGradscientific_qualification asc";
            dtformercumulative.DefaultView.Sort = "formercumulative asc";
            dtFormarDegree.DefaultView.Sort = "formerdegree asc";            
            dtCreatedOn.DefaultView.Sort = "CreatedOn asc";
            dtfgpa.DefaultView.Sort = "FormerGPA asc";

            //dropdown for fullName 1
            DropDownCheckBoxes first_Name = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFirstName");
            first_Name.DataSource = dtfirst_Name.DefaultView;
            first_Name.DataTextField = "First_Name";
            first_Name.DataBind();
            first_Name.Style.DropDownBoxBoxWidth = new Unit(160);
            first_Name.SelectedIndex = -1;

            //dropdown for second name 2
            DropDownCheckBoxes second_Name = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlSecondName");
            second_Name.DataSource = dtSecond_Name.DefaultView;
            second_Name.DataTextField = "Second_Name";
            second_Name.DataBind();
            second_Name.Style.DropDownBoxBoxWidth = new Unit(160);
            second_Name.SelectedIndex = -1;

            //dropdown for thirdname 3
            DropDownCheckBoxes Third_Name = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlThirdName");
            Third_Name.DataSource = dtThird_Name.DefaultView;
            Third_Name.DataTextField = "Third_Name";
            Third_Name.DataBind();
            Third_Name.Style.DropDownBoxBoxWidth = new Unit(160);
            Third_Name.SelectedIndex = -1;

            //dropdown for last_name 4
            DropDownCheckBoxes Last_Name = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlLastname");
            Last_Name.DataSource = dtlast_Name.DefaultView;
            Last_Name.DataTextField = "Last_Name";
            Last_Name.DataBind();
            Last_Name.Style.DropDownBoxBoxWidth = new Unit(160);
            Last_Name.SelectedIndex = -1;

            //dropdown for Mobile Number 5
            DropDownCheckBoxes MobileNumber = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlMobileNumber");
            MobileNumber.DataSource = dtmobile.DefaultView;
            MobileNumber.DataTextField = "MobileNumber";
            MobileNumber.DataBind();
            MobileNumber.Style.DropDownBoxBoxWidth = new Unit(160);
            MobileNumber.SelectedIndex = -1;

            //dropdown for Email 6
            DropDownCheckBoxes Email = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlEmail");
            Email.DataSource = dtEmail.DefaultView;
            Email.DataTextField = "EmailAddress";
            Email.DataBind();
            Email.Style.DropDownBoxBoxWidth = new Unit(160);
            Email.SelectedIndex = -1;

            //dropdown for lstdegree 7
            DropDownCheckBoxes lastDegree = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlDegree");
            lastDegree.DataSource = dtDegree.DefaultView;
            lastDegree.DataTextField = "lastdegree";
            lastDegree.DataBind();
            lastDegree.Style.DropDownBoxBoxWidth = new Unit(160);
            lastDegree.SelectedIndex = -1;

            //dropdown for Scientific Qualification 8
            DropDownCheckBoxes squalification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlSQ");
            squalification.DataSource = dtSQ.DefaultView;
            squalification.DataTextField = "scientific_qualification";
            squalification.DataBind();
            squalification.Style.DropDownBoxBoxWidth = new Unit(160);
            squalification.SelectedIndex = -1;           

            //dropdown for Gradscientific_qualification 9
            DropDownCheckBoxes Gqualification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlGQ");
            Gqualification.DataSource = dtGq.DefaultView;
            Gqualification.DataTextField = "Gradscientific_qualification";
            Gqualification.DataBind();
            Gqualification.Style.DropDownBoxBoxWidth = new Unit(160);
            Gqualification.SelectedIndex = -1;

            //dropdown for lastcumulative 10
            DropDownCheckBoxes lastcumulative = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddllastcumulative");
            lastcumulative.DataSource = dtlastcumulative.DefaultView;
            lastcumulative.DataTextField = "lastcumulative";
            lastcumulative.DataBind();
            lastcumulative.Style.DropDownBoxBoxWidth = new Unit(160);
            lastcumulative.SelectedIndex = -1;

            //dropdown for GPA
            DropDownCheckBoxes gpa = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlGpa");
            gpa.DataSource = dtGpa.DefaultView;
            gpa.DataTextField = "GPA";
            gpa.DataBind();
            gpa.Style.DropDownBoxBoxWidth = new Unit(160);
            gpa.SelectedIndex = -1;

            //for Formerscientific Qualification
            DropDownCheckBoxes FSQualification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlfsq");
            FSQualification.DataSource = dtfsq.DefaultView;
            FSQualification.DataTextField = "Formerscientific_qualification";
            FSQualification.DataBind();
            FSQualification.Style.DropDownBoxBoxWidth = new Unit(160);
            FSQualification.SelectedIndex = -1;

            //for FGQualificarion
            DropDownCheckBoxes FGQualification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlfgq");
            FGQualification.DataSource = dtfgq.DefaultView;
            FGQualification.DataTextField = "FormerGradscientific_qualification";
            FGQualification.DataBind();
            FGQualification.Style.DropDownBoxBoxWidth = new Unit(160);
            FGQualification.SelectedIndex = -1;

            //for former cumulative
            DropDownCheckBoxes formercumulative = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFormarcumulative");
            formercumulative.DataSource = dtformercumulative.DefaultView;
            formercumulative.DataTextField = "formercumulative";
            formercumulative.DataBind();
            formercumulative.Style.DropDownBoxBoxWidth = new Unit(160);
            formercumulative.SelectedIndex = -1;

            //for formar degree
            DropDownCheckBoxes formardegree = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFDegree");
            formardegree.DataSource = dtFormarDegree.DefaultView;
            formardegree.DataTextField = "formerdegree";
            formardegree.DataBind();
            formardegree.Style.DropDownBoxBoxWidth = new Unit(160);
            formardegree.SelectedIndex = -1;            

            //drop down on date
            DropDownCheckBoxes CreatedOn = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddldate");            
            CreatedOn.DataSource = dtCreatedOn.DefaultView;
            CreatedOn.DataTextField = "CreatedOn";
            CreatedOn.DataBind();
            CreatedOn.Style.DropDownBoxBoxWidth = new Unit(160);
            CreatedOn.SelectedIndex = -1;

            //dropdown for FormerGPA

            DropDownCheckBoxes FormerGPA = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFormerGPA");
            FormerGPA.DataSource = dtfgpa.DefaultView;
            FormerGPA.DataTextField = "FormerGPA";
            FormerGPA.DataBind();
            FormerGPA.Style.DropDownBoxBoxWidth = new Unit(160);
            FormerGPA.SelectedIndex = -1;
        }
        public DataView GetUserDetails()
        {
            gvwncms.DataSource = null;
            gvwncms.DataBind();

            DataSet xds1 = (DataSet)Session["UserDetails"];
            xds1.Tables[0].Rows.Clear();
            DataView dv = new DataView();
            DataSet dsCheck_Response = new DataSet();
            dsCheck_Response = DAL_User.getUserDetails();

            if (dsCheck_Response.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in dsCheck_Response.Tables[0].Rows)
                {
                    DataRow dr = xds1.Tables[0].NewRow();
                    dr["ID"] = row["ID"].ToString();
                    dr["First_Name"] = row["First_Name"].ToString();
                    dr["Second_Name"] = row["Second_Name"].ToString();
                    dr["Third_Name"] = row["Third_Name"].ToString();
                    dr["Last_Name"] = row["Last_Name"].ToString();
                    dr["EmailAddress"] = row["EmailAddress"].ToString();
                    dr["MobileNumber"] = row["MobileNumber"].ToString();
                    dr["scientific_qualification"] = row["scientific_qualification"].ToString();
                    dr["Gradscientific_qualification"] = row["Gradscientific_qualification"].ToString();
                    dr["lastcumulative"] = row["lastcumulative"].ToString();
                    dr["cumulative_other"] = row["cumulative_other"].ToString();
                    dr["GPA"] = row["GPA"].ToString();
                    dr["Formerscientific_qualification"] = row["Formerscientific_qualification"].ToString();
                    dr["FormerGradscientific_qualification"] = row["FormerGradscientific_qualification"].ToString();
                    dr["Formercumulative_other"] = row["Formercumulative_other"].ToString();
                    dr["FormerGPA"] = row["FormerGPA"].ToString();
                    dr["lastdegree"] = row["lastdegree"].ToString();
                    dr["formerdegree"] = row["formerdegree"].ToString();                   
                    dr["formercumulative"] = row["formercumulative"].ToString();
                    dr["CreatedOn"] = row["CreatedOn"].ToString();
                    xds1.Tables[0].Rows.Add(dr);                   
                }
            }
            Session["RequestData"] = xds1.Tables[0];

            if (ViewState["sortExpr"] != null)
            {
                dv = new DataView(xds1.Tables[0]);

                dv.Sort = (string)ViewState["sortExpr"];
            }
            else
                dv = xds1.Tables[0].DefaultView;           
            int dd = dv.Count;
            return dv;
        }
        public void ViewstateDropdown()
        {
            hdnViewstate.Value = "FromViewState";
        }
        string _filter = string.Empty; string _filter1 = string.Empty; string _filter2 = string.Empty; string _filter3 = string.Empty; string _filter4 = string.Empty;
        string _filter5 = string.Empty; string _filter6 = string.Empty; string _filter7 = string.Empty; string _filter8 = string.Empty; string _filter9 = string.Empty;
        string _filter10 = string.Empty; string _filter11 = string.Empty; string _filter12 = string.Empty;
        string _filter13 = string.Empty; string _filter14 = string.Empty; string _filter15 = string.Empty;
        string _filter16 = string.Empty;

        string display; string display1; string display2; string display3; string display4; string display5; string display6; string display7; string display8; string display9;
        string display10; string display11; string display12; string display13; string display14; string display15; string display16;

        static ArrayList _fullNameArr = new ArrayList();
        static ArrayList _second_name = new ArrayList();
        static ArrayList _Third_Name = new ArrayList();
        static ArrayList _last_Name = new ArrayList();
        static ArrayList _mobile = new ArrayList();
        static ArrayList _EmailArr = new ArrayList();
        static ArrayList _degreeArr = new ArrayList();
        static ArrayList _ScintificQualificationArr = new ArrayList();
        static ArrayList _GradscientificQualificationArr = new ArrayList();
        static ArrayList _lastcumulative = new ArrayList();
        static ArrayList _Gpa = new ArrayList();
        static ArrayList _FSQualification = new ArrayList();
        static ArrayList _FGQualification = new ArrayList();
        static ArrayList _formercumulative = new ArrayList();
        static ArrayList _formerGPA = new ArrayList();
        static ArrayList _formardegree = new ArrayList();       
        static ArrayList _CreatedOn = new ArrayList();
        
        static ArrayList _CategoryArrays = new ArrayList();
        static int fn = 0, ld = 0, sq = 0, ea = 0, gq = 0, mn = 0, gpa = 0, fsq = 0, fgq = 0, fd = 0, lc = 0, co = 0, fgpa = 0, sn = 0, tn = 0, ln = 0, fc = 0;
        OrderedDictionary dict = new OrderedDictionary();
        
        static string totalFilter;

        protected void fullName_IndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (hdnViewstate.Value == "FromViewState")
                {

                }
                else
                {       
                     string xyz = btnClick.Value;
                     if (xyz != "True")
                     {
                         StringBuilder str = new StringBuilder();
                         string _o = ",";
                         if (Session["Dictionary"] != null)
                         {
                             dict = (OrderedDictionary)Session["Dictionary"];
                         }
                         DropDownCheckBoxes ddlsender = (DropDownCheckBoxes)sender;
                         string s = ddlsender.Texts.SelectBoxCaption;
                         if (ddlsender.SelectedIndex != -1)
                         {
                             if (ddlsender.ValidationGroup == "p1")
                             {
                                 fn = 0; mn = 0; sn = 0; tn = 0; ln = 0; fc = 0;
                                 ld = 0; sq = 0; ea = 0; gq = 0; gpa = 0; fsq = 0; fgq = 0; fd = 0; lc = 0; co = 0; fgpa = 0; totalFilter = "";
                                 _fullNameArr.Clear(); _second_name.Clear(); _Third_Name.Clear(); _last_Name.Clear();
                                 _degreeArr.Clear(); _ScintificQualificationArr.Clear(); _formercumulative.Clear();
                                 _GradscientificQualificationArr.Clear(); _mobile.Clear(); _Gpa.Clear();
                                 _EmailArr.Clear(); _FSQualification.Clear(); _FGQualification.Clear();
                                 _formardegree.Clear(); _lastcumulative.Clear(); _CreatedOn.Clear();
                                 _formerGPA.Clear();
                                 //_CategoryArrays.Clear(); 

                                 DropDownCheckBoxes ddlFirstName = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFirstName");
                                 string _or = " OR ";
                                 string _and = " AND ";

                                 foreach (ListItem item in (ddlFirstName as ListControl).Items)
                                 {
                                     string i = item.Text;
                                     if (item.Selected)
                                     {
                                         _filter = _filter + _or + "First_Name Like '" + item.Text.Trim() + "'";
                                         if (_filter.StartsWith(_or))
                                         {
                                             _filter = _filter.Replace(_or, "");
                                             display = display + item.Text.Trim();
                                         }
                                         fn++;
                                         _fullNameArr.Add(item.Value);
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict[item.Text.Trim()] = "First_Name";
                                         }
                                         else
                                         {
                                             dict.Add(item.Text.Trim(), "First_Name");
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter)) _filter = "(" + _filter + ")" + _and;

                                 DropDownCheckBoxes ddlDegree = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlDegree");

                                 foreach (ListItem item in (ddlDegree as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter1 = _filter1 + _or + "lastdegree Like'" + item.Text.Trim() + "'";
                                         if (_filter1.StartsWith(_or))
                                         {
                                             _filter1 = _filter1.Replace(_or, "");
                                             display1 = display1 + item.Text.Trim();
                                         }
                                         ld++;
                                         _degreeArr.Add(item.Value);
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict[item.Text.Trim()] = "lastdegree";
                                         }
                                         else
                                         {
                                             dict.Add(item.Text.Trim(), "lastdegree");
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter1)) _filter1 = "(" + _filter1 + ")" + _and;
                                 //for Scientific Qualification
                                 DropDownCheckBoxes ddlScintificQualification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlSQ");

                                 foreach (ListItem item in (ddlScintificQualification as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter2 = _filter2 + _or + "scientific_qualification Like'" + item.Text.Trim() + "'";
                                         if (_filter2.StartsWith(_or))
                                         {
                                             _filter2 = _filter2.Replace(_or, "");
                                             display2 = display2 + item.Text.Trim();
                                         }
                                         sq++;
                                         _ScintificQualificationArr.Add(item.Value);
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict[item.Text.Trim()] = "scientific_qualification";
                                         }
                                         else
                                         {
                                             dict.Add(item.Text.Trim(), "scientific_qualification");
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter2)) _filter2 = "(" + _filter2 + ")" + _and;

                                 //for Email Address
                                 DropDownCheckBoxes ddlEmail = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlEmail");

                                 foreach (ListItem item in (ddlEmail as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter3 = _filter3 + _or + "EmailAddress Like'" + item.Text.Trim() + "'";
                                         if (_filter3.StartsWith(_or))
                                         {
                                             _filter3 = _filter3.Replace(_or, "");
                                             display3 = display3 + item.Text.Trim();
                                         }
                                         ea++;
                                         _EmailArr.Add(item.Value);
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict[item.Text.Trim()] = "EmailAddress";
                                         }
                                         else
                                         {
                                             dict.Add(item.Text.Trim(), "EmailAddress");
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter3)) _filter3 = "(" + _filter3 + ")" + _and;
                                 //
                                 //for GQualification
                                 DropDownCheckBoxes ddlGqualification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlGQ");

                                 foreach (ListItem item in (ddlGqualification as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter4 = _filter4 + _or + "Gradscientific_qualification Like'" + item.Text.Trim() + "'";
                                         if (_filter4.StartsWith(_or))
                                         {
                                             _filter4 = _filter4.Replace(_or, "");
                                             display4 = display4 + item.Text.Trim();
                                         }
                                         gq++;
                                         _GradscientificQualificationArr.Add(item.Value);
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict[item.Text.Trim()] = "Gradscientific_qualification";
                                         }
                                         else
                                         {
                                             dict.Add(item.Text.Trim(), "Gradscientific_qualification");
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter4)) _filter4 = "(" + _filter4 + ")" + _and;
                                 //end

                                 //mobile
                                 DropDownCheckBoxes ddlMobile = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlMobileNumber");

                                 foreach (ListItem item in (ddlMobile as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter5 = _filter5 + _or + "MobileNumber Like'" + item.Text.Trim() + "'";
                                         if (_filter5.StartsWith(_or))
                                         {
                                             _filter5 = _filter5.Replace(_or, "");
                                             display5 = display5 + item.Text.Trim();
                                         }
                                         mn++;
                                         _mobile.Add(item.Value);
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict[item.Text.Trim()] = "MobileNumber";
                                         }
                                         else
                                         {
                                             dict.Add(item.Text.Trim(), "MobileNumber");
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter5)) _filter5 = "(" + _filter5 + ")" + _and;

                                 //end

                                 //gpa

                                 DropDownCheckBoxes ddlGpa = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlGpa");

                                 foreach (ListItem item in (ddlGpa as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter6 = _filter6 + _or + "GPA Like'" + item.Text.Trim() + "'";
                                         if (_filter6.StartsWith(_or))
                                         {
                                             _filter6 = _filter6.Replace(_or, "");
                                             display6 = display6 + item.Text.Trim();
                                         }
                                         gpa++;
                                         _Gpa.Add(item.Value);
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict[item.Text.Trim()] = "GPA";
                                         }
                                         else
                                         {
                                             dict.Add(item.Text.Trim(), "GPA");
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter6)) _filter6 = "(" + _filter6 + ")" + _and;
                                 //end
                                 //fsq
                                 DropDownCheckBoxes ddlfsq = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlfsq");

                                 foreach (ListItem item in (ddlfsq as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter7 = _filter7 + _or + "Formerscientific_qualification Like'" + item.Text.Trim() + "'";
                                         if (_filter7.StartsWith(_or))
                                         {
                                             _filter7 = _filter7.Replace(_or, "");
                                             display7 = display7 + item.Text.Trim();
                                         }
                                         fsq++;
                                         _FSQualification.Add(item.Value);
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict[item.Text.Trim()] = "Formerscientific_qualification";
                                         }
                                         else
                                         {
                                             dict.Add(item.Text.Trim(), "Formerscientific_qualification");
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter7)) _filter7 = "(" + _filter7 + ")" + _and;
                                 //end

                                 //fgq
                                 DropDownCheckBoxes ddlfgq = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlfgq");

                                 foreach (ListItem item in (ddlfgq as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter8 = _filter8 + _or + "FormerGradscientific_qualification Like'" + item.Text.Trim() + "'";
                                         if (_filter8.StartsWith(_or))
                                         {
                                             _filter8 = _filter8.Replace(_or, "");
                                             display8 = display8 + item.Text.Trim();
                                         }
                                         fgq++;
                                         _FGQualification.Add(item.Value);
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict[item.Text.Trim()] = "FormerGradscientific_qualification";
                                         }
                                         else
                                         {
                                             dict.Add(item.Text.Trim(), "FormerGradscientific_qualification");
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter8)) _filter8 = "(" + _filter8 + ")" + _and;
                                 //end
                                 //for formardegree
                                 DropDownCheckBoxes ddlformardegree = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFDegree");

                                 foreach (ListItem item in (ddlformardegree as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter9 = _filter9 + _or + "formerdegree Like'" + item.Text.Trim() + "'";
                                         if (_filter9.StartsWith(_or))
                                         {
                                             _filter9 = _filter9.Replace(_or, "");
                                             display9 = display9 + item.Text.Trim();
                                         }
                                         fd++;
                                         _formardegree.Add(item.Value);
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict[item.Text.Trim()] = "formerdegree";
                                         }
                                         else
                                         {
                                             dict.Add(item.Text.Trim(), "formerdegree");
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter9)) _filter9 = "(" + _filter9 + ")" + _and;
                                 //end
                                 //for cumulative degree
                                 DropDownCheckBoxes ddlCumumulativedegree = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddllastcumulative");

                                 foreach (ListItem item in (ddlCumumulativedegree as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter10 = _filter10 + _or + "lastcumulative Like'" + item.Text.Trim() + "'";
                                         if (_filter10.StartsWith(_or))
                                         {
                                             _filter10 = _filter10.Replace(_or, "");
                                             display10 = display10 + item.Text.Trim();
                                         }
                                         lc++;
                                         _lastcumulative.Add(item.Value);
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict[item.Text.Trim()] = "lastcumulative";
                                         }
                                         else
                                         {
                                             dict.Add(item.Text.Trim(), "lastcumulative");
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter10)) _filter10 = "(" + _filter10 + ")" + _and;
                                 //end
                                 //for Date
                                 DropDownCheckBoxes ddlDate = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddldate");

                                 foreach (ListItem item in (ddlDate as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter11 = _filter11 + _or + "CreatedOn Like'" + String.Format("{0:dd/Mm/yyyy}", item.Text.Trim()) + "'";
                                         if (_filter11.StartsWith(_or))
                                         {
                                             _filter11 = _filter11.Replace(_or, "");
                                             display11 = display11 + item.Text.Trim();
                                         }
                                         co++;
                                         _CreatedOn.Add(item.Value);
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict[item.Text.Trim()] = "CreatedOn";
                                         }
                                         else
                                         {
                                             dict.Add(item.Text.Trim(), "CreatedOn");
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter11)) _filter11 = "(" + _filter11 + ")" + _and;
                                 //end
                                 //former GPA
                                 DropDownCheckBoxes ddlFormerGPA = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFormerGPA");
                                 foreach (ListItem item in (ddlFormerGPA as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter12 = _filter12 + _or + "FormerGPA Like'" + item.Text.Trim() + "'";
                                         if (_filter12.StartsWith(_or))
                                         {
                                             _filter12 = _filter12.Replace(_or, "");
                                             display12 = display12 + item.Text.Trim();
                                         }
                                         fgpa++;
                                         _formerGPA.Add(item.Value);
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict[item.Text.Trim()] = "FormerGPA";
                                         }
                                         else
                                         {
                                             dict.Add(item.Text.Trim(), "FormerGPA");
                                         }
                                     }

                                 }
                                 if (!string.IsNullOrEmpty(_filter12)) _filter12 = "(" + _filter12 + ")" + _and;
                                 //end
                                 //second_name
                                 DropDownCheckBoxes ddlSecondName = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlSecondName");

                                 foreach (ListItem item in (ddlSecondName as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter13 = _filter13 + _or + "Second_Name Like'" + item.Text.Trim() + "'";
                                         if (_filter13.StartsWith(_or))
                                         {
                                             _filter13 = _filter13.Replace(_or, "");
                                             display13 = display13 + item.Text.Trim();
                                         }
                                         sn++;
                                         _second_name.Add(item.Value);
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict[item.Text.Trim()] = "Second_Name";
                                         }
                                         else
                                         {
                                             dict.Add(item.Text.Trim(), "Second_Name");
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter13)) _filter13 = "(" + _filter13 + ")" + _and;
                                 //end
                                 //third_name
                                 DropDownCheckBoxes ddlThirdName = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlThirdName");

                                 foreach (ListItem item in (ddlThirdName as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter14 = _filter14 + _or + "Third_Name Like'" + item.Text.Trim() + "'";
                                         if (_filter14.StartsWith(_or))
                                         {
                                             _filter14 = _filter14.Replace(_or, "");
                                             display14 = display14 + item.Text.Trim();
                                         }
                                         tn++;
                                         _Third_Name.Add(item.Value);
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict[item.Text.Trim()] = "Third_Name";
                                         }
                                         else
                                         {
                                             dict.Add(item.Text.Trim(), "Third_Name");
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter14)) _filter14 = "(" + _filter14 + ")" + _and;
                                 //end
                                 //last_name
                                 DropDownCheckBoxes ddlLastname = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlLastname");

                                 foreach (ListItem item in (ddlLastname as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter15 = _filter15 + _or + "Last_Name Like'" + item.Text.Trim() + "'";
                                         if (_filter15.StartsWith(_or))
                                         {
                                             _filter15 = _filter15.Replace(_or, "");
                                             display15 = display15 + item.Text.Trim();
                                         }
                                         ln++;
                                         _last_Name.Add(item.Value);
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict[item.Text.Trim()] = "Last_Name";
                                         }
                                         else
                                         {
                                             dict.Add(item.Text.Trim(), "Last_Name");
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter15)) _filter15 = "(" + _filter15 + ")" + _and;
                                 //end
                                 //former cumulative
                                 DropDownCheckBoxes ddlFormarcumulative = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFormarcumulative");

                                 foreach (ListItem item in (ddlFormarcumulative as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter16 = _filter16 + _or + "formercumulative Like'" + item.Text.Trim() + "'";
                                         if (_filter16.StartsWith(_or))
                                         {
                                             _filter16 = _filter16.Replace(_or, "");
                                             display16 = display16 + item.Text.Trim();
                                         }
                                         fc++;
                                         _formercumulative.Add(item.Value);
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict[item.Text.Trim()] = "formercumulative";
                                         }
                                         else
                                         {
                                             dict.Add(item.Text.Trim(), "formercumulative");
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter16)) _filter16 = "(" + _filter16 + ")" + _and;
                                 //end
                                 Session["Dictionary"] = dict;
                                 totalFilter = (_filter) + (_filter1) + (_filter2) + (_filter3) + (_filter4) + (_filter5) + (_filter6) + (_filter7) + (_filter8) + (_filter9) + (_filter10) + (_filter11) + (_filter12) + (_filter13) + (_filter14) + (_filter15) + (_filter16);
                                 if (totalFilter.EndsWith(_and)) totalFilter = totalFilter.Substring(0, totalFilter.Length - _and.Length);
                                 lblfilterData.ForeColor = System.Drawing.Color.Black;
                                 lblfilterData.Font.Bold = true;
                                 //if (Session["UserDetails"] == null)
                                 //{
                                 //    //gvwncms.DataSource = dtgridview;
                                 //    //gvwncms.DataBind();
                                 //    GetUserDetails();
                                 //    dtgridview = (DataTable)Session["UserDetails"];
                                 //}
                                 //else
                                 //{
                                 //    dtgridview = (DataTable)Session["UserDetails"];
                                 //}
                                 if (Session["RequestData"] == null)
                                 {
                                     gvwncms.DataSource = GetUserDetails();
                                     gvwncms.DataBind();
                                     dtgridview = (DataTable)Session["RequestData"];
                                 }
                                 else
                                 {
                                     dtgridview = (DataTable)Session["RequestData"];
                                 }
                                 dtgridview.DefaultView.RowFilter = totalFilter;

                                 int child = dtgridview.DefaultView.Count;
                                 if (child == 0)
                                 {
                                     string comment = "(Request filter data not found...!)";
                                     lblfilterData.Text = display + display1 + display2 + display3 + display4 + display5 + display6 + display7 + display8 + display9 + display10 + display11 + display12 + display13 + display14 + display15 + display16 + comment;
                                     lblfilterData.ForeColor = System.Drawing.Color.Red;
                                 }
                                 else
                                 {
                                     gvwncms.DataSource = dtgridview.DefaultView;
                                     gvwncms.DataBind();
                                 }
                                 if (gvwncms.Rows.Count > 0)
                                 {
                                     UpdateDropDownChckBoxBinding();
                                 }
                                 ViewstateDropdown();
                             }
                         }
                         else
                         {
                             if (fn == 0 && ld == 0 && sq == 0 && ea == 0 && gq == 0 && mn == 0 && gpa == 0 && fsq == 0 && fgq == 0 && fd == 0 && lc == 0 && co == 0 && fgpa == 0 && sn == 0 && tn == 0 && ln == 0 && fc==0)
                             {
                                 Response.Redirect("DefaultTest.aspx", false);
                             }
                             else
                             {
                                 fn = 0; sn = 0; tn = 0; ln = 0; fc = 0;
                                 ld = 0; sq = 0; ea = 0; gq = 0; gpa = 0; fsq = 0; fgq = 0; fd = 0; lc = 0; co = 0; fgpa = 0; totalFilter = "";
                                 _fullNameArr.Clear(); _second_name.Clear(); _Third_Name.Clear(); _last_Name.Clear(); _formercumulative.Clear();
                                 _degreeArr.Clear(); _CategoryArrays.Clear(); _ScintificQualificationArr.Clear();
                                 _GradscientificQualificationArr.Clear(); _mobile.Clear(); _Gpa.Clear();
                                 _EmailArr.Clear(); _FSQualification.Clear(); _FGQualification.Clear();
                                 _formardegree.Clear(); _lastcumulative.Clear(); _CreatedOn.Clear();
                                 _formerGPA.Clear();

                                 string _or = " OR ";
                                 string _and = " AND ";

                                 DropDownCheckBoxes ddlFirstName = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFirstName");
                                 foreach (ListItem item in (ddlFirstName as ListControl).Items)
                                 {
                                     string i = item.Text;
                                     if (item.Selected)
                                     {
                                         _filter = _filter + _or + "First_Name Like '" + item.Text.Trim() + "'";
                                         if (_filter.StartsWith(_or))
                                         {
                                             _filter = _filter.Replace(_or, "");
                                             display = display + item.Text.Trim();
                                         }
                                         fn++;
                                         _fullNameArr.Add(item.Value);
                                     }
                                     else
                                     {
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict.Remove(item.Text.Trim());
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter)) _filter = "(" + _filter + ")" + _and;

                                 DropDownCheckBoxes ddlDegree = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlDegree");

                                 foreach (ListItem item in (ddlDegree as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter1 = _filter1 + _or + "lastdegree Like'" + item.Text.Trim() + "'";
                                         if (_filter1.StartsWith(_or))
                                         {
                                             _filter1 = _filter1.Replace(_or, "");
                                             display1 = display1 + item.Text.Trim();
                                         }
                                         ld++;
                                         _degreeArr.Add(item.Value);
                                     }
                                     else
                                     {
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict.Remove(item.Text.Trim());
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter1)) _filter1 = "(" + _filter1 + ")" + _and;

                                 //for Scientific Qualifiucation

                                 DropDownCheckBoxes ddlScintificQualification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlSQ");

                                 foreach (ListItem item in (ddlScintificQualification as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter2 = _filter2 + _or + "scientific_qualification Like'" + item.Text.Trim() + "'";
                                         if (_filter2.StartsWith(_or))
                                         {
                                             _filter2 = _filter2.Replace(_or, "");
                                             display2 = display2 + item.Text.Trim();
                                         }
                                         sq++;
                                         _ScintificQualificationArr.Add(item.Value);
                                     }
                                     else
                                     {
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict.Remove(item.Text.Trim());
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter2)) _filter2 = "(" + _filter2 + ")" + _and;

                                 //end

                                 //for Email address
                                 DropDownCheckBoxes ddlEmail = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlEmail");

                                 foreach (ListItem item in (ddlEmail as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter3 = _filter3 + _or + "EmailAddress Like'" + item.Text.Trim() + "'";
                                         if (_filter3.StartsWith(_or))
                                         {
                                             _filter3 = _filter3.Replace(_or, "");
                                             display3 = display3 + item.Text.Trim();
                                         }
                                         ea++;
                                         _EmailArr.Add(item.Value);
                                     }
                                     else
                                     {
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict.Remove(item.Text.Trim());
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter3)) _filter3 = "(" + _filter3 + ")" + _and;
                                 //end

                                 //gor GQualification
                                 DropDownCheckBoxes ddlGqualification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlGQ");

                                 foreach (ListItem item in (ddlGqualification as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter4 = _filter4 + _or + "Gradscientific_qualification Like'" + item.Text.Trim() + "'";
                                         if (_filter4.StartsWith(_or))
                                         {
                                             _filter4 = _filter4.Replace(_or, "");
                                             display4 = display4 + item.Text.Trim();
                                         }
                                         gq++;
                                         _GradscientificQualificationArr.Add(item.Value);
                                     }
                                     else
                                     {
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict.Remove(item.Text.Trim());
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter4)) _filter4 = "(" + _filter4 + ")" + _and;
                                 //end

                                 //mobile
                                 DropDownCheckBoxes ddlMobile = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlMobileNumber");

                                 foreach (ListItem item in (ddlMobile as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter5 = _filter5 + _or + "MobileNumber Like'" + item.Text.Trim() + "'";
                                         if (_filter5.StartsWith(_or))
                                         {
                                             _filter5 = _filter5.Replace(_or, "");
                                             display5 = display5 + item.Text.Trim();
                                         }
                                         mn++;
                                         _mobile.Add(item.Value);
                                     }
                                     else
                                     {
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict.Remove(item.Text.Trim());
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter5)) _filter5 = "(" + _filter5 + ")" + _and;
                                 //end

                                 //gpa
                                 DropDownCheckBoxes ddlGpa = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlGpa");

                                 foreach (ListItem item in (ddlGpa as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter6 = _filter6 + _or + "GPA Like'" + item.Text.Trim() + "'";
                                         if (_filter6.StartsWith(_or))
                                         {
                                             _filter6 = _filter6.Replace(_or, "");
                                             display6 = display6 + item.Text.Trim();
                                         }
                                         gpa++;
                                         _Gpa.Add(item.Value);
                                     }
                                     else
                                     {
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict.Remove(item.Text.Trim());
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter6)) _filter6 = "(" + _filter6 + ")" + _and;
                                 //end
                                 //for fsq
                                 DropDownCheckBoxes ddlfsq = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlfsq");

                                 foreach (ListItem item in (ddlfsq as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter7 = _filter7 + _or + "Formerscientific_qualification Like'" + item.Text.Trim() + "'";
                                         if (_filter7.StartsWith(_or))
                                         {
                                             _filter7 = _filter7.Replace(_or, "");
                                             display7 = display7 + item.Text.Trim();
                                         }
                                         fsq++;
                                         _FSQualification.Add(item.Value);
                                     }
                                     else
                                     {
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict.Remove(item.Text.Trim());
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter7)) _filter7 = "(" + _filter7 + ")" + _and;
                                 //end
                                 //for fgq
                                 DropDownCheckBoxes ddlfgq = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlfgq");

                                 foreach (ListItem item in (ddlfgq as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter8 = _filter8 + _or + "FormerGradscientific_qualification Like'" + item.Text.Trim() + "'";
                                         if (_filter8.StartsWith(_or))
                                         {
                                             _filter8 = _filter8.Replace(_or, "");
                                             display8 = display8 + item.Text.Trim();
                                         }
                                         fgq++;
                                         _FGQualification.Add(item.Value);
                                     }
                                     else
                                     {
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict.Remove(item.Text.Trim());
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter8)) _filter8 = "(" + _filter8 + ")" + _and;
                                 //end
                                 //for formar degree
                                 DropDownCheckBoxes ddlFormardegree = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFDegree");

                                 foreach (ListItem item in (ddlFormardegree as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter9 = _filter9 + _or + "formerdegree Like'" + item.Text.Trim() + "'";
                                         if (_filter9.StartsWith(_or))
                                         {
                                             _filter9 = _filter9.Replace(_or, "");
                                             display9 = display9 + item.Text.Trim();
                                         }
                                         fd++;
                                         _formardegree.Add(item.Value);
                                     }
                                     else
                                     {
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict.Remove(item.Text.Trim());
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter9)) _filter9 = "(" + _filter9 + ")" + _and;
                                 //end
                                 //last cumulative
                                 DropDownCheckBoxes ddllastcumulative = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddllastcumulative");

                                 foreach (ListItem item in (ddllastcumulative as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter10 = _filter10 + _or + "lastcumulative Like'" + item.Text.Trim() + "'";
                                         if (_filter10.StartsWith(_or))
                                         {
                                             _filter10 = _filter10.Replace(_or, "");
                                             display10 = display10 + item.Text.Trim();
                                         }
                                         lc++;
                                         _lastcumulative.Add(item.Value);
                                     }
                                     else
                                     {
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict.Remove(item.Text.Trim());
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter10)) _filter10 = "(" + _filter10 + ")" + _and;
                                 //end
                                 //created on
                                 DropDownCheckBoxes ddlDate = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddldate");

                                 foreach (ListItem item in (ddlDate as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter11 = _filter11 + _or + "CreatedOn Like'" + String.Format("{0:dd/MM/yyyy}", item.Text.Trim()) + "'";
                                         if (_filter11.StartsWith(_or))
                                         {
                                             _filter11 = _filter11.Replace(_or, "");
                                             display11 = display11 + item.Text.Trim();
                                         }
                                         co++;
                                         _CreatedOn.Add(item.Value);
                                     }
                                     else
                                     {
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict.Remove(item.Text.Trim());
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter11)) _filter11 = "(" + _filter11 + ")" + _and;
                                 //end
                                 //for formerGPA

                                 DropDownCheckBoxes ddlFormerGPA = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFormerGPA");
                                 foreach (ListItem item in (ddlFormerGPA as ListControl).Items)
                                 {
                                     if (item.Selected)
                                     {
                                         _filter12 = _filter12 + _or + "FormerGPA Like'" + item.Text.Trim() + "'";
                                         if (_filter12.StartsWith(_or))
                                         {
                                             _filter12 = _filter12.Replace(_or, "");
                                             display12 = display12 + item.Text.Trim();
                                         }
                                         ++fgpa;
                                         _formerGPA.Add(item.Value);
                                     }
                                     else
                                     {
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict.Remove(item.Text.Trim());
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter12)) _filter12 = "(" + _filter12 + ")" + _and;
                                 //end
                                 //SecondName
                                 DropDownCheckBoxes ddlSecondName = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlSecondName");
                                 foreach (ListItem item in (ddlSecondName as ListControl).Items)
                                 {
                                     string i = item.Text;
                                     if (item.Selected)
                                     {
                                         _filter13 = _filter13 + _or + "Second_Name Like '" + item.Text.Trim() + "'";
                                         if (_filter13.StartsWith(_or))
                                         {
                                             _filter13 = _filter13.Replace(_or, "");
                                             display13 = display13 + item.Text.Trim();
                                         }
                                         sn++;
                                         _second_name.Add(item.Value);
                                     }
                                     else
                                     {
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict.Remove(item.Text.Trim());
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter13)) _filter13 = "(" + _filter13 + ")" + _and;
                                 //end
                                 //third_name
                                 DropDownCheckBoxes ddlThirdName = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlThirdName");
                                 foreach (ListItem item in (ddlThirdName as ListControl).Items)
                                 {
                                     string i = item.Text;
                                     if (item.Selected)
                                     {
                                         _filter14 = _filter14 + _or + "Third_Name Like '" + item.Text.Trim() + "'";
                                         if (_filter14.StartsWith(_or))
                                         {
                                             _filter14 = _filter14.Replace(_or, "");
                                             display14 = display14 + item.Text.Trim();
                                         }
                                         tn++;
                                         _Third_Name.Add(item.Value);
                                     }
                                     else
                                     {
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict.Remove(item.Text.Trim());
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter14)) _filter14 = "(" + _filter14 + ")" + _and;
                                 //end
                                 //last_name
                                 DropDownCheckBoxes ddlLastname = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlLastname");
                                 foreach (ListItem item in (ddlLastname as ListControl).Items)
                                 {
                                     string i = item.Text;
                                     if (item.Selected)
                                     {
                                         _filter15 = _filter15 + _or + "Last_Name Like '" + item.Text.Trim() + "'";
                                         if (_filter15.StartsWith(_or))
                                         {
                                             _filter15 = _filter15.Replace(_or, "");
                                             display15 = display15 + item.Text.Trim();
                                         }
                                         ln++;
                                         _last_Name.Add(item.Value);
                                     }
                                     else
                                     {
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict.Remove(item.Text.Trim());
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter15)) _filter15 = "(" + _filter15 + ")" + _and;
                                 //end
                                 //former cumulative
                                 DropDownCheckBoxes ddlFormarcumulative = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFormarcumulative");
                                 foreach (ListItem item in (ddlFormarcumulative as ListControl).Items)
                                 {
                                     string i = item.Text;
                                     if (item.Selected)
                                     {
                                         _filter16 = _filter16 + _or + "formercumulative Like '" + item.Text.Trim() + "'";
                                         if (_filter16.StartsWith(_or))
                                         {
                                             _filter16 = _filter16.Replace(_or, "");
                                             display16 = display16 + item.Text.Trim();
                                         }
                                         fc++;
                                         _formercumulative.Add(item.Value);
                                     }
                                     else
                                     {
                                         if (dict.Contains(item.Text.Trim()))
                                         {
                                             dict.Remove(item.Text.Trim());
                                         }
                                     }
                                 }
                                 if (!string.IsNullOrEmpty(_filter16)) _filter16 = "(" + _filter16 + ")" + _and;
                                 //end
                                 Session["Dictionary"] = dict;
                                 totalFilter = (_filter) + (_filter1) + (_filter2) + (_filter3) + (_filter4) + (_filter5) + (_filter6) + (_filter7) + (_filter8) + (_filter9) + (_filter10) + (_filter11) + (_filter12) + (_filter13) + (_filter14) + (_filter15) + (_filter16);
                                 if (totalFilter.EndsWith(_and)) totalFilter = totalFilter.Substring(0, totalFilter.Length - _and.Length);
                                 lblfilterData.ForeColor = System.Drawing.Color.Black;
                                 lblfilterData.Font.Bold = true;

                                 //if (Session["UserDetails"] == null)
                                 //{
                                 //    //GetUserDetails();
                                 //    //dtgridview = (DataTable)Session["UserDetails"];

                                 //    gvwncms.DataSource = GetUserDetails();
                                 //    gvwncms.DataBind();
                                 //    dtgridview = (DataTable)Session["UserDetails"];
                                 //}
                                 //else
                                 //{
                                 //    dtgridview = (DataTable)Session["UserDetails"];
                                 //}
                                 if (Session["RequestData"] == null)
                                 {
                                     gvwncms.DataSource = GetUserDetails();
                                     gvwncms.DataBind();
                                     dtgridview = (DataTable)Session["RequestData"];
                                 }
                                 else
                                 {
                                     dtgridview = (DataTable)Session["RequestData"];
                                 }
                                 dtgridview.DefaultView.RowFilter = totalFilter;

                                 int child = dtgridview.DefaultView.Count;
                                 if (child == 0)
                                 {
                                     string comment = "(Requested Filter Data Not Found)";
                                     lblfilterData.Text = display + display1 + display2 + display3 + display4 + display5 + display6 + display7 + display8 + display9 + display10 + display11 + display12 + display13 + display14 + display15 + display16 + comment;
                                     lblfilterData.ForeColor = System.Drawing.Color.Red;
                                 }
                                 else
                                 {
                                     gvwncms.DataSource = dtgridview.DefaultView;
                                     gvwncms.DataBind();
                                 }
                                 if (gvwncms.Rows.Count > 0)
                                 {
                                     UpdateDropDownChckBoxBinding();
                                 }
                                 ViewstateDropdown();
                             }
                         }
                     }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void UpdateDropDownChckBoxBinding()
        {
            if (Session["Dictionary"] != null)
            {
                dict = (OrderedDictionary)Session["Dictionary"];
            }
            if (Session["RequestData"] == null)
            {
                gvwncms.DataSource = dtgridview;
                gvwncms.DataBind();
                dtgridview = (DataTable)Session["RequestData"];
            }
            else
            {
                dtgridview = (DataTable)Session["RequestData"];
            }
            //ddl fullname
            DropDownCheckBoxes first_name = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFirstName");
            DataTable dtfirst_Name = dtgridview.DefaultView.ToTable(true, "First_Name");
            dtfirst_Name.DefaultView.Sort = "First_Name asc";
            first_name.DataSource = dtfirst_Name.DefaultView;
            ViewState["fnType"] = dtfirst_Name;
            first_name.DataTextField = "First_Name";
            first_name.DataBind();
            first_name.Style.DropDownBoxBoxWidth = new Unit(160);

            //ddl secondname
            DropDownCheckBoxes Second_Name = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlSecondName");
            DataTable dtSecond_Name = dtgridview.DefaultView.ToTable(true, "Second_Name");
            dtSecond_Name.DefaultView.Sort = "Second_Name asc";
            Second_Name.DataSource = dtSecond_Name.DefaultView;
            ViewState["fnType"] = dtSecond_Name;
            Second_Name.DataTextField = "Second_Name";
            Second_Name.DataBind();
            Second_Name.Style.DropDownBoxBoxWidth = new Unit(160);

            //ddl third_Name
            DropDownCheckBoxes Third_Name = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlThirdName");
            DataTable dtThird_Name = dtgridview.DefaultView.ToTable(true, "Third_Name");
            dtThird_Name.DefaultView.Sort = "Third_Name asc";
            Third_Name.DataSource = dtThird_Name.DefaultView;
            ViewState["fnType"] = dtThird_Name;
            Third_Name.DataTextField = "Third_Name";
            Third_Name.DataBind();
            Third_Name.Style.DropDownBoxBoxWidth = new Unit(160);

            //ddl last name
            DropDownCheckBoxes last_Name = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlLastname");
            DataTable dtlast_Name = dtgridview.DefaultView.ToTable(true, "Last_Name");
            dtlast_Name.DefaultView.Sort = "Last_Name asc";
            last_Name.DataSource = dtlast_Name.DefaultView;
            ViewState["fnType"] = dtlast_Name;
            last_Name.DataTextField = "Last_Name";
            last_Name.DataBind();
            last_Name.Style.DropDownBoxBoxWidth = new Unit(160);


            //ddl degree
            DropDownCheckBoxes Degree = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlDegree");
            DataTable dtDegree = dtgridview.DefaultView.ToTable(true, "lastdegree");
            dtDegree.DefaultView.Sort = "lastdegree asc";
            Degree.DataSource = dtDegree.DefaultView;
            ViewState["ldType"] = dtDegree;
            Degree.DataTextField = "lastdegree";
            Degree.DataBind();
            Degree.Style.DropDownBoxBoxWidth = new Unit(160);

            //ddl scintific Qualification
            DropDownCheckBoxes ScintificQualification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlSQ");
            DataTable dtSq = dtgridview.DefaultView.ToTable(true, "scientific_qualification");
            dtSq.DefaultView.Sort = "scientific_qualification asc";
            ScintificQualification.DataSource = dtSq.DefaultView;
            ViewState["SqType"] = dtSq;
            ScintificQualification.DataTextField = "scientific_qualification";
            ScintificQualification.DataBind();
            ScintificQualification.Style.DropDownBoxBoxWidth = new Unit(160);

            //for Email Address
            DropDownCheckBoxes Email = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlEmail");
            DataTable dtEmail = dtgridview.DefaultView.ToTable(true, "EmailAddress");
            dtEmail.DefaultView.Sort = "EmailAddress asc";
            Email.DataSource = dtEmail.DefaultView;
            ViewState["EAType"] = dtEmail;
            Email.DataTextField = "EmailAddress";
            Email.DataBind();
            Email.Style.DropDownBoxBoxWidth = new Unit(160);

            //for Gradscientific_qualification
            DropDownCheckBoxes GradscientificQualification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlGQ");
            DataTable dtGq = dtgridview.DefaultView.ToTable(true, "Gradscientific_qualification");
            dtGq.DefaultView.Sort = "Gradscientific_qualification asc";
            GradscientificQualification.DataSource = dtGq.DefaultView;
            ViewState["GqType"] = dtGq;
            GradscientificQualification.DataTextField = "Gradscientific_qualification";
            GradscientificQualification.DataBind();
            GradscientificQualification.Style.DropDownBoxBoxWidth = new Unit(160);

            //mobilenumber
            DropDownCheckBoxes mobilenumber = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlMobileNumber");
            DataTable dtMobile = dtgridview.DefaultView.ToTable(true, "MobileNumber");
            dtMobile.DefaultView.Sort = "MobileNumber asc";
            mobilenumber.DataSource = dtMobile.DefaultView;
            ViewState["MNType"] = dtMobile;
            mobilenumber.DataTextField = "MobileNumber";
            mobilenumber.DataBind();
            mobilenumber.Style.DropDownBoxBoxWidth = new Unit(160);


            //for GPA
            DropDownCheckBoxes GPA = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlGpa");
            DataTable dtgpa = dtgridview.DefaultView.ToTable(true, "GPA");
            dtgpa.DefaultView.Sort = "GPA asc";
            GPA.DataSource = dtgpa.DefaultView;
            ViewState["GPAType"] = dtgpa;
            GPA.DataTextField = "GPA";
            GPA.DataBind();
            GPA.Style.DropDownBoxBoxWidth = new Unit(160);

            //for FSQ
            DropDownCheckBoxes FSQualification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlfsq");
            DataTable dtfsq = dtgridview.DefaultView.ToTable(true, "Formerscientific_qualification");
            dtfsq.DefaultView.Sort = "Formerscientific_qualification asc";
            FSQualification.DataSource = dtfsq.DefaultView;
            ViewState["fsqType"] = dtfsq;
            FSQualification.DataTextField = "Formerscientific_qualification";
            FSQualification.DataBind();
            FSQualification.Style.DropDownBoxBoxWidth = new Unit(160);

            //for FGQ
            DropDownCheckBoxes FGQualification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlfgq");
            DataTable dtfgq = dtgridview.DefaultView.ToTable(true, "FormerGradscientific_qualification");
            dtfgq.DefaultView.Sort = "FormerGradscientific_qualification asc";
            FGQualification.DataSource = dtfgq.DefaultView;
            ViewState["fgqType"] = dtfgq;
            FGQualification.DataTextField = "FormerGradscientific_qualification";
            FGQualification.DataBind();
            FGQualification.Style.DropDownBoxBoxWidth = new Unit(160);

            //for formar degree
            DropDownCheckBoxes formardegree = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFDegree");
            DataTable dtformardegree = dtgridview.DefaultView.ToTable(true, "formerdegree");
            dtformardegree.DefaultView.Sort = "formerdegree asc";
            formardegree.DataSource = dtformardegree.DefaultView;
            ViewState["fdType"] = dtformardegree;
            formardegree.DataTextField = "formerdegree";
            formardegree.DataBind();
            formardegree.Style.DropDownBoxBoxWidth = new Unit(160);

            //for cumulative Degree
            DropDownCheckBoxes cumulativeDegree = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddllastcumulative");
            DataTable dtcumulativedegree = dtgridview.DefaultView.ToTable(true, "lastcumulative");
            dtcumulativedegree.DefaultView.Sort = "lastcumulative asc";
            cumulativeDegree.DataSource = dtcumulativedegree.DefaultView;
            ViewState["lcType"] = dtcumulativedegree;
            cumulativeDegree.DataTextField = "lastcumulative";
            cumulativeDegree.DataBind();
            cumulativeDegree.Style.DropDownBoxBoxWidth = new Unit(160);

            //for Dates
            DropDownCheckBoxes date = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddldate");
            DataTable dtCreatedon = dtgridview.DefaultView.ToTable(true, "CreatedOn");
            dtCreatedon.DefaultView.Sort = "CreatedOn asc";
            date.DataSource = dtCreatedon.DefaultView;
            ViewState["DType"] = dtCreatedon;
            date.DataTextField = "CreatedOn";            
            date.DataBind();
            date.Style.DropDownBoxBoxWidth = new Unit(160);

            //for Former GPA
            DropDownCheckBoxes FormerGPA = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFormerGPA");
            DataTable dtFormerGPA = dtgridview.DefaultView.ToTable(true, "FormerGPA");
            dtFormerGPA.DefaultView.Sort = "FormerGPA asc";
            FormerGPA.DataSource = dtFormerGPA.DefaultView;
            ViewState["FormerGPA"] = dtFormerGPA;
            FormerGPA.DataTextField = "FormerGPA";
            FormerGPA.DataBind();
            FormerGPA.Style.DropDownBoxBoxWidth = new Unit(160);
            //end

            //for former cumulative
            DropDownCheckBoxes formercumulative = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFormarcumulative");
            DataTable dtformercumulative = dtgridview.DefaultView.ToTable(true, "formercumulative");
            dtformercumulative.DefaultView.Sort = "formercumulative asc";
            formercumulative.DataSource = dtformercumulative.DefaultView;
            ViewState["Formercumulative"] = dtformercumulative;
            formercumulative.DataTextField = "formercumulative";
            formercumulative.DataBind();
            formercumulative.Style.DropDownBoxBoxWidth = new Unit(160);

            //for fullname
            if (fn > 0)
            {
                if (_fullNameArr.Count > first_name.Items.Count)
                {
                    string FNCoulmnName = "First_Name";
                    DataTable newFN = dtfirst_Name.Clone();
                    for (int j = 0; j <= _fullNameArr.Count - 1; j++)
                    {
                        string compareValue = Convert.ToString(_fullNameArr[j]).Trim();
                        string sqlFilter = string.Format("{0} Like '{1}'", FNCoulmnName, compareValue);
                        DataRow[] dataRowArray = dtfirst_Name.Select(sqlFilter);

                        if (dataRowArray.Length > 0)
                        {
                            if (dict.Contains(compareValue))
                            {

                            }
                            else
                            {
                                fn = fn - 1;
                                dict.Remove(compareValue);
                            }
                        }
                        else
                        {
                            fn = fn - 1;
                            dict.Remove(compareValue);
                        }
                    }
                }
                for (int i = 0; i < first_name.Items.Count; i++)
                {
                    first_name.Items[i].Selected = true;
                    display = display + first_name.Items[i] + "+";
                }
            }

            //for degree
            if (ld > 0)
            {
                if (_degreeArr.Count > Degree.Items.Count)
                {
                    string LDColumnName = "lastdegree";
                    DataTable newLD = dtDegree.Clone();
                    for (int j = 0; j <= _degreeArr.Count - 1; j++)
                    {
                        string CompareValueLD = Convert.ToString(_degreeArr[j]).Trim();
                        string filterLD = string.Format("{0}Like '{1}'", LDColumnName, CompareValueLD);
                        DataRow[] dataRowLD = dtDegree.Select(filterLD);
                        if (dataRowLD.Length > 0)
                        {
                            if (dict.Contains(CompareValueLD))
                            {

                            }
                            else
                            {
                                ld = ld - 1;
                                dict.Remove(CompareValueLD);
                            }
                        }
                        else
                        {
                            ld = ld - 1;
                            dict.Remove(CompareValueLD);
                        }
                    }
                }
                for (int i = 0; i < Degree.Items.Count; i++)
                {
                    Degree.Items[i].Selected = true;
                    display1 = display1 + Degree.Items[i] + "+";
                }
            }
            //scintific qualification
            if (sq > 0)
            {
                if (_ScintificQualificationArr.Count > ScintificQualification.Items.Count)
                {
                    string sqColumnName = "scientific_qualification";
                    DataTable newSQ = dtSq.Clone();
                    for (int j = 0; j <= _ScintificQualificationArr.Count - 1; j++)
                    {
                        string CompareValueSQ = Convert.ToString(_ScintificQualificationArr[j]).Trim();
                        string filterSQ = string.Format("{0}Like '{1}'", sqColumnName, CompareValueSQ);
                        DataRow[] dataRowSQ = dtSq.Select(filterSQ);
                        if (dataRowSQ.Length > 0)
                        {
                            if (dict.Contains(CompareValueSQ))
                            {

                            }
                            else
                            {
                                sq = sq - 1;
                                dict.Remove(CompareValueSQ);
                            }
                        }
                        else
                        {
                            sq = sq - 1;
                            dict.Remove(CompareValueSQ);
                        }
                    }
                }
                for (int i = 0; i < ScintificQualification.Items.Count; i++)
                {
                    ScintificQualification.Items[i].Selected = true;
                    display2 = display2 + ScintificQualification.Items[i] + "+";
                }
            }
            //
            //for EmailAddress
            if (ea > 0)
            {
                if (_EmailArr.Count > Email.Items.Count)
                {
                    string EAColumnName = "EmailAddress";
                    DataTable newEa = dtEmail.Clone();
                    for (int j = 0; j <= _EmailArr.Count - 1; j++)
                    {
                        string CompareValueEa = Convert.ToString(_EmailArr[j]).Trim();
                        string filterSQ = string.Format("{0}Like '{1}'", EAColumnName, CompareValueEa);
                        DataRow[] dataRowEa = dtEmail.Select(filterSQ);
                        if (dataRowEa.Length > 0)
                        {
                            if (dict.Contains(CompareValueEa))
                            {

                            }
                            else
                            {
                                ea = ea - 1;
                                dict.Remove(CompareValueEa);
                            }
                        }
                        else
                        {
                            ea = ea - 1;
                            dict.Remove(CompareValueEa);
                        }
                    }
                }
                for (int i = 0; i < Email.Items.Count; i++)
                {
                    Email.Items[i].Selected = true;
                    display3 = display3 + Email.Items[i] + "+";
                }
            }
            //end
            //for GradscientificQualification
            if (gq > 0)
            {
                if (_GradscientificQualificationArr.Count > GradscientificQualification.Items.Count)
                {
                    string GqColumnName = "Gradscientific_qualification";
                    DataTable newGQ = dtGq.Clone();
                    for (int j = 0; j <= _GradscientificQualificationArr.Count - 1; j++)
                    {
                        string CompareValueGQ = Convert.ToString(_GradscientificQualificationArr[j]).Trim();
                        string filterGQ = string.Format("{0}Like '{1}'", GqColumnName, CompareValueGQ);
                        DataRow[] dataRowGQ = dtGq.Select(filterGQ);
                        if (dataRowGQ.Length > 0)
                        {
                            if (dict.Contains(CompareValueGQ))
                            {

                            }
                            else
                            {
                                gq = gq - 1;
                                dict.Remove(CompareValueGQ);
                            }
                        }
                        else
                        {
                            gq = gq - 1;
                            dict.Remove(CompareValueGQ);
                        }
                    }
                }
                for (int i = 0; i < GradscientificQualification.Items.Count; i++)
                {
                    GradscientificQualification.Items[i].Selected = true;
                    display4 = display4 + GradscientificQualification.Items[i] + "+";
                }
            }
            //end
            //mobile
            if (mn > 0)
            {
                if (_mobile.Count > mobilenumber.Items.Count)
                {
                    string MNColumnName = "MobileNumber";
                    DataTable newMN = dtMobile.Clone();
                    for (int j = 0; j <= _mobile.Count - 1; j++)
                    {
                        string CompareValueMN = Convert.ToString(_mobile[j]).Trim();
                        string filterMN = string.Format("{0}Like '{1}'", MNColumnName, CompareValueMN);
                        DataRow[] dataRowMN = dtMobile.Select(filterMN);
                        if (dataRowMN.Length > 0)
                        {
                            if (dict.Contains(CompareValueMN))
                            {

                            }
                            else
                            {
                                mn = mn - 1;
                                dict.Remove(CompareValueMN);
                            }
                        }
                        else
                        {
                            mn = mn - 1;
                            dict.Remove(CompareValueMN);
                        }
                    }
                }
                for (int i = 0; i < mobilenumber.Items.Count; i++)
                {
                    mobilenumber.Items[i].Selected = true;
                    display5 = display5 + mobilenumber.Items[i] + "+";
                }
            }
            //end
            //for GPA
            if (gpa > 0)
            {
                if (_Gpa.Count > GPA.Items.Count)
                {
                    string gpaColumnName = "GPA";
                    DataTable newgpa = dtgpa.Clone();
                    for (int j = 0; j <= _Gpa.Count - 1; j++)
                    {
                        string CompareValuegpa = Convert.ToString(_Gpa[j]).Trim();
                        string filtergpa = string.Format("{0}Like '{1}'", gpaColumnName, CompareValuegpa);
                        DataRow[] dataRowgpa = dtgpa.Select(filtergpa);
                        if (dataRowgpa.Length > 0)
                        {
                            if (dict.Contains(CompareValuegpa))
                            {

                            }
                            else
                            {
                                gpa = gpa - 1;
                                dict.Remove(CompareValuegpa);
                            }
                        }
                        else
                        {
                            gpa = gpa - 1;
                            dict.Remove(CompareValuegpa);
                        }
                    }
                }
                for (int i = 0; i < GPA.Items.Count; i++)
                {
                    GPA.Items[i].Selected = true;
                    display6 = display6 + GPA.Items[i] + "+";
                }
            }
            //End

            //for FSQ
            if (fsq > 0)
            {
                if (_FSQualification.Count > FSQualification.Items.Count)
                {
                    string fsqColumnName = "Formerscientific_qualification";
                    DataTable newfsq = dtfsq.Clone();
                    for (int j = 0; j <= _FSQualification.Count - 1; j++)
                    {
                        string CompareValuefsq = Convert.ToString(_FSQualification[j]).Trim();
                        string filterfsq = string.Format("{0}Like '{1}'", fsqColumnName, CompareValuefsq);
                        DataRow[] dataRowfsq = dtfsq.Select(filterfsq);
                        if (dataRowfsq.Length > 0)
                        {
                            if (dict.Contains(CompareValuefsq))
                            {

                            }
                            else
                            {
                                fsq = fsq - 1;
                                dict.Remove(CompareValuefsq);
                            }
                        }
                        else
                        {
                            fsq = fsq - 1;
                            dict.Remove(CompareValuefsq);
                        }
                    }
                }
                for (int i = 0; i < FSQualification.Items.Count; i++)
                {
                    FSQualification.Items[i].Selected = true;
                    display7 = display7 + FSQualification.Items[i] + "+";
                }
            }
            //end
            //for fgq
            if (fgq > 0)
            {
                if (_FGQualification.Count > FGQualification.Items.Count)
                {
                    string fgqColumnName = "FormerGradscientific_qualification";
                    DataTable newfgq = dtfgq.Clone();
                    for (int j = 0; j <= _FGQualification.Count - 1; j++)
                    {
                        string CompareValuefgq = Convert.ToString(_FGQualification[j]).Trim();
                        string filterfsq = string.Format("{0}Like '{1}'", fgqColumnName, CompareValuefgq);
                        DataRow[] dataRowfgq = dtfgq.Select(filterfsq);
                        if (dataRowfgq.Length > 0)
                        {
                            if (dict.Contains(CompareValuefgq))
                            {
                            }
                            else
                            {
                                fgq = fgq - 1;
                                dict.Remove(CompareValuefgq);
                            }
                        }
                        else
                        {
                            fgq = fgq - 1;
                            dict.Remove(CompareValuefgq);
                        }
                    }
                }
                for (int i = 0; i < FGQualification.Items.Count; i++)
                {
                    FGQualification.Items[i].Selected = true;
                    display8 = display8 + FGQualification.Items[i] + "+";
                }
            }
            //end
            //for formar degree
            if (fd > 0)
            {
                if (_formardegree.Count > formardegree.Items.Count)
                {
                    string FDColumnName = "formerdegree";
                    DataTable newffd = dtformardegree.Clone();
                    for (int j = 0; j <= _formardegree.Count - 1; j++)
                    {
                        string CompareValuefd = Convert.ToString(_formardegree[j]).Trim();
                        string filterfd = string.Format("{0}Like '{1}'", FDColumnName, CompareValuefd);
                        DataRow[] dataRowfd = dtformardegree.Select(filterfd);
                        if (dataRowfd.Length > 0)
                        {
                            if (dict.Contains(CompareValuefd))
                            {

                            }
                            else
                            {
                                fd = fd - 1;
                                dict.Remove(CompareValuefd);
                            }
                        }
                        else
                        {
                            fd = fd - 1;
                            dict.Remove(CompareValuefd);
                        }
                    }
                }
                for (int i = 0; i < formardegree.Items.Count; i++)
                {
                    formardegree.Items[i].Selected = true;
                    display9 = display9 + formardegree.Items[i] + "+";
                }
            }
            //end
            //cumulative degree
            if (lc > 0)
            {
                if (_lastcumulative.Count > cumulativeDegree.Items.Count)
                {
                    string lcColumnName = "lastcumulative";
                    DataTable newflc = dtcumulativedegree.Clone();
                    for (int j = 0; j <= _lastcumulative.Count - 1; j++)
                    {
                        string CompareValuelc = Convert.ToString(_lastcumulative[j]).Trim();
                        string filterlc = string.Format("{0}Like '{1}'", lcColumnName, CompareValuelc);
                        DataRow[] dataRowlc = dtcumulativedegree.Select(filterlc);
                        if (dataRowlc.Length > 0)
                        {
                            if (dict.Contains(CompareValuelc))
                            {

                            }
                            else
                            {
                                lc = lc - 1;
                                dict.Remove(CompareValuelc);
                            }
                        }
                        else
                        {
                            lc = lc - 1;
                            dict.Remove(CompareValuelc);
                        }
                    }
                }
                for (int i = 0; i < cumulativeDegree.Items.Count; i++)
                {
                    cumulativeDegree.Items[i].Selected = true;
                    display10 = display10 + cumulativeDegree.Items[i] + "+";
                }
            }
            //end
            //for date
            if (co > 0)
            {
                if (_CreatedOn.Count > date.Items.Count)
                {
                    string coColumnName = "CreatedOn";
                    DataTable newfco = dtCreatedon.Clone();
                    for (int j = 0; j <= _CreatedOn.Count - 1; j++)
                    {
                        string CompareValueco = Convert.ToString(_CreatedOn[j]).Trim();
                        string filterco = string.Format("{0}Like '{1}'", coColumnName, CompareValueco);
                        DataRow[] dataRowco = dtCreatedon.Select(filterco);
                        if (dataRowco.Length > 0)
                        {
                            if (dict.Contains(CompareValueco))
                            {

                            }
                            else
                            {
                                co = co - 1;
                                dict.Remove(CompareValueco);
                            }
                        }
                        else
                        {
                            co = co - 1;
                            dict.Remove(CompareValueco);
                        }
                    }
                }
                for (int i = 0; i < date.Items.Count; i++)
                {
                    date.Items[i].Selected = true;
                    display11 = display11 + date.Items[i] + "+";
                }
            }
            //for Former GPA
            if(fgpa>0)
            {
                if(_formerGPA.Count>FormerGPA.Items.Count)
                {
                    string columnNameFGPA = "FormerGPA";
                    DataTable newfgpa = dtFormerGPA.Clone();
                    for(int j=0 ; j <= _formerGPA.Count - 1;j++)
                    {
                        double compareValueFGPA = Convert.ToDouble(_formerGPA[j]);
                        string filterFGPA = string.Format("{0}Like '{1}'",columnNameFGPA,compareValueFGPA.ToString());
                        DataRow[] datarowFGPA = dtFormerGPA.Select(filterFGPA);
                        if(datarowFGPA.Length > 0)
                        {
                            if (dict.Contains(compareValueFGPA))
                            {
                            }
                            else
                            {
                                fgpa = fgpa - 1;
                                dict.Remove(compareValueFGPA);
                            }
                        }
                    }
                    for(int i=0;i<FormerGPA.Items.Count;i++)
                    {
                        FormerGPA.Items[i].Selected = true;
                        display12 = display12 + FormerGPA.Items[i] + "+";
                    }
                }
            }
            //end
            //secondname
            if (sn > 0)
            {
                if (_second_name.Count > Second_Name.Items.Count)
                {
                    string columnNamesn = "Second_Name";
                    DataTable newsn =dtSecond_Name.Clone();
                    for (int j = 0; j <= _second_name.Count - 1; j++)
                    {
                        double compareValuesn = Convert.ToDouble(_second_name[j]);
                        string filtersn = string.Format("{0}Like '{1}'", columnNamesn, compareValuesn);
                        DataRow[] datarowsn = dtSecond_Name.Select(filtersn);
                        if (datarowsn.Length > 0)
                        {
                            if (dict.Contains(compareValuesn))
                            {
                            }
                            else
                            {
                                sn = sn - 1;
                                dict.Remove(compareValuesn);
                            }
                        }
                    }
                    for (int i = 0; i < Second_Name.Items.Count; i++)
                    {
                        Second_Name.Items[i].Selected = true;
                        display13 = display13 + Second_Name.Items[i] + "+";
                    }
                }
            }
            //end
            //third_name
            if (tn > 0)
            {
                if (_Third_Name.Count > Third_Name.Items.Count)
                {
                    string columnNametn = "Third_Name";
                    DataTable newtn = dtThird_Name.Clone();
                    for (int j = 0; j <= _Third_Name.Count - 1; j++)
                    {
                        double compareValuetn = Convert.ToDouble(_Third_Name[j]);
                        string filtertn = string.Format("{0}Like '{1}'", columnNametn, compareValuetn);
                        DataRow[] datarowtn = dtThird_Name.Select(filtertn);
                        if (datarowtn.Length > 0)
                        {
                            if (dict.Contains(compareValuetn))
                            {
                            }
                            else
                            {
                                tn = tn - 1;
                                dict.Remove(compareValuetn);
                            }
                        }
                    }
                    for (int i = 0; i < Third_Name.Items.Count; i++)
                    {
                        Third_Name.Items[i].Selected = true;
                        display14 = display14 + Third_Name.Items[i] + "+";
                    }
                }
            }
            //end
            //lastname
            if (ln > 0)
            {
                if (_last_Name.Count > last_Name.Items.Count)
                {
                    string columnNameln = "Last_Name";
                    DataTable newln = dtlast_Name.Clone();
                    for (int j = 0; j <= _last_Name.Count - 1; j++)
                    {
                        double compareValueln = Convert.ToDouble(_last_Name[j]);
                        string filterln = string.Format("{0}Like '{1}'", columnNameln, compareValueln);
                        DataRow[] datarowln = dtlast_Name.Select(filterln);
                        if (datarowln.Length > 0)
                        {
                            if (dict.Contains(compareValueln))
                            {
                            }
                            else
                            {
                                ln = ln - 1;
                                dict.Remove(compareValueln);
                            }
                        }
                    }
                    for (int i = 0; i < last_Name.Items.Count; i++)
                    {
                        last_Name.Items[i].Selected = true;
                        display15 = display15 + last_Name.Items[i] + "+";
                    }
                }
            }
            //end
            //former cumulative
            if (fc > 0)
            {
                if (_formercumulative.Count > formercumulative.Items.Count)
                {
                    string columnNamefc = "formercumulative";
                    DataTable newfc = dtformercumulative.Clone();
                    for (int j = 0; j <= _formercumulative.Count - 1; j++)
                    {
                        double compareValuefc = Convert.ToDouble(_formercumulative[j]);
                        string filterfc = string.Format("{0}Like '{1}'", columnNamefc, compareValuefc);
                        DataRow[] datarowfc = dtformercumulative.Select(filterfc);
                        if (datarowfc.Length > 0)
                        {
                            if (dict.Contains(compareValuefc))
                            {
                            }
                            else
                            {
                                fc = fc - 1;
                                dict.Remove(compareValuefc);
                            }
                        }
                    }
                    for (int i = 0; i < formercumulative.Items.Count; i++)
                    {
                        formercumulative.Items[i].Selected = true;
                        display16 = display16 + formercumulative.Items[i] + "+";
                    }
                }
            }
            //end

            int child = dtgridview.DefaultView.Count;

            if (fn > 0 || ld > 0 || sq > 0 || ea > 0 || gq > 0 || mn > 0 || gpa > 0 || fsq > 0 || fgq > 0 || fd > 0 || lc > 0 || co > 0 || fgpa > 0 || sn > 0 || tn > 0 || ln > 0 || fc > 0)
            {
                lblRequestsCount.Text = "(" + child + ")";
                string strHeader = string.Empty;

                foreach (DictionaryEntry de in dict)
                {
                    strHeader = strHeader + de.Key.ToString().Trim() + "+";
                }
                lblfilterData.Text = strHeader;
            }
            else
            {
                lblfilterData.Text = string.Empty;
                lblRequestsCount.Text = "(" + child + ")";
            }
           
        }
        protected void gvwncms_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {           
            gvwncms.PageIndex = e.NewPageIndex;
            if (Session["RequestData"] == null)
            {
                gvwncms.DataSource = GetUserDetails();
                gvwncms.DataBind();
                dtgridview = (DataTable)Session["RequestData"];
            }
            else
            {
                dtgridview = (DataTable)Session["RequestData"];
            }

            gvwncms.DataSource = dtgridview;
            gvwncms.DataBind();
                       
            if (dtgridview.Rows.Count > 0)
            {
                UpdateDropDownChckBoxBinding();
            }            
        }
        protected void btnPrint_Command(object sender, CommandEventArgs e)
        {
            Session["Id"] = e.CommandArgument;
            ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page), "OpenWindow", "window.open('UserDetailsById.Aspx?id=" + e.CommandArgument + "');", true);
        }
        protected void btnSearch_Click(object sender, ImageClickEventArgs e)
        {
            if (ddlfilter.SelectedItem.Text != "0" || txtSearch.Text != "")
            {
                DataSet dsSearch = new DataSet();
                dsSearch = DAL_User.GetSearchText(Convert.ToInt32(ddlfilter.SelectedValue.ToString()), txtSearch.Text.ToString());
                gvwncms.DataSource = dsSearch.Tables[0];
                gvwncms.DataBind();
                Session["RequestData"] = dsSearch.Tables[0];
                DropDownChckBoxBinding();
                btnRefresh.Visible = true;
                txtSearch.Text = string.Empty;
            }
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            //gvwncms.DataSource = GetUserDetails();
            //gvwncms.DataBind();
            //DropDownChckBoxBinding();
            //btnRefresh.Visible = false;
            DataSet ds = DAL_User.getUserDetails();
            gvwncms.DataSource=ds;
            gvwncms.DataBind();
            Session["RequestData"] = ds.Tables[0];
            DropDownChckBoxBinding();
            btnRefresh.Visible = false;
        }

        protected void btnPrintAll_Click(object sender, EventArgs e)
        {
            StringBuilder id = new StringBuilder();
            string[] str = new string[1];
            //CheckBox chkbSelectAll = (CheckBox)gvwncms.HeaderRow.FindControl("chkbSelectAll");
            //if(chkbSelectAll.Checked==true)
            //{
            //    //string strRedirect;
            //    foreach(GridViewRow gvr in gvwncms.Rows)
            //    {
            //        CheckBox chkbSelect = (CheckBox)gvr.FindControl("chkbSelect");
            //        if (chkbSelect.Checked == true)
            //        {
            //            str[0] = gvwncms.DataKeys[gvr.RowIndex].Values[0].ToString();                       
            //            sbid.Append(str[0] + ",");                      
            //        }
            //    }
            //    ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page), "OpenWindow", "window.open('UserDetailsById.aspx?sbid=" + sbid + "');", true);                      
            //}
            //else
            //{
                foreach (GridViewRow gvr in gvwncms.Rows)
                {
                    CheckBox chkbSelect = (CheckBox)gvr.FindControl("chkbSelect");
                    if (chkbSelect.Checked == true)
                    {
                        str[0] = gvwncms.DataKeys[gvr.RowIndex].Values[0].ToString();
                        id.Append(str[0] + ",");
                    }
                }
                //Session["sbid"] = id;
                ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page), "OpenWindow", "window.open('UserDetailsById.aspx?id=" + id + "');", true);
                btnClick.Value = "False";
                pnlNCMS.Update();
                //clearing checkbox after print a record
                //foreach (GridViewRow row in gvwncms.Rows)
                //{
                //    CheckBox cbox = row.FindControl("chkbSelect") as CheckBox;
                //    if (cbox.Checked)
                //    {
                //        cbox.Checked = false;
                //    }
                //}
            //}            
        }      
        protected void gvwncms_Rowcreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
            {
                CheckBox chkBxSelect = (CheckBox)e.Row.Cells[1].FindControl("chkbSelect");
                CheckBox chkBxHeader = (CheckBox)this.gvwncms.HeaderRow.FindControl("chkbSelectAll");

                chkBxSelect.Attributes["onclick"] = string.Format("javascript:ChildClick(this,'{0}');", chkBxHeader.ClientID);
            }
        }       
    }
}