﻿<%@ Page Title="الشركة الوطنية للأنظمة الميكانيكية - قسم التوظيف" Language="C#" MasterPageFile="~/NCMSMobileApp.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="NCMSMobileAPP.Default" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .colVisibility {
            display: none;
        }

        div.dd_chk_select {
            border-color: #344E8B;
            height: 40px;
            padding: 10px 15px;
            font-size: 14px;
        }

            div.dd_chk_select div#caption {
                text-align: right !important;
            }
        /*div.dd_chk_select{
            background-image: url("WebResource.axd?d=8mNM2oY0_MTbltHc0o1ItfBtftnl_Ix-J3XMyAmXe43x2mStvm4W236Ml96RhLEK-F4hjsF3c7O8pkQD-NSzaUrxIaLZm-Eq3OKyNWdRLgdhz_ZKclbD9lkWS-so_HikC1LntSmeQW8cwYVBXF1IaUmZklhJ8Qy6rcrSEpkGWnQ1&t=634974191230236446");
            background-position: center left;
            background-repeat: no-repeat;
        }
        div.dd_chk_select:hover{
                background-image: url("WebResource.axd?d=8mNM2oY0_MTbltHc0o1ItfBtftnl_Ix-J3XMyAmXe43x2mStvm4W236Ml96RhLEK-F4hjsF3c7O8pkQD-NSzaUrxIaLZm-Eq3OKyNWdRLgdhz_ZKclbD9lkWS-so_HikC1LntSmeQW8cwYVBXF1IaUmZklhJ8Qy6rcrSEpkGWnQ1&t=634974191230236446");
                background-position: center left;           
	            background-repeat: no-repeat;
         }*/
        div.dd_chk_drop {
            top: 20px !important;
            text-align: right !important;
            border-color: #344E8B !important;
            left: inherit !important;
            right: -1px;
        }

        div.dd_chk_select {
            border-color: #344E8B !important;
            height: 25px !important;
            padding: 4px 15px !important;
            font-size: 14px;
        }

        .btn-blue {
            min-height: 40px;
        }

        input[type='checkbox'] {
            margin-left: 10px;
        }

        div.dd_chk_drop div#buttons input {
            background-image: none !important;
        }

            div.dd_chk_drop div#buttons input:hover {
                background-image: none !important;
            }

        .dd_chk_drop {
            width: 190px !important;
        }

        #buttons input[type='button'] {
            background: #fff;
            border: 1px solid #344E8B !important;
            border-radius: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="smgr" runat="server"></asp:ScriptManager>
    <asp:HiddenField Value="False" ID="btnClick" runat="server" />
    <asp:UpdatePanel runat="server" ID="upd">
        <ContentTemplate>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <%--<asp:Button runat="server" CssClass="btn btn-blue" Text="Apply Filter" />
                            <asp:Button runat="server" CssClass="btn btn-blue" Text="أصدر" ID="btnSubmit"/>
                            <asp:Button runat="server" CssClass="btn btn-blue" Text="طباعة" />--%>
                            <asp:Label ID="lblError" runat="server"></asp:Label>                            
                            <asp:Label ID="lblfilterData" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lblRequestsCount" runat="server" Visible="false"></asp:Label>
                            <table class="table table-bordered">
                                <tr>
                                    <asp:GridView ID="gvwncms" runat="server" AutoGenerateColumns="false" OnPageIndexChanging="gvwncms_PageIndexChanging" PageSize="10"
                                        AllowPaging="true" AllowSorting="true" EnableViewState="true" OnPreRender="gvwncms_PreRender" CssClass="table table-bordered" OnSorting="gvwncms_Sorting">
                                        <Columns>
                                            <%--<asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkSelect" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnPrint" runat="server" Text="Print" OnCommand="btnPrint_Command" CommandArgument='<%#Eval("ID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           <asp:BoundField DataField="ID" HeaderText="ID" />                                           
                                            <asp:TemplateField HeaderText="الاسم الكامل" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlfullName" runat="server" EnableViewState="true" AutoPostBack="false" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select Full Name" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="الاسم الكامل" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                        <%--<Style DropDownBoxBoxWidth="230px" SelectBoxWidth="120px" />--%>
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFullName" runat="server" Text='<%#Eval("FullName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:BoundField DataField="MobileNumber" HeaderText="MobileNumber" ItemStyle-HorizontalAlign="Right" />    --%>
                                            <%--mobile number--%>
                                            <asp:TemplateField HeaderText="رقم الجوال المحلي" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlMobileNumber" runat="server" EnableViewState="true" AutoPostBack="false" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select Mobile number" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="رقم الجوال المحلي" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMobileNumber" runat="server" Text='<%#Eval("MobileNumber") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--end--%>
                                            <asp:TemplateField HeaderText="البريد الالكتروني" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlEmail" runat="server" EnableViewState="true" AutoPostBack="false" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select Email" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="البريد الالكتروني" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmail" runat="server" Text='<%#Eval("EmailAddress") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="الدرجة العلمية الأخيرة" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlDegree" runat="server" EnableViewState="true" AutoPostBack="false" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select Degree" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="الدرجة العلمية الأخيرة" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                        <%--<Style DropDownBoxBoxWidth="230px" SelectBoxWidth="120px" />--%>
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDegree" runat="server" Text='<%#Eval("lastdegree") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="المؤهل العلمي" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlSQ" runat="server" EnableViewState="true" AutoPostBack="false" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select Scientific Qualification" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="المؤهل العلمي" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSq" runat="server" Text='<%#Eval("scientific_qualification") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- for Gradscientific_qualification--%>
                                            <asp:TemplateField HeaderText="درجة التأهيل العلمي" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlGQ" runat="server" EnableViewState="true" AutoPostBack="false" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select Gradscientific Qualification" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="درجة التأهيل العلمي" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGQ" runat="server" Text='<%#Eval("Gradscientific_qualification") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--for GPA--%>
                                            <asp:TemplateField HeaderText="GPA" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlGpa" runat="server" EnableViewState="true" AutoPostBack="false" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select GPA" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="GPA" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblgpa" runat="server" Text='<%#Eval("GPA") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:BoundField DataField="FormerGPA" HeaderText="FormerGPA" />--%>
                                            <%--end--%>
                                            <%-- for Formerscientific_qualification--%>
                                            <asp:TemplateField HeaderText="Formerscientific Qualification" ShowHeader="true" ItemStyle-CssClass="colVisibility" HeaderStyle-CssClass="colVisibility">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlfsq" runat="server" EnableViewState="true" AutoPostBack="false" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select Formerscientific Qualification" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="Formerscientific Qualification" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblfsq" runat="server" Text='<%#Eval("Formerscientific_qualification") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- end--%>
                                            <%--for FormerGradscientific_qualification--%>
                                            <asp:TemplateField HeaderText="FormerGradscientific Qualification" ShowHeader="true" ItemStyle-CssClass="colVisibility" HeaderStyle-CssClass="colVisibility">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlfgq" runat="server" EnableViewState="true" AutoPostBack="false" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select FormerGradscientific Qualification" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="FormerGradscientific Qualification" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblgpaformer" runat="server" Text='<%#Eval("FormerGradscientific_qualification") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--end--%>
                                            <asp:BoundField DataField="FormerGPA" HeaderText="FormerGPA" ItemStyle-CssClass="colVisibility" HeaderStyle-CssClass="colVisibility" />
                                            <%--for formerdegree--%>
                                            <asp:TemplateField HeaderText="FormerDegree" ShowHeader="true" ItemStyle-CssClass="colVisibility" HeaderStyle-CssClass="colVisibility">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlFDegree" runat="server" EnableViewState="true" AutoPostBack="false" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select Former Degree" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="Former Degree" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFormarDegree" runat="server" Text='<%#Eval("formerdegree") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--end--%>

                                            <%--for FormerCumulative--%>
                                            <asp:TemplateField HeaderText="FormerCumulative" ShowHeader="true" ItemStyle-CssClass="colVisibility" HeaderStyle-CssClass="colVisibility">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlFormarcumulative" runat="server" EnableViewState="true" AutoPostBack="false" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select Former Cumulative" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="Last Cumulative" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbllastcumulative" runat="server" Text='<%#Eval("lastcumulative") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--end--%>
                                            <%--for created on--%>
                                            <asp:TemplateField HeaderText="تاريخ" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddldate" runat="server" EnableViewState="true" AutoPostBack="false" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select Date" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="تاريخ" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                        <%--<Style DropDownBoxBoxWidth="230px" SelectBoxWidth="120px" />--%>
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDate" runat="server" Text='<%#Eval("CreatedOn") %>'></asp:Label>
                                                    
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <%--end--%>
                                        </Columns>
                                    </asp:GridView>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updProgres" runat="server">
        <ProgressTemplate>
            <div id="Loader">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>
