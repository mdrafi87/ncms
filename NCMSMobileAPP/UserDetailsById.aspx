﻿<%@ Page Title="Get User Details" Language="C#"  AutoEventWireup="true" CodeBehind="UserDetailsById.aspx.cs" Inherits="NCMSMobileAPP.UserDetailsById" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>--%>


<%--<html>--%>
<body>
<form id="form1" runat="server">
<div align="center">
    <rsweb:ReportViewer ID="rptUserDetails" runat="server" AsyncRendering="false" Width="800px" Height="900px"></rsweb:ReportViewer>
    <asp:ScriptManager ID="smgr" runat="server"></asp:ScriptManager>
</div>
</form>
</body>
</html>
