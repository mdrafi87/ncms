﻿<%@ Page Title="User Details" Language="C#" MasterPageFile="~/NCMSMobileApp.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="NCMSMobileAPP.Test_Default" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="Page-Exit" content="progid:DXImageTransform.Microsoft.Fade(Overlap=1.00,duration=0.2)" />
    <style type="text/css">
        .colVisibility {
            display: none;
        }

        div.dd_chk_select {
            border-color: #344E8B;
            height: 40px;
            padding: 10px 15px;
            font-size: 14px;
        }

            div.dd_chk_select div#caption {
                text-align: right !important;
            }
        /*.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
        padding: 8px;
        line-height: 1.42857143;
        outline-width:medium;
        vertical-align: top;
        border-top: 1px solid #ddd;
        }*/
        div.dd_chk_select {
        border-color: #344E8B !important;
        height: 25px !important;
        min-width: 180px;
        padding: 4px 15px !important;
        font-size: 14px;
        }
        div.dd_chk_drop {
            top: 20px !important;
            text-align: right !important;
            border-color: #344E8B !important;
            left: inherit !important;
            right: -1px;
        }

        div.dd_chk_select {
            border-color: #344E8B !important;
            height: 25px !important;
            padding: 4px 15px !important;
            font-size: 14px;
        }

        .btn-blue {
            min-height: 40px;
        }

        input[type='checkbox'] {
            margin-left: 10px;
        }

        div.dd_chk_drop div#buttons input {
            background-image: none !important;
        }

            div.dd_chk_drop div#buttons input:hover {
                background-image: none !important;
            }

        .dd_chk_drop {
            width: 190px !important;
        }

        #buttons input[type='button'] {
            background: #fff;
            border: 1px solid #344E8B !important;
            border-radius: 5px;
        }

        .AutoExtender {
            display: block;
            border: solid 1px #272725;
            padding: 15px;
            background-color: White;
            margin-left: 0px;
            text-align: left;
            max-width: 100%;
        }

        .AutoExtenderList {
            border-bottom: solid 1px #ccc;
            cursor: pointer;
            margin-bottom: 8px;
            list-style: none;
            padding-bottom: 5px;
        }

        .AutoExtenderHighlight {
            color: White;
            background-color: #344E8B;
            cursor: pointer;
            margin-bottom: 8px;
            list-style: none;
            padding-bottom: 5px;
        }

        .print-btn {
            padding: 3px 12px;
            margin: 0;
            min-height: 20px;
        }

        .new .form-control {
            height: 40px;
        }

        .new .btn-blue {
            margin-top: -11px;
        }

        .new .input-group-btn .btn {
            padding: 3px 12px;
        }

        table#ContentPlaceHolder1_gvwncms tr:last-child {
            background: #fff;
        }

            table#ContentPlaceHolder1_gvwncms tr:last-child td table tr td {
                padding: 15px;
                padding-left: 0px;
                padding-right: 0;
                margin-left: -2px;
                /*border: 1px solid #344E8B;
            color: #272725*/
            }

                table#ContentPlaceHolder1_gvwncms tr:last-child td table tr td a {
                    color: #272725;
                    padding: 6px 15px;
                    border: 1px solid #344E8B;
                }

                table#ContentPlaceHolder1_gvwncms tr:last-child td table tr td span {
                    background: #344E8B;
                    color: #fff;
                    padding: 6px 15px;
                    border: 1px solid #344E8B;
                }

        .AutoExtenderList, .AutoExtenderHighlight, .AutoExtender {
            text-align: right !important;
        }

        /*.table-responsive {
            min-height: .01%;
            border: medium;
        }*/
    </style>
    <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        function SetContextKey() {
            $find('AutoEx').set_contextKey($get("<%=ddlfilter.ClientID %>").value);
        }
        function acePopulated(sender, e) {


        }
        function aceSelected(sender, e) {


        }
    </script>
    <script type="text/javascript">

        function HeaderClick(CheckBox) {
            var TargetBaseControl = document.getElementById('<%= this.gvwncms.ClientID %>');
            var TargetChildControl = "chkbSelect";
            var counterHead = 0;
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                    Inputs[n].checked = CheckBox.checked;
                    if (Inputs[n].checked)
                        counterHead = counterHead + 1;
                }
            Counter = CheckBox.checked ? TotalChkBx : 0;
            $('#lblSelectCount').text(counterHead);
        }
        function checkValue() {
            var hidControl = document.getElementById('<%= btnClick.ClientID %>');
            if (hidControl != null) hidControl.value = "True";
        }
        function ChildClick(CheckBox, HCheckBox) {

            var counterChild = 0;
            var TargetBaseControl = document.getElementById('<%= this.gvwncms.ClientID %>');
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            var TargetChildControl = "chkbSelect";
            var Counter = 0;

            var HeaderCheckBox = document.getElementById(HCheckBox);


            if (CheckBox.checked && Counter < TotalChkBx)
                Counter++;
            else if (Counter > 0)
                Counter--;
            if (Counter < TotalChkBx)
                HeaderCheckBox.checked = false;
            else if (Counter == TotalChkBx)
                HeaderCheckBox.checked = true;


            for (var n = 0; n < Inputs.length; ++n) {

                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl) != -1) {
                    if (Inputs[n].checked)
                        counterChild = counterChild + 1;
                }
            }
            $('#lblSelectCount').text(counterChild);
        }
    </script>
    <script type="text/javascript">
        window.onload = function () {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<%=lblprinterror.ClientID%>").style.display = "none";
            }, seconds * 1500);
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="pnlNCMS" ChildrenAsTriggers="true">
        <ContentTemplate>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-horizontal">
                            <asp:HiddenField Value="False" ID="btnClick" runat="server" />
                            <asp:Label ID="lblError" runat="server"></asp:Label>
                            <asp:Label ID="lblRequestsCount" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lblfilterData" runat="server" Visible="false"></asp:Label>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddlfilter" runat="server" name="Items" ClientIDMode="Static" CssClass="form-control-new">
                                        <asp:ListItem Value="1">الاسم الأول</asp:ListItem>
                                        <asp:ListItem Value="2">اسم الأب</asp:ListItem>
                                        <asp:ListItem Value="3">اسم العائلة</asp:ListItem>
                                        <asp:ListItem Value="4">اسم العائلة</asp:ListItem>
                                        <asp:ListItem Value="5">رقم الجوال المحلي</asp:ListItem>
                                        <asp:ListItem Value="6">البريد الالكتروني</asp:ListItem>
                                        <asp:ListItem Value="7">جامعة</asp:ListItem>
                                        <asp:ListItem Value="8">الدرجة العلمية الأخيرة</asp:ListItem>
                                        <asp:ListItem Value="9">التخصص للمؤهل العلمي الأخير</asp:ListItem>
                                        <asp:ListItem Value="10">المعدل التراكمي للمؤهل العلمي الأخير</asp:ListItem>
                                        <asp:ListItem Value="11">مدينة</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-sm-4">
                                    <div class="input-group new">
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" onkeyup="SetContextKey()" placeholder="بحث..."></asp:TextBox>
                                        <span class="input-group-btn">
                                            <asp:ImageButton ID="btnSearch" ImageUrl="~/img/search.png" runat="server" CssClass="btn btn-blue" Text="Search" OnClick="btnSearch_Click" />
                                        </span>
                                        <cc1:AutoCompleteExtender BehaviorID="AutoEx" ID="AutoCompleteExtender" runat="server"
                                            TargetControlID="txtSearch" CompletionInterval="250" MinimumPrefixLength="1" UseContextKey="true"
                                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                                            CompletionListHighlightedItemCssClass="AutoExtenderHighlight" OnClientPopulated="acePopulated"
                                            CompletionSetCount="15" OnClientItemSelected="aceSelected"
                                            ServiceMethod="GetSearchgDetails" ServicePath="~/WebServices/SearchAutoSuggest.asmx">
                                        </cc1:AutoCompleteExtender>
                                    </div>
                                    <!-- end input-group -->
                                </div>
                                <div class="col-sm-2">
                                    <asp:Button ID="btnRefresh" runat="server" Text="نعش الذاكرة" OnClick="btnRefresh_Click" CssClass="btn btn-blue" />
                                </div>
                                <div class="col-sm-2">
                                    <asp:Button runat="server" CssClass="btn btn-blue" Text="طباعة جميع" ID="btnPrintAll" OnClick="btnPrintAll_Click" OnClientClick="javascript:checkValue();" />
                                </div>
                            </div>
                            <!-- end form-group -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <asp:Label ID="lblprinterror" runat="server" Visible="false" Text="الرجاء تحديد...!" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                            <!-- end form-group -->
                        </div>
                        <!-- end form-horizontal -->
                    </div>
                    <!-- end col-sm12 -->

                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive" style="overflow-x: auto;" id="div">
                            <table class="table table-bordered">
                                <tr>
                                    <asp:GridView ID="gvwncms" runat="server" AutoGenerateColumns="false" OnPageIndexChanging="gvwncms_PageIndexChanging" PageSize="10"
                                        AllowPaging="True" AllowSorting="true" EnableViewState="true" CssClass="table table-bordered table-striped" DataKeyNames="ID"
                                        EnableSortingAndPagingCallbacks="false" OnRowCreated="gvwncms_Rowcreated">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkbSelectAll" runat="server" onclick="javascript:HeaderClick(this);" CausesValidation="true" BorderStyle="None" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkbSelect" runat="server" CausesValidation="true" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnPrint" runat="server" Text="طباعة" CssClass="btn btn-blue print-btn" OnCommand="btnPrint_Command" CommandArgument='<%#Eval("ID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="ID" HeaderText="ID" HeaderStyle-CssClass="colVisibility" ItemStyle-CssClass="colVisibility" />
                                            <asp:TemplateField HeaderText="الاسم الأول" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlFirstName" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="الاسم الاول" OkButton="تطبيق" CancelButton="إلغاء" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblfirstname" runat="server" Text='<%#Eval("First_Name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="اسم الأب" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlSecondName" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="اسم الأب" OkButton="تطبيق" CancelButton="إلغاء" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSecondName" runat="server" Text='<%#Eval("Second_Name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="اسم العائلة" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlThirdName" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="اسم العائلة" OkButton="تطبيق" CancelButton="إلغاء" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblThirdName" runat="server" Text='<%#Eval("Third_Name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="اسم العائلة" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlLastname" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="اسم العائلة" OkButton="تطبيق" CancelButton="إلغاء" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLastname" runat="server" Text='<%#Eval("Last_Name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="رقم الجوال المحلي" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlMobileNumber" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select Mobile number" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="رقم الجوال المحلي" OkButton="تطبيق" CancelButton="إلغاء" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMobileNumber" runat="server" Text='<%#Eval("MobileNumber") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="البريد الالكتروني" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlEmail" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select Email" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="البريد الالكتروني" OkButton="تطبيق" CancelButton="إلغاء" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmail" runat="server" Text='<%#Eval("EmailAddress") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="الدرجة العلمية الأخيرة" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlDegree" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select Degree" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="الدرجة العلمية الأخيرة" OkButton="تطبيق" CancelButton="إلغاء" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDegree" runat="server" Text='<%#Eval("lastdegree") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="التخصص للمؤهل العلمي الأخير" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlSQ" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select Scientific Qualification" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="التخصص للمؤهل العلمي الأخير" OkButton="تطبيق" CancelButton="إلغاء" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSq" runat="server" Text='<%#Eval("scientific_qualification") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="جهة التخرج للمؤهل العلمي الأخير" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlGQ" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="جهة التخرج للمؤهل العلمي الأخير" OkButton="تطبيق" CancelButton="إلغاء" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGQ" runat="server" Text='<%#Eval("Gradscientific_qualification") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="وزن المعدل التراكي للمؤهل الأخير" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddllastcumulative" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="وزن المعدل التراكي للمؤهل الأخير" OkButton="تطبيق" CancelButton="إلغاء" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbllastcumulative" runat="server" Text='<%#Eval("lastcumulative") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="آخر التراكمي" ShowHeader="true" HeaderStyle-CssClass="colVisibility" ItemStyle-CssClass="colVisibility">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlcumulativeother" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="آخر التراكمي" OkButton="تطبيق" CancelButton="إلغاء" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="ddlcumulativeother" runat="server" Text='<%#Eval("cumulative_other") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="المعدل التراكمي للمؤهل العلمي الأخير " ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlGpa" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="المعدل التراكمي للمؤهل العلمي الأخير " OkButton="تطبيق" CancelButton="إلغاء" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblgpa" runat="server" Text='<%#Eval("GPA") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="درجة علمية سابقة" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlFDegree" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="درجة علمية سابقة" OkButton="تطبيق" CancelButton="إلغاء" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFormarDegree" runat="server" Text='<%#Eval("formerdegree") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="جهة التخرج للمؤهل العلمي السابق" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlfgq" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="جهة التخرج للمؤهل العلمي السابق" OkButton="تطبيق" CancelButton="إلغاء" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblgpaformer" runat="server" Text='<%#Eval("FormerGradscientific_qualification") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="التخصص للمؤهل العلمي السابق" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlfsq" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="التخصص للمؤهل العلمي السابق " OkButton="تطبيق" CancelButton="إلغاء" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblfsq" runat="server" Text='<%#Eval("Formerscientific_qualification") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="وزن المعدل التراكي للمؤهل السابق" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlFormarcumulative" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="وزن المعدل التراكي للمؤهل السابق" OkButton="تطبيق" CancelButton="إلغاء" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblformercumulative" runat="server" Text='<%#Eval("formercumulative") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Formercumulative other" ShowHeader="true" HeaderStyle-CssClass="colVisibility" ItemStyle-CssClass="colVisibility">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlfcother" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="Former Cumulative" OkButton="تطبيق" CancelButton="إلغاء" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblfcother" runat="server" Text='<%#Eval("Formercumulative_other") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="المعدل التراكمي للمؤهل العلمي السابق" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlFormerGPA" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select Former GPA"
                                                        ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="المعدل التراكمي للمؤهل العلمي السابق" OkButton="تطبيق" CancelButton="إلغاء" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblfgpa" runat="server" Text='<%#Eval("FormerGPA") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="مدينة" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlCityAr" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="مدينة" OkButton="تطبيق" CancelButton="إلغاء" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCityAr" runat="server" Text='<%#Eval("CityName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="تاريخ" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddldate" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="تاريخ" OkButton="تطبيق" CancelButton="إلغاء" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDate" runat="server" Text='<%#Eval("CreatedOn") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ملاحظات" ShowHeader="true">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNotes" runat="server" Text='<%#Eval("Notes") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </tr>
                            </table>
                        </div>
                        <!-- table-responsive -->
                    </div>
                    <!-- end col-sm-12 -->
                </div>
                <!-- end row -->

            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
