﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="NCMSMobileAPP.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="ar" dir="rtl">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>الشركة الوطنية للأنظمة الميكانيكية - قسم التوظيف</title>
    <link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="arabic-styles/bootstrap.min.css" rel="stylesheet" />
    <link href="arabic-styles/login.css" rel="stylesheet" />
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>    
    <link rel="shortcut icon" href="img/favicon.ico" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="wrapper">
            <div class="form-signin">       
              <div class="header">
                  <img src="img/ncms-logo.jpg" alt="NCMS Logo" class="img-responsive" />
                  <div class="form-signin-heading">
                      <h2> قسم التوظيف </h2>
                  </div>
              </div>
              <asp:TextBox runat="server" CssClass="form-control" placeholder="اسم المستخدم"  autofocus="" ID="txtUserName" />
                 <asp:RequiredFieldValidator ID="rfvtxtUserName" runat="server" ControlToValidate="txtUserName"
                                                    ValidationGroup="v1" SetFocusOnError="true" Display="Dynamic" ForeColor="Red"
                                ErrorMessage="* Enter user name ">
                                                </asp:RequiredFieldValidator>

              <asp:Textbox runat="server" TextMode="Password" CssClass="form-control" placeholder="كلمة المرور"  ID="txtPassword"/>   
                  <asp:RequiredFieldValidator ID="rfvtxtPassword" runat="server" ControlToValidate="txtPassword" ForeColor="Red"
                                                    ValidationGroup="v1" SetFocusOnError="true" Display="Dynamic" 
                                ErrorMessage="* Enter password ">
                                                </asp:RequiredFieldValidator>               
              <asp:Button runat="server" CssClass="btn btn-lg btn-primary btn-block" Text="دخول"  OnClick="btnSubmit_Click" ValidationGroup="v1"/>
                <asp:Label ID="lblloginError" runat="server" CssClass="failureNotification"></asp:Label>            
            </div><!-- end form-signin -->
          </div><!-- end wrapper -->
       <%-- <p class="footer-text">
             &copy; 2016 حقوق الطبع والنشر محفوظة<a href="http://www.creative-sols.com" target="_blank">الحلول الإبداعية</a> 
        
        </p>--%>
        <p class="footer-text">
             &copy;  <asp:Literal ID="ltrCopyrightYear" runat="server"></asp:Literal>  حقوق الطبع والنشر محفوظة <a href="http://www.ncms.sa/" target="_blank">الشركة الوطنية للأنظمة الميكانيكية</a>    
        
        </p>
    </div>
    </form>
</body>
</html>
