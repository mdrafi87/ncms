﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using SettingsLibrary;
using LibraryCore.DAL;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saplin.Controls;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using Microsoft.Reporting.WebForms;

namespace NCMSMobileAPP
{
    public partial class UserDetailsById : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                string Username = Session["User"].ToString();
                GetUserDetailsByID();
            }
        }
        private void GetUserDetailsByID()
        {
            DataTable dt = new DataTable();            
            dt.Columns.Add("ID", typeof(Int32));
            //dt.Columns.Add("FullName", typeof(string));
            dt.Columns.Add("First_Name", typeof(string));
            dt.Columns.Add("Second_Name", typeof(string));
            dt.Columns.Add("Third_Name", typeof(string));
            dt.Columns.Add("Last_Name", typeof(string));
            dt.Columns.Add("EmailAddress", typeof(string));
            dt.Columns.Add("MobileNumber", typeof(string));
            dt.Columns.Add("scientific_qualification", typeof(string));
            dt.Columns.Add("Gradscientific_qualification", typeof(string));
            dt.Columns.Add("cumulative_other", typeof(string));
            dt.Columns.Add("GPA", typeof(double));
            dt.Columns.Add("Formerscientific_qualification", typeof(string));            
            dt.Columns.Add("FormerGradscientific_qualification", typeof(string));
            dt.Columns.Add("Formercumulative_other", typeof(string));
            dt.Columns.Add("FormerGPA", typeof(double));
            dt.Columns.Add("LastDegree_Id",typeof(Int32));
            dt.Columns.Add("lastdegree", typeof(string));
            dt.Columns.Add("cumulative_Id",typeof(Int32));
            dt.Columns.Add("lastcumulative", typeof(string));
            dt.Columns.Add("formerdegree_Id",typeof(Int32));
            dt.Columns.Add("formerdegree", typeof(string));
            dt.Columns.Add("formercumulative_Id", typeof(Int32));
            dt.Columns.Add("formercumulative", typeof(string));
            dt.Columns.Add("CreatedOn", typeof(DateTime));
            dt.Columns.Add("CityName",typeof(string));
            dt.Columns.Add("Notes",typeof(string));

            //string id = Session["id"].ToString();
            string id = Request.QueryString["id"];
            DataSet dsUserDetails = DAL_User.GetUserDetailsById(id);

            DataRow dr;

            for(int i=0;i<dsUserDetails.Tables[0].Rows.Count;i++)
            {
                dr = dt.NewRow();
                dr["ID"]=dsUserDetails.Tables[0].Rows[i]["ID"].ToString();
                dr["First_Name"] = dsUserDetails.Tables[0].Rows[i]["First_Name"].ToString();
                dr["Second_Name"] = dsUserDetails.Tables[0].Rows[i]["Second_Name"].ToString();
                dr["Third_Name"] = dsUserDetails.Tables[0].Rows[i]["Third_Name"].ToString();
                dr["Last_Name"] = dsUserDetails.Tables[0].Rows[i]["Last_Name"].ToString();
                dr["MobileNumber"] = dsUserDetails.Tables[0].Rows[i]["MobileNumber"].ToString();
                dr["EmailAddress"] = dsUserDetails.Tables[0].Rows[i]["EmailAddress"].ToString();
                dr["scientific_qualification"] = dsUserDetails.Tables[0].Rows[i]["scientific_qualification"].ToString();
                dr["Gradscientific_qualification"] = dsUserDetails.Tables[0].Rows[i]["Gradscientific_qualification"].ToString();
                dr["GPA"]=dsUserDetails.Tables[0].Rows[i]["GPA"].ToString();
                
                if(!string.IsNullOrWhiteSpace(dsUserDetails.Tables[0].Rows[0]["Notes"].ToString()))
                {
                    dr["Notes"] = dsUserDetails.Tables[0].Rows[i]["Notes"].ToString();
                }
                else
                {
                    dr["Notes"] = DBNull.Value;
                }
                if(!string.IsNullOrEmpty(dsUserDetails.Tables[0].Rows[0].ToString()))
                {
                    dr["cumulative_other"] = dsUserDetails.Tables[0].Rows[i]["cumulative_other"].ToString();
                }
                else
                {
                    dr["cumulative_other"] = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(dsUserDetails.Tables[0].Rows[i]["Formercumulative_other"].ToString()))
                {
                    dr["Formercumulative_other"] = dsUserDetails.Tables[0].Rows[i]["Formercumulative_other"].ToString();
                }
                else
                {
                    dr["Formercumulative_other"] = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(dsUserDetails.Tables[0].Rows[i]["Formerscientific_qualification"].ToString()))
                {
                    dr["Formerscientific_qualification"] = dsUserDetails.Tables[0].Rows[i]["Formerscientific_qualification"].ToString();
                }
                else
                {
                    dr["Formerscientific_qualification"] = DBNull.Value;
                }
                if (!string.IsNullOrEmpty(dsUserDetails.Tables[0].Rows[i]["FormerGradscientific_qualification"].ToString()))
                {
                    dr["FormerGradscientific_qualification"] = dsUserDetails.Tables[0].Rows[i]["FormerGradscientific_qualification"].ToString();
                }
                else
                {
                    dr["FormerGradscientific_qualification"] = DBNull.Value;
                }

                if (!string.IsNullOrEmpty(dsUserDetails.Tables[0].Rows[i]["FormerGPA"].ToString()))
                {
                    dr["FormerGPA"] = dsUserDetails.Tables[0].Rows[i]["FormerGPA"].ToString();
                }
                else
                {
                    dr["FormerGPA"] = DBNull.Value;
                }              
                
                dr["cumulative_Id"] =dsUserDetails.Tables[0].Rows[i]["cumulative_Id"].ToString();
                
                if (!string.IsNullOrEmpty(dsUserDetails.Tables[0].Rows[i]["lastcumulative"].ToString()))
                {
                    dr["lastcumulative"] = dsUserDetails.Tables[0].Rows[i]["lastcumulative"].ToString();
                }
                else
                {
                    dr["lastcumulative"] = DBNull.Value;
                }

                if (!string.IsNullOrWhiteSpace(dsUserDetails.Tables[0].Rows[0]["CityName"].ToString()))
                {
                    dr["CityName"] = dsUserDetails.Tables[0].Rows[i]["CityName"].ToString();
                }
                else
                {
                    dr["CityName"] = DBNull.Value;
                }
                //dr["LastDegree_Id"] = dsUserDetails.Tables[0].Rows[i]["LastDegree_Id"].ToString();
                dr["lastdegree"] = dsUserDetails.Tables[0].Rows[i]["lastdegree"].ToString();
                //dr["formerdegree_Id"] = dsUserDetails.Tables[0].Rows[i]["formerdegree_Id"].ToString();              

                if (!string.IsNullOrEmpty(dsUserDetails.Tables[0].Rows[0]["formerdegree"].ToString()))
                {
                    dr["formerdegree"] = dsUserDetails.Tables[0].Rows[i]["formerdegree"].ToString();
                }
                else
                {
                    dr["formerdegree"] = DBNull.Value;
                }
                dr["formercumulative_Id"] = dsUserDetails.Tables[0].Rows[i]["formercumulative_Id"];
                if (!string.IsNullOrEmpty(dsUserDetails.Tables[0].Rows[i]["formercumulative"].ToString()))
                {
                    dr["formercumulative"] = dsUserDetails.Tables[0].Rows[i]["formercumulative"].ToString();
                }
                else
                {
                    dr["formercumulative"] = DBNull.Value;
                }
                dr["formercumulative"] = dsUserDetails.Tables[0].Rows[i]["formercumulative"].ToString();
                //dr["CreatedOn"] = Convert.ToDateTime(dsUserDetails.Tables[0].Rows[i]["CreatedOn"].ToString());
                dt.Rows.Add(dr);
            }
            string Username = Session["User"].ToString();
            rptUserDetails.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            rptUserDetails.LocalReport.ReportPath = Server.MapPath("~/Report/Report.rdlc");
            rptUserDetails.LocalReport.EnableExternalImages = true;
            string image = new Uri(Server.MapPath("~/img/ncms-logo.jpg")).AbsoluteUri;
            ReportParameterCollection param = new ReportParameterCollection();
            param.Add(new ReportParameter("rptImage", image));
            param.Add(new ReportParameter("rptPrintedBy", Username));
            this.rptUserDetails.LocalReport.SetParameters(param);
            Session["UserDetails"] = dt;
            dt = dt.DefaultView.ToTable();
            ReportDataSource rdUserDetails = new ReportDataSource("dsUserDetails", dt);
            rptUserDetails.LocalReport.DataSources.Clear();
            rptUserDetails.LocalReport.DataSources.Add(rdUserDetails);
            rptUserDetails.LocalReport.Refresh();
        }
    }
}