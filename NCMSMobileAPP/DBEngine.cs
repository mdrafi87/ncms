﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace DataAccessLayer
{
    public static class DBEngine
    {   
        #region Static Variable Declaration
        private static SqlConnection _connection;
        #endregion

        static DBEngine()
        {
            _connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NCMSConStr"].ConnectionString);
        }
        #region Open Connection
        public static void OpenConnection()
        {
            if (_connection.State.Equals(ConnectionState.Closed))
                _connection.Open();
        }

        #endregion

        #region Close Connection
        public static void CloseConnection()
        {
            if (_connection.State.Equals(ConnectionState.Open))
                _connection.Close();
        }
        #endregion
        //This will execute with paramaters in stored procedure....
        #region ExecuteScalar
        public static object ExecuteScalar(string _commandText, params SqlParameter[] parameters)
        {
            try
            {
                SqlCommand _command = new SqlCommand(_commandText, _connection);
                _command.CommandType = CommandType.StoredProcedure;
                if (parameters != null)
                {
                    if (parameters.Length > 0)
                    {
                        foreach (SqlParameter parameter in parameters)
                            _command.Parameters.Add(parameters);
                    }
                }
                OpenConnection();
                return _command.ExecuteScalar();
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                CloseConnection();
            }
        }
        public static object ExecuteScalar(string _commandText)
        {
            try
            {
                return ExecuteScalar(_commandText, null);
            }
            catch (Exception)
            {

                throw;
            }

        }
        #endregion
        //The below function without passing parameters to stored procedure.
     
        #region Execute Dataset
        public static DataSet ExecuteDataset(string _commandText, params SqlParameter[] parameters)
        {
            try
            {
                DataSet _ds = new DataSet();
                ConnectionState state = _connection.State;
                SqlCommand _command = new SqlCommand(_commandText, _connection);
                _command.CommandType = CommandType.StoredProcedure;
                if (parameters != null)
                {
                    if (parameters.Length > 0)
                    {
                        foreach (SqlParameter _objSqlParams in parameters)
                            _command.Parameters.Add(_objSqlParams);
                    }
                }
                SqlDataAdapter _da = new SqlDataAdapter(_command);
                _da.Fill(_ds);
                return _ds;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                CloseConnection();
            }
        }
        public static DataSet ExecuteDataset(string _commandText)
        {
            try
            {
                return ExecuteDataset(_commandText, null);
            }
            catch (Exception)
            {

                throw;
            }
           
        }
        #endregion

        #region Execute dataTable
        public static DataTable ExecuteDataTable(string _commandtext, params SqlParameter[] parameters)
        {
            try
            {
                DataTable _dt = new DataTable();
                SqlCommand _command = new SqlCommand(_commandtext,_connection);
                _command.CommandType = CommandType.StoredProcedure;
                if (parameters != null)
                {
                    if (parameters.Length > 0)
                    {
                        foreach (SqlParameter _objSqlParams in parameters)
                            _command.Parameters.Add(_objSqlParams);
                    }
                }
                SqlDataAdapter _ds = new SqlDataAdapter(_command);
                _ds.Fill(_dt);
                return _dt;
            }
            catch (Exception)
            {
                
                throw;
            }           
        }
        public static DataTable ExecuteDataTable(string _commandtext)
        {
            try
            {
                DataTable _dt = new DataTable();
                SqlCommand _command = new SqlCommand(_commandtext, _connection);
                _command.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter _da = new SqlDataAdapter(_command);
                _da.Fill(_dt);
                return _dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static DataTable ExecuteDataTable(SqlCommand _command)
        {
            try
            {
                DataTable _dt = new DataTable();
                //SqlCommand _command = new SqlCommand(_commandtext, _connection);
                _command.Connection = _connection;
                _command.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter _da = new SqlDataAdapter(_command);
                _da.Fill(_dt);
                return _dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Execute Reader
        public static SqlDataReader ExecuteDataReader(string _commandtext, params SqlParameter[] parameters)
        {
            try
            {
                SqlCommand _command = new SqlCommand(_commandtext, _connection);
                _command.CommandType = CommandType.StoredProcedure;
                if (parameters != null)
                {
                    if (parameters.Length > 0)
                    {
                        foreach (SqlParameter _objSQLParams in parameters)
                            _command.Parameters.Add(_objSQLParams);
                    }
                }
                OpenConnection();
                return _command.ExecuteReader();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }


        public static SqlDataReader ExecuteDataReader(string _commandtext)
        {
            try
            {
                return ExecuteDataReader(_commandtext, null);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (_connection.State.Equals(ConnectionState.Open))
                {
                    _connection.Close();

                }
                _connection.Dispose();
            }
        }
        #endregion

        #region Execute Non-Query
        public static int ExecuteNonQuery(string _commandText, params SqlParameter[] parameters)
        {
            try
            {
                SqlCommand _command = new SqlCommand(_commandText, _connection);
                _command.CommandType = CommandType.StoredProcedure;

                if (parameters != null)
                {
                    if (parameters.Length > 0)
                    {
                        foreach (SqlParameter _objSQLParams in parameters)
                            _command.Parameters.Add(_objSQLParams);
                    }
                }
                OpenConnection();
                return _command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public static int ExecuteNonQuery(string _commandText)
        {
            try
            {
                return ExecuteNonQuery(_commandText, null);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }
        #endregion
    }
}
