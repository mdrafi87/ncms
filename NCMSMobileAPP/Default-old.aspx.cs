﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using SettingsLibrary;
using LibraryCore.DAL;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saplin.Controls;
using System.Collections;
using System.Collections.Specialized;
using System.Text;

namespace NCMSMobileAPP
{
    public partial class Default : System.Web.UI.Page
    {
        DataTable dtgridview = null;
        HiddenField hdnViewstate = new HiddenField();
        DataTable dtgridResponse = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                string Username = Session["User"].ToString();
                gvwncms.DataSource = GetUserDetails();
                gvwncms.DataBind();
                
            }
        }
        public DataTable DtGetUserDetails()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID",typeof(Int32));
            dt.Columns.Add("FullName", typeof(string));
            dt.Columns.Add("EmailAddress", typeof(string));
            dt.Columns.Add("scientific_qualification", typeof(string));
            dt.Columns.Add("Gradscientific_qualification", typeof(string));
            dt.Columns.Add("cumulative_other", typeof(string));
            dt.Columns.Add("GPA", typeof(Int32));
            dt.Columns.Add("Formerscientific_qualification", typeof(string));
            dt.Columns.Add("FormerGradscientific_qualification", typeof(string));
            dt.Columns.Add("Formercumulative_other", typeof(string));
            dt.Columns.Add("FormerGPA", typeof(Int32));
            dt.Columns.Add("lastdegree", typeof(string));
            dt.Columns.Add("formerdegree", typeof(string));
            dt.Columns.Add("lastcumulative", typeof(string));
            dt.Columns.Add("formercumulative", typeof(string));
            dt.Columns.Add("CreatedOn", typeof(DateTime));
            return dt;
        }
        public DataView GetUserDetails()
        {
            DataView dv = new DataView();
            DataSet dsUserDetails = DAL_User.getUserDetails();
            DataTable dt = dsUserDetails.Tables[0];
            gvwncms.DataSource = dt;
            gvwncms.DataBind();          
           
            DataTable dtfullname = new DataTable();
            DataTable dtDegree = new DataTable();            
            DataTable dtSQ = new DataTable();
            DataTable dtEmail = new DataTable();
            DataTable dtGq = new DataTable();
            DataTable dtmobile = new DataTable();
            DataTable dtGpa = new DataTable();
            DataTable dtfsq = new DataTable();
            DataTable dtfgq = new DataTable();
            DataTable dtFormarDegree = new DataTable();
            DataTable dtlastcumulative = new DataTable();
            DataTable dtCreatedOn = new DataTable();

            dtfullname = dt.DefaultView.ToTable(true, "FullName");
            dtDegree = dt.DefaultView.ToTable(true, "lastdegree");
            dtSQ = dt.DefaultView.ToTable(true, "scientific_qualification");
            dtEmail = dt.DefaultView.ToTable(true, "EmailAddress");
            dtGq = dt.DefaultView.ToTable(true, "Gradscientific_qualification");
            dtmobile = dt.DefaultView.ToTable(true, "MobileNumber");
            dtGpa = dt.DefaultView.ToTable(true, "GPA");
            dtfsq = dt.DefaultView.ToTable(true, "Formerscientific_qualification");
            dtfgq = dt.DefaultView.ToTable(true, "FormerGradscientific_qualification");
            dtFormarDegree = dt.DefaultView.ToTable(true, "formerdegree");
            dtlastcumulative = dt.DefaultView.ToTable(true, "lastcumulative");
            dtCreatedOn = dt.DefaultView.ToTable(true, "CreatedOn");
            

            dtfullname.DefaultView.Sort = "FullName asc";
            dtDegree.DefaultView.Sort = "lastdegree asc";
            dtSQ.DefaultView.Sort = "scientific_qualification asc";
            dtEmail.DefaultView.Sort = "EmailAddress asc";
            dtGq.DefaultView.Sort = "Gradscientific_qualification asc";
            dtmobile.DefaultView.Sort = "MobileNumber asc";
            dtGpa.DefaultView.Sort = "GPA asc";
            dtfsq.DefaultView.Sort = "Formerscientific_qualification asc";
            dtfgq.DefaultView.Sort = "FormerGradscientific_qualification asc";
            dtFormarDegree.DefaultView.Sort = "formerdegree asc";
            dtlastcumulative.DefaultView.Sort = "lastcumulative asc";
            dtCreatedOn.DefaultView.Sort = "CreatedOn asc";

            //dropdown for fullName
            DropDownCheckBoxes fullName = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlfullName");
            fullName.DataSource = dtfullname.DefaultView;
            fullName.DataTextField = "FullName";
            fullName.DataBind();
            fullName.Style.DropDownBoxBoxWidth = new Unit(160);
            fullName.SelectedIndex = -1;

            //dropdown for lstdegree
            DropDownCheckBoxes lastDegree = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlDegree");
            lastDegree.DataSource = dtDegree.DefaultView;
            lastDegree.DataTextField = "lastdegree";
            lastDegree.DataBind();
            lastDegree.Style.DropDownBoxBoxWidth = new Unit(160);
            lastDegree.SelectedIndex = -1;

            //dropdown for Scientific Qualification
            DropDownCheckBoxes squalification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlSQ");
            squalification.DataSource = dtSQ.DefaultView;
            squalification.DataTextField = "scientific_qualification";
            squalification.DataBind();
            squalification.Style.DropDownBoxBoxWidth = new Unit(160);
            squalification.SelectedIndex = -1;

            //dropdown for Email
            DropDownCheckBoxes Email = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlEmail");
            Email.DataSource = dtEmail.DefaultView;
            Email.DataTextField = "EmailAddress";
            Email.DataBind();
            Email.Style.DropDownBoxBoxWidth = new Unit(160);
            Email.SelectedIndex = -1;

            //dropdown for Gradscientific_qualification
            DropDownCheckBoxes Gqualification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlGQ");
            Gqualification.DataSource = dtGq.DefaultView;
            Gqualification.DataTextField = "Gradscientific_qualification";
            Gqualification.DataBind();
            Gqualification.Style.DropDownBoxBoxWidth = new Unit(160);
            Gqualification.SelectedIndex = -1;

            //dropdown for Mobile Number
            DropDownCheckBoxes MobileNumber = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlMobileNumber");
            MobileNumber.DataSource = dtmobile.DefaultView;
            MobileNumber.DataTextField = "MobileNumber";
            MobileNumber.DataBind();
            MobileNumber.Style.DropDownBoxBoxWidth = new Unit(160);
            MobileNumber.SelectedIndex = -1;

            //dropdown for GPA
            DropDownCheckBoxes gpa = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlGpa");
            gpa.DataSource = dtGpa.DefaultView;
            gpa.DataTextField = "GPA";
            gpa.DataBind();
            gpa.Style.DropDownBoxBoxWidth = new Unit(160);
            gpa.SelectedIndex = -1;

            //for Formerscientific Qualification
            DropDownCheckBoxes FSQualification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlfsq");
            FSQualification.DataSource = dtfsq.DefaultView;
            FSQualification.DataTextField = "Formerscientific_qualification";
            FSQualification.DataBind();
            FSQualification.Style.DropDownBoxBoxWidth = new Unit(160);
            FSQualification.SelectedIndex = -1;

            //for FGQualificarion
            DropDownCheckBoxes FGQualification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlfgq");
            FGQualification.DataSource = dtfgq.DefaultView;
            FGQualification.DataTextField = "FormerGradscientific_qualification";
            FGQualification.DataBind();
            FGQualification.Style.DropDownBoxBoxWidth = new Unit(160);
            FGQualification.SelectedIndex = -1;
            //for formar degree
            DropDownCheckBoxes formardegree = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFDegree");
            formardegree.DataSource = dtFormarDegree.DefaultView;
            formardegree.DataTextField = "formerdegree";
            formardegree.DataBind();
            formardegree.Style.DropDownBoxBoxWidth = new Unit(160);
            formardegree.SelectedIndex = -1;

            //dropdown for lastcumulative
            DropDownCheckBoxes lastcumulative = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFormarcumulative");
            lastcumulative.DataSource = dtlastcumulative.DefaultView;
            lastcumulative.DataTextField = "lastcumulative";
            lastcumulative.DataBind();
            lastcumulative.Style.DropDownBoxBoxWidth = new Unit(160);
            lastcumulative.SelectedIndex = -1;

            //filter based on date
            DropDownCheckBoxes CreatedOn = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddldate");          
            CreatedOn.DataSource = dtCreatedOn.DefaultView;
            CreatedOn.DataTextField = "CreatedOn";
            CreatedOn.DataBind();
            CreatedOn.Style.DropDownBoxBoxWidth = new Unit(160);
            CreatedOn.SelectedIndex = -1;

            dv = dt.DefaultView;
            if (dv.Count != 0)
            {
                dv = dt.DefaultView;
            }
            else
            {
                dv = new DataView(dt);

                btnClick.Value = "False";
            }
            int dd = dv.Count;
            return dv;
            }
     
        public void UpdateDropDownChckBoxBinding()
        {
            if(Session["Dictionary"]!=null)
            {
                dict = (OrderedDictionary)Session["Dictionary"];
            }
            if (Session["UserDetails"] == null)
            {
                gvwncms.DataSource = dtgridview;
                gvwncms.DataBind();
                dtgridview = (DataTable)Session["UserDetails"];
            }
            else
            {
                dtgridview = (DataTable)Session["UserDetails"];
            }
            //ddl fullname
            DropDownCheckBoxes fullName = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlfullName");
            DataTable dtfullName = dtgridview.DefaultView.ToTable(true,"FullName");
            dtfullName.DefaultView.Sort = "FullName asc";
            fullName.DataSource = dtfullName.DefaultView;
            ViewState["fnType"] = dtfullName;
            fullName.DataTextField = "FullName";
            fullName.DataBind();
            fullName.Style.DropDownBoxBoxWidth = new Unit(160);

            //ddl degree
            DropDownCheckBoxes Degree = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlDegree");
            DataTable dtDegree = dtgridview.DefaultView.ToTable(true,"lastdegree");
            dtDegree.DefaultView.Sort = "lastdegree asc";
            Degree.DataSource = dtDegree.DefaultView;
            ViewState["ldType"] = dtDegree;
            Degree.DataTextField = "lastdegree";
            Degree.DataBind();
            Degree.Style.DropDownBoxBoxWidth = new Unit(160);

            //ddl scintific Qualification
            DropDownCheckBoxes ScintificQualification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlSQ");
            DataTable dtSq = dtgridview.DefaultView.ToTable(true, "scientific_qualification");
            dtSq.DefaultView.Sort = "scientific_qualification asc";
            ScintificQualification.DataSource = dtSq.DefaultView;
            ViewState["SqType"] = dtSq;
            ScintificQualification.DataTextField = "scientific_qualification";
            ScintificQualification.DataBind();
            ScintificQualification.Style.DropDownBoxBoxWidth = new Unit(160);

            //for Email Address
            DropDownCheckBoxes Email = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlEmail");
            DataTable dtEmail = dtgridview.DefaultView.ToTable(true, "EmailAddress");
            dtEmail.DefaultView.Sort = "EmailAddress asc";
            Email.DataSource = dtEmail.DefaultView;
            ViewState["EAType"] = dtEmail;
            Email.DataTextField = "EmailAddress";
            Email.DataBind();
            Email.Style.DropDownBoxBoxWidth = new Unit(160);

            //for Gradscientific_qualification
            DropDownCheckBoxes GradscientificQualification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlGQ");
            DataTable dtGq = dtgridview.DefaultView.ToTable(true, "Gradscientific_qualification");
            dtGq.DefaultView.Sort = "Gradscientific_qualification asc";
            GradscientificQualification.DataSource = dtGq.DefaultView;
            ViewState["GqType"] = dtGq;
            GradscientificQualification.DataTextField = "Gradscientific_qualification";
            GradscientificQualification.DataBind();
            GradscientificQualification.Style.DropDownBoxBoxWidth = new Unit(160);

            //mobilenumber
            DropDownCheckBoxes mobilenumber = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlMobileNumber");
            DataTable dtMobile = dtgridview.DefaultView.ToTable(true, "MobileNumber");
            dtMobile.DefaultView.Sort = "MobileNumber asc";
            mobilenumber.DataSource = dtMobile.DefaultView;
            ViewState["MNType"] = dtMobile;
            mobilenumber.DataTextField = "MobileNumber";
            mobilenumber.DataBind();
            mobilenumber.Style.DropDownBoxBoxWidth = new Unit(160);


            //for GPA
            DropDownCheckBoxes GPA = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlGpa");
            DataTable dtgpa = dtgridview.DefaultView.ToTable(true, "GPA");
            dtgpa.DefaultView.Sort = "GPA asc";
            GPA.DataSource = dtgpa.DefaultView;
            ViewState["GPAType"] = dtgpa;
            GPA.DataTextField = "GPA";
            GPA.DataBind();
            GPA.Style.DropDownBoxBoxWidth = new Unit(160);

            //for FSQ
            DropDownCheckBoxes FSQualification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlfsq");
            DataTable dtfsq = dtgridview.DefaultView.ToTable(true, "Formerscientific_qualification");
            dtfsq.DefaultView.Sort = "Formerscientific_qualification asc";
            FSQualification.DataSource = dtfsq.DefaultView;
            ViewState["fsqType"] = dtfsq;
            FSQualification.DataTextField = "Formerscientific_qualification";
            FSQualification.DataBind();
            FSQualification.Style.DropDownBoxBoxWidth = new Unit(160);

            //for FGQ
            DropDownCheckBoxes FGQualification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlfgq");
            DataTable dtfgq = dtgridview.DefaultView.ToTable(true, "FormerGradscientific_qualification");
            dtfgq.DefaultView.Sort = "FormerGradscientific_qualification asc";
            FGQualification.DataSource = dtfgq.DefaultView;
            ViewState["fgqType"] = dtfgq;
            FGQualification.DataTextField = "FormerGradscientific_qualification";
            FGQualification.DataBind();
            FGQualification.Style.DropDownBoxBoxWidth = new Unit(160);

            //for formar degree
            DropDownCheckBoxes formardegree = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFDegree");
            DataTable dtformardegree = dtgridview.DefaultView.ToTable(true, "formerdegree");
            dtformardegree.DefaultView.Sort = "formerdegree asc";
            formardegree.DataSource = dtformardegree.DefaultView;
            ViewState["fdType"] = dtformardegree;
            formardegree.DataTextField = "formerdegree";
            formardegree.DataBind();
            formardegree.Style.DropDownBoxBoxWidth = new Unit(160);

            //for cumulative Degree
            DropDownCheckBoxes cumulativeDegree = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFormarcumulative");
            DataTable dtcumulativedegree = dtgridview.DefaultView.ToTable(true, "lastcumulative");
            dtcumulativedegree.DefaultView.Sort = "lastcumulative asc";
            cumulativeDegree.DataSource = dtcumulativedegree.DefaultView;
            ViewState["lcType"] = dtcumulativedegree;
            cumulativeDegree.DataTextField = "lastcumulative";
            cumulativeDegree.DataBind();
            cumulativeDegree.Style.DropDownBoxBoxWidth = new Unit(160);

            //for Dates
            DropDownCheckBoxes date = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddldate");
            DataTable dtCreatedon = dtgridview.DefaultView.ToTable(true, "CreatedOn");
            dtCreatedon.DefaultView.Sort = "CreatedOn asc";            
            date.DataSource = dtCreatedon.DefaultView;
            ViewState["DType"] = dtCreatedon;
            date.DataTextField = "CreatedOn";
            date.DataTextFormatString = "{0:dd/MM/yyyy}";
            date.DataBind();
            date.Style.DropDownBoxBoxWidth = new Unit(160);

            //for fullname
            if(fn>0)
            {
                if(_fullNameArr.Count>fullName.Items.Count)
                {
                    string FNCoulmnName = "FullName";
                    DataTable newFN = dtfullName.Clone();
                    for(int j=0; j<=_fullNameArr.Count - 1 ;j++)
                    {
                        string compareValue = Convert.ToString(_fullNameArr[j]).Trim();
                        string sqlFilter = string.Format("{0} Like '{1}'",FNCoulmnName,compareValue);
                        DataRow[] dataRowArray = dtfullName.Select(sqlFilter);

                        if(dataRowArray.Length>0)
                        {
                            if(dict.Contains(compareValue))
                            {

                            }
                            else
                            {
                                fn = fn - 1;
                                dict.Remove(compareValue);
                            }
                        }
                        else
                        {
                            fn = fn - 1;
                            dict.Remove(compareValue);
                        }
                    }
                }
                for(int i=0;i<fullName.Items.Count;i++)
                {
                    fullName.Items[i].Selected = true;
                    display = display + fullName.Items[i] + "+";
                }
            }

            //for degree
            if(ld>0)
            {
               if(_degreeArr.Count>Degree.Items.Count)
               {
                   string LDColumnName = "lastdegree";
                   DataTable newLD = dtDegree.Clone();
                   for (int j = 0; j <= _degreeArr.Count - 1; j++)
                   {
                       string CompareValueLD = Convert.ToString(_degreeArr[j]).Trim();
                       string filterLD = string.Format("{0}Like '{1}'",LDColumnName,CompareValueLD);
                       DataRow[] dataRowLD=dtDegree.Select(filterLD);
                       if(dataRowLD.Length>0)
                       {
                           if(dict.Contains(CompareValueLD))
                           {

                           }
                           else
                           {
                               ld = ld - 1;
                               dict.Remove(CompareValueLD);
                           }
                       }
                       else
                       {
                           ld = ld - 1;
                           dict.Remove(CompareValueLD);
                       }
                   }
               }
                for(int i=0;i<Degree.Items.Count;i++)
                {
                    Degree.Items[i].Selected = true;
                    display1 = display1 + Degree.Items[i] + "+";
                }
            }
            //scintific qualification
            if (sq > 0)
            {
                if (_ScintificQualificationArr.Count > ScintificQualification.Items.Count)
                {
                    string sqColumnName = "scientific_qualification";
                    DataTable newSQ = dtSq.Clone();
                    for (int j = 0; j <= _ScintificQualificationArr.Count - 1; j++)
                    {
                        string CompareValueSQ = Convert.ToString(_ScintificQualificationArr[j]).Trim();
                        string filterSQ = string.Format("{0}Like '{1}'", sqColumnName, CompareValueSQ);
                        DataRow[] dataRowSQ = dtDegree.Select(filterSQ);
                        if (dataRowSQ.Length > 0)
                        {
                            if (dict.Contains(CompareValueSQ))
                            {

                            }
                            else
                            {
                                sq = sq - 1;
                                dict.Remove(CompareValueSQ);
                            }
                        }
                        else
                        {
                            sq = sq - 1;
                            dict.Remove(CompareValueSQ);
                        }
                    }
                }
                for (int i = 0; i < ScintificQualification.Items.Count; i++)
                {
                    ScintificQualification.Items[i].Selected = true;
                    display2 = display2 + ScintificQualification.Items[i] + "+";
                }
            }
            //
            //for EmailAddress
            if (ea > 0)
            {
                if (_EmailArr.Count > Email.Items.Count)
                {
                    string EAColumnName = "scientific_qualification";
                    DataTable newEa = dtSq.Clone();
                    for (int j = 0; j <= _EmailArr.Count - 1; j++)
                    {
                        string CompareValueEa = Convert.ToString(_EmailArr[j]).Trim();
                        string filterSQ = string.Format("{0}Like '{1}'", EAColumnName, CompareValueEa);
                        DataRow[] dataRowEa = dtDegree.Select(filterSQ);
                        if (dataRowEa.Length > 0)
                        {
                            if (dict.Contains(CompareValueEa))
                            {

                            }
                            else
                            {
                                ea = ea - 1;
                                dict.Remove(CompareValueEa);
                            }
                        }
                        else
                        {
                            ea = ea - 1;
                            dict.Remove(CompareValueEa);
                        }
                    }
                }
                for (int i = 0; i < Email.Items.Count; i++)
                {
                    Email.Items[i].Selected = true;
                    display3 = display3 + Email.Items[i] + "+";
                }
            }
            //end
            //for GradscientificQualification
            if (gq > 0)
            {
                if (_GradscientificQualificationArr.Count > GradscientificQualification.Items.Count)
                {
                    string GqColumnName = "Gradscientific_qualification";
                    DataTable newGQ = dtGq.Clone();
                    for (int j = 0; j <= _GradscientificQualificationArr.Count - 1; j++)
                    {
                        string CompareValueGQ = Convert.ToString(_GradscientificQualificationArr[j]).Trim();
                        string filterGQ = string.Format("{0}Like '{1}'", GqColumnName, CompareValueGQ);
                        DataRow[] dataRowGQ = dtGq.Select(filterGQ);
                        if (dataRowGQ.Length > 0)
                        {
                            if (dict.Contains(CompareValueGQ))
                            {

                            }
                            else
                            {
                                gq = gq - 1;
                                dict.Remove(CompareValueGQ);
                            }
                        }
                        else
                        {
                            gq = gq - 1;
                            dict.Remove(CompareValueGQ);
                        }
                    }
                }
                for (int i = 0; i < GradscientificQualification.Items.Count; i++)
                {
                    GradscientificQualification.Items[i].Selected = true;
                    display4 = display4 + GradscientificQualification.Items[i] + "+";
                }
            }
            //end
            //mobile
            if (mn > 0)
            {
                if (_mobile.Count > mobilenumber.Items.Count)
                {
                    string MNColumnName = "MobileNumber";
                    DataTable newMN = dtMobile.Clone();
                    for (int j = 0; j <= _mobile.Count - 1; j++)
                    {
                        string CompareValueMN = Convert.ToString(_mobile[j]).Trim();
                        string filterMN = string.Format("{0}Like '{1}'", MNColumnName, CompareValueMN);
                        DataRow[] dataRowMN = dtMobile.Select(filterMN);
                        if (dataRowMN.Length > 0)
                        {
                            if (dict.Contains(CompareValueMN))
                            {

                            }
                            else
                            {
                                mn = mn - 1;
                                dict.Remove(CompareValueMN);
                            }
                        }
                        else
                        {
                            mn = mn - 1;
                            dict.Remove(CompareValueMN);
                        }
                    }
                }
                for (int i = 0; i < mobilenumber.Items.Count; i++)
                {
                    mobilenumber.Items[i].Selected = true;
                    display5 = display5 + mobilenumber.Items[i] + "+";
                }
            }
            //end
            //for GPA
            if (gpa > 0)
            {
                if (_Gpa.Count > GPA.Items.Count)
                {
                    string gpaColumnName = "GPA";
                    DataTable newgpa = dtgpa.Clone();
                    for (int j = 0; j <= _Gpa.Count - 1; j++)
                    {
                        string CompareValuegpa = Convert.ToString(_Gpa[j]).Trim();
                        string filtergpa = string.Format("{0}Like '{1}'", gpaColumnName, CompareValuegpa);
                        DataRow[] dataRowgpa = dtgpa.Select(filtergpa);
                        if (dataRowgpa.Length > 0)
                        {
                            if (dict.Contains(CompareValuegpa))
                            {

                            }
                            else
                            {
                                gpa = gpa - 1;
                                dict.Remove(CompareValuegpa);
                            }
                        }
                        else
                        {
                            gpa = gpa - 1;
                            dict.Remove(CompareValuegpa);
                        }
                    }
                }
                for (int i = 0; i < GPA.Items.Count; i++)
                {
                    GPA.Items[i].Selected = true;
                    display6 = display6 + GPA.Items[i] + "+";
                }
            }
            //End

            //for FSQ
            if (fsq > 0)
            {
                if (_FSQualification.Count > FSQualification.Items.Count)
                {
                    string fsqColumnName = "Formerscientific_qualification";
                    DataTable newfsq = dtfsq.Clone();
                    for (int j = 0; j <= _FSQualification.Count - 1; j++)
                    {
                        string CompareValuefsq = Convert.ToString(_FSQualification[j]).Trim();
                        string filterfsq = string.Format("{0}Like '{1}'", fsqColumnName, CompareValuefsq);
                        DataRow[] dataRowfsq = dtfsq.Select(filterfsq);
                        if (dataRowfsq.Length > 0)
                        {
                            if (dict.Contains(CompareValuefsq))
                            {

                            }
                            else
                            {
                                fsq = fsq - 1;
                                dict.Remove(CompareValuefsq);
                            }
                        }
                        else
                        {
                            fsq = fsq - 1;
                            dict.Remove(CompareValuefsq);
                        }
                    }
                }
                for (int i = 0; i < FSQualification.Items.Count; i++)
                {
                    FSQualification.Items[i].Selected = true;
                    display7 = display7 + FSQualification.Items[i] + "+";
                }
            }
            //end
            //for fgq
            if (fgq > 0)
            {
                if (_FGQualification.Count > FGQualification.Items.Count)
                {
                    string fgqColumnName = "FormerGradscientific_qualification";
                    DataTable newfgq = dtfgq.Clone();
                    for (int j = 0; j <= _FGQualification.Count - 1; j++)
                    {
                        string CompareValuefgq = Convert.ToString(_FGQualification[j]).Trim();
                        string filterfsq = string.Format("{0}Like '{1}'", fgqColumnName, CompareValuefgq);
                        DataRow[] dataRowfgq = dtfgq.Select(filterfsq);
                        if (dataRowfgq.Length > 0)
                        {
                            if (dict.Contains(CompareValuefgq))
                            {

                            }
                            else
                            {
                                fgq = fgq - 1;
                                dict.Remove(CompareValuefgq);
                            }
                        }
                        else
                        {
                            fgq = fgq - 1;
                            dict.Remove(CompareValuefgq);
                        }
                    }
                }
                for (int i = 0; i < FGQualification.Items.Count; i++)
                {
                    FGQualification.Items[i].Selected = true;
                    display8 = display8 + FGQualification.Items[i] + "+";
                }
            }
            //end
            //for formar degree
            if (fd > 0)
            {
                if (_formardegree.Count > formardegree.Items.Count)
                {
                    string FDColumnName = "formerdegree";
                    DataTable newffd = dtformardegree.Clone();
                    for (int j = 0; j <= _formardegree.Count - 1; j++)
                    {
                        string CompareValuefd = Convert.ToString(_FGQualification[j]).Trim();
                        string filterfd = string.Format("{0}Like '{1}'", FDColumnName, CompareValuefd);
                        DataRow[] dataRowfd = dtformardegree.Select(filterfd);
                        if (dataRowfd.Length > 0)
                        {
                            if (dict.Contains(CompareValuefd))
                            {

                            }
                            else
                            {
                                fd = fd - 1;
                                dict.Remove(CompareValuefd);
                            }
                        }
                        else
                        {
                            fd = fd - 1;
                            dict.Remove(CompareValuefd);
                        }
                    }
                }
                for (int i = 0; i < formardegree.Items.Count; i++)
                {
                    formardegree.Items[i].Selected = true;
                    display9 = display9 + formardegree.Items[i] + "+";
                }
            }
            //end
            //cumulative degree
            if (lc > 0)
            {
                if (_lastcumulative.Count > cumulativeDegree.Items.Count)
                {
                    string lcColumnName = "lastcumulative";
                    DataTable newflc = dtcumulativedegree.Clone();
                    for (int j = 0; j <= _lastcumulative.Count - 1; j++)
                    {
                        string CompareValuelc = Convert.ToString(_lastcumulative[j]).Trim();
                        string filterlc = string.Format("{0}Like '{1}'", lcColumnName, CompareValuelc);
                        DataRow[] dataRowlc = dtformardegree.Select(filterlc);
                        if (dataRowlc.Length > 0)
                        {
                            if (dict.Contains(CompareValuelc))
                            {

                            }
                            else
                            {
                                lc = lc - 1;
                                dict.Remove(CompareValuelc);
                            }
                        }
                        else
                        {
                            lc = lc - 1;
                            dict.Remove(CompareValuelc);
                        }
                    }
                }
                for (int i = 0; i < cumulativeDegree.Items.Count; i++)
                {
                    cumulativeDegree.Items[i].Selected = true;
                    display10 = display10 + cumulativeDegree.Items[i] + "+";
                }
            }
            //end
            //for date
            if (co > 0)
            {
                if (_CreatedOn.Count > date.Items.Count)
                {
                    string coColumnName = "CreatedOn";
                    DataTable newfco = dtCreatedon.Clone();
                    for (int j = 0; j <= _CreatedOn.Count - 1; j++)
                    {
                        string CompareValueco = Convert.ToString(_CreatedOn[j]).Trim();
                        string filterco = string.Format("{0}Like '{1}'", coColumnName, CompareValueco);
                        DataRow[] dataRowco = dtCreatedon.Select(filterco);
                        if (dataRowco.Length > 0)
                        {
                            if (dict.Contains(CompareValueco))
                            {

                            }
                            else
                            {
                                co = co - 1;
                                dict.Remove(CompareValueco);
                            }
                        }
                        else
                        {
                            co = co - 1;
                            dict.Remove(CompareValueco);
                        }
                    }
                }
                for (int i = 0; i < date.Items.Count; i++)
                {
                    date.Items[i].Selected = true;
                    display11 = display11 + date.Items[i] + "+";
                }
            }

            //end
            int child = dtgridview.DefaultView.Count;

            if (fn > 0 || ld > 0 || sq > 0 || ea > 0 || gq > 0 || mn > 0 || gpa > 0 || fsq > 0 || fgq > 0 ||fd>0 ||lc>0 ||co>0)
            {
                lblRequestsCount.Text = "(" + child + ")";
                string strHeader = string.Empty;

                foreach(DictionaryEntry de in dict)
                {
                    strHeader = strHeader + de.Key.ToString().Trim() + "+";
                }
                lblfilterData.Text = strHeader;
            }
            else
            {
                lblfilterData.Text = string.Empty;
                lblRequestsCount.Text = "(" + child + ")";
            }
        }
        public void ViewstateDropdown()
        {
            hdnViewstate.Value = "FromViewState";
        }

        string _filter = string.Empty; string _filter1 = string.Empty; string _filter2 = string.Empty; string _filter3 = string.Empty; string _filter4 = string.Empty;
        string _filter5 = string.Empty; string _filter6 = string.Empty; string _filter7 = string.Empty; string _filter8 = string.Empty; string _filter9 = string.Empty;
        string _filter10 = string.Empty; string _filter11 = string.Empty;        
        string display; string display1; string display2; string display3; string display4; string display5; string display6; string display7; string display8; string display9;
        string display10; string display11;
        static ArrayList _fullNameArr = new ArrayList(); static ArrayList _degreeArr = new ArrayList();
        static ArrayList _ScintificQualificationArr = new ArrayList(); static ArrayList _EmailArr = new ArrayList(); static ArrayList _GradscientificQualificationArr = new ArrayList();
        static ArrayList _mobile = new ArrayList(); static ArrayList _Gpa = new ArrayList(); static ArrayList _FSQualification = new ArrayList();
        static ArrayList _formardegree = new ArrayList(); static ArrayList _lastcumulative = new ArrayList();
        static ArrayList _FGQualification = new ArrayList(); static ArrayList _CreatedOn = new ArrayList();
        static ArrayList _CategoryArrays = new ArrayList();

        OrderedDictionary dict = new OrderedDictionary();
        static int fn = 0, ld = 0,sq=0,ea=0,gq=0,mn=0,gpa=0,fsq=0,fgq=0,fd=0,lc=0,co=0;
        static string totalFilter;
        protected void fullName_IndexChanged(object sender,EventArgs e)
        {
            try 
            {
                if(hdnViewstate.Value=="FromViewState")
                {

                }
                else
                {
                    string xyz = btnClick.Value;
                    if(xyz!="true")
                    {
                        StringBuilder str = new StringBuilder();
                        string _o = ",";
                        if(Session["Dictionary"]!=null)
                        {
                            dict = (OrderedDictionary)Session["Dictionary"];
                        }
                        DropDownCheckBoxes ddlsender = (DropDownCheckBoxes)sender;
                        string s = ddlsender.Texts.SelectBoxCaption;
                        if(ddlsender.SelectedIndex!=-1)
                        {
                            if(ddlsender.ValidationGroup=="p1")
                            {
                                fn = 0; ld = 0; sq = 0; ea = 0; gq = 0; gpa = 0; fsq = 0; fgq = 0; fd = 0; lc = 0; co = 0; totalFilter = "";
                                _fullNameArr.Clear(); _degreeArr.Clear(); _CategoryArrays.Clear(); _ScintificQualificationArr.Clear();
                                _GradscientificQualificationArr.Clear(); _mobile.Clear(); _Gpa.Clear();
                                _EmailArr.Clear(); _FSQualification.Clear(); _FGQualification.Clear();
                                _formardegree.Clear(); _lastcumulative.Clear(); _CreatedOn.Clear();

                                DropDownCheckBoxes ddlFullName = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlfullName");
                                string _or = " OR ";
                                string _and = " AND ";

                                foreach (ListItem item in (ddlFullName as ListControl).Items)
                                {
                                    string i = item.Text;
                                    if (item.Selected)
                                    {
                                        _filter = _filter + _or + "FullName Like '" + item.Text.Trim() + "'";
                                        if (_filter.StartsWith(_or))
                                        {
                                            _filter = _filter.Replace(_or, "");
                                            display = display + item.Text.Trim();
                                        }
                                        fn++;
                                        _fullNameArr.Add(item.Value);
                                        if(dict.Contains(item.Text.Trim()))
                                        {
                                            dict[item.Text.Trim()] = "FullName";
                                        }
                                        else
                                        {
                                            dict.Add(item.Text.Trim(),"FullName");
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter)) _filter = "(" + _filter + ")" + _and;
                                DropDownCheckBoxes ddlDegree = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlDegree");

                                foreach (ListItem item in (ddlDegree as ListControl).Items)
                                {
                                    if (item.Selected)
                                    {
                                        _filter1 = _filter1 + _or + "lastdegree Like'" + item.Text.Trim() + "'";
                                        if (_filter1.StartsWith(_or))
                                        {
                                            _filter1 = _filter1.Replace(_or, "");
                                            display1 = display1 + item.Text.Trim();
                                        }
                                        ld++;
                                        _degreeArr.Add(item.Value);
                                        if(dict.Contains(item.Text.Trim()))
                                        {
                                            dict[item.Text.Trim()] = "lastdegree";
                                        }
                                        else
                                        {
                                            dict.Add(item.Text.Trim(), "lastdegree");
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter1)) _filter1 = "(" + _filter1 + ")" + _and;
                                //for Scientific Qualification
                                DropDownCheckBoxes ddlScintificQualification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlSQ");

                                foreach (ListItem item in (ddlScintificQualification as ListControl).Items)
                                {
                                    if (item.Selected)
                                    {
                                        _filter2 = _filter2 + _or + "scientific_qualification Like'" + item.Text.Trim() + "'";
                                        if (_filter2.StartsWith(_or))
                                        {
                                            _filter2 = _filter2.Replace(_or, "");
                                            display2 = display2 + item.Text.Trim();
                                        }
                                        sq++;
                                        _ScintificQualificationArr.Add(item.Value);
                                        if (dict.Contains(item.Text.Trim()))
                                        {
                                            dict[item.Text.Trim()] = "scientific_qualification";
                                        }
                                        else
                                        {
                                            dict.Add(item.Text.Trim(), "scientific_qualification");
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter2)) _filter2 = "(" + _filter2 + ")" + _and;

                                //for Email Address
                                DropDownCheckBoxes ddlEmail = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlEmail");

                                foreach (ListItem item in (ddlEmail as ListControl).Items)
                                {
                                    if (item.Selected)
                                    {
                                        _filter3 = _filter3 + _or + "EmailAddress Like'" + item.Text.Trim() + "'";
                                        if (_filter3.StartsWith(_or))
                                        {
                                            _filter3 = _filter3.Replace(_or, "");
                                            display3 = display3 + item.Text.Trim();
                                        }
                                        ea++;
                                        _EmailArr.Add(item.Value);
                                        if (dict.Contains(item.Text.Trim()))
                                        {
                                            dict[item.Text.Trim()] = "EmailAddress";
                                        }
                                        else
                                        {
                                            dict.Add(item.Text.Trim(), "EmailAddress");
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter3)) _filter3 = "(" + _filter3 + ")" + _and;
                                //
                                //for GQualification
                                DropDownCheckBoxes ddlGqualification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlGQ");

                                foreach (ListItem item in (ddlGqualification as ListControl).Items)
                                {
                                    if (item.Selected)
                                    {
                                        _filter4 = _filter4 + _or + "Gradscientific_qualification Like'" + item.Text.Trim() + "'";
                                        if (_filter4.StartsWith(_or))
                                        {
                                            _filter4 = _filter4.Replace(_or, "");
                                            display4 = display4 + item.Text.Trim();
                                        }
                                        gq++;
                                        _GradscientificQualificationArr.Add(item.Value);
                                        if (dict.Contains(item.Text.Trim()))
                                        {
                                            dict[item.Text.Trim()] = "Gradscientific_qualification";
                                        }
                                        else
                                        {
                                            dict.Add(item.Text.Trim(), "Gradscientific_qualification");
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter4)) _filter4 = "(" + _filter4 + ")" + _and;
                                //end

                                //mobile
                                DropDownCheckBoxes ddlMobile = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlMobileNumber");

                                foreach (ListItem item in (ddlMobile as ListControl).Items)
                                {
                                    if (item.Selected)
                                    {
                                        _filter5 = _filter5 + _or + "MobileNumber Like'" + item.Text.Trim() + "'";
                                        if (_filter5.StartsWith(_or))
                                        {
                                            _filter5 = _filter5.Replace(_or, "");
                                            display5 = display5 + item.Text.Trim();
                                        }
                                        mn++;
                                        _mobile.Add(item.Value);
                                        if (dict.Contains(item.Text.Trim()))
                                        {
                                            dict[item.Text.Trim()] = "MobileNumber";
                                        }
                                        else
                                        {
                                            dict.Add(item.Text.Trim(), "MobileNumber");
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter5)) _filter5 = "(" + _filter5 + ")" + _and;

                                //end

                                //gpa

                                DropDownCheckBoxes ddlGpa = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlGpa");

                                foreach (ListItem item in (ddlGpa as ListControl).Items)
                                {
                                    if (item.Selected)
                                    {
                                        _filter6 = _filter6 + _or + "GPA Like'" + item.Text.Trim() + "'";
                                        if (_filter6.StartsWith(_or))
                                        {
                                            _filter6 = _filter6.Replace(_or, "");
                                            display6 = display6 + item.Text.Trim();
                                        }
                                        gpa++;
                                        _Gpa.Add(item.Value);
                                        if (dict.Contains(item.Text.Trim()))
                                        {
                                            dict[item.Text.Trim()] = "GPA";
                                        }
                                        else
                                        {
                                            dict.Add(item.Text.Trim(), "GPA");
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter6)) _filter6 = "(" + _filter6 + ")" + _and;
                                //end
                                //fsq
                                DropDownCheckBoxes ddlfsq = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlfsq");

                                foreach (ListItem item in (ddlfsq as ListControl).Items)
                                {
                                    if (item.Selected)
                                    {
                                        _filter7 = _filter7 + _or + "Formerscientific_qualification Like'" + item.Text.Trim() + "'";
                                        if (_filter7.StartsWith(_or))
                                        {
                                            _filter7 = _filter7.Replace(_or, "");
                                            display7 = display7 + item.Text.Trim();
                                        }
                                        fsq++;
                                        _FSQualification.Add(item.Value);
                                        if (dict.Contains(item.Text.Trim()))
                                        {
                                            dict[item.Text.Trim()] = "Formerscientific_qualification";
                                        }
                                        else
                                        {
                                            dict.Add(item.Text.Trim(), "Formerscientific_qualification");
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter7)) _filter7 = "(" + _filter7 + ")" + _and;

                                //end
                                //fgq
                                DropDownCheckBoxes ddlfgq = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlfgq");

                                foreach (ListItem item in (ddlfgq as ListControl).Items)
                                {
                                    if (item.Selected)
                                    {
                                        _filter8 = _filter8 + _or + "FormerGradscientific_qualification Like'" + item.Text.Trim() + "'";
                                        if (_filter8.StartsWith(_or))
                                        {
                                            _filter8 = _filter8.Replace(_or, "");
                                            display8 = display8 + item.Text.Trim();
                                        }
                                        fgq++;
                                        _FGQualification.Add(item.Value);
                                        if (dict.Contains(item.Text.Trim()))
                                        {
                                            dict[item.Text.Trim()] = "FormerGradscientific_qualification";
                                        }
                                        else
                                        {
                                            dict.Add(item.Text.Trim(), "FormerGradscientific_qualification");
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter8)) _filter8 = "(" + _filter8 + ")" + _and;
                                //end
                                //for formardegree
                                DropDownCheckBoxes ddlformardegree = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFDegree");

                                foreach (ListItem item in (ddlformardegree as ListControl).Items)
                                {
                                    if (item.Selected)
                                    {
                                        _filter9 = _filter9 + _or + "formerdegree Like'" + item.Text.Trim() + "'";
                                        if (_filter9.StartsWith(_or))
                                        {
                                            _filter9 = _filter9.Replace(_or, "");
                                            display9 = display9 + item.Text.Trim();
                                        }
                                        fd++;
                                        _formardegree.Add(item.Value);
                                        if (dict.Contains(item.Text.Trim()))
                                        {
                                            dict[item.Text.Trim()] = "formerdegree";
                                        }
                                        else
                                        {
                                            dict.Add(item.Text.Trim(), "formerdegree");
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter9)) _filter9 = "(" + _filter9 + ")" + _and;
                                //end
                                //for cumulative degree
                                DropDownCheckBoxes ddlCumumulativedegree = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFormarcumulative");

                                foreach (ListItem item in (ddlCumumulativedegree as ListControl).Items)
                                {
                                    if (item.Selected)
                                    {
                                        _filter10 = _filter10 + _or + "lastcumulative Like'" + item.Text.Trim() + "'";
                                        if (_filter10.StartsWith(_or))
                                        {
                                            _filter10 = _filter10.Replace(_or, "");
                                            display10 = display10 + item.Text.Trim();
                                        }
                                        lc++;
                                        _lastcumulative.Add(item.Value);
                                        if (dict.Contains(item.Text.Trim()))
                                        {
                                            dict[item.Text.Trim()] = "lastcumulative";
                                        }
                                        else
                                        {
                                            dict.Add(item.Text.Trim(), "lastcumulative");
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter10)) _filter10 = "(" + _filter10 + ")" + _and;
                                //end
                                //for Date
                                DropDownCheckBoxes ddlDate = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddldate");

                                foreach (ListItem item in (ddlDate as ListControl).Items)
                                {
                                    if (item.Selected)
                                    {
                                        _filter11 = _filter11 + _or + "CreatedOn Like'" +String.Format("{0:dd/Mm/yyyy}",item.Text.Trim())+"'";
                                        if (_filter11.StartsWith(_or))
                                        {
                                            _filter11 = _filter11.Replace(_or, "");
                                            display11 = display11 + item.Text.Trim();
                                        }
                                        co++;
                                        _CreatedOn.Add(item.Value);
                                        if (dict.Contains(item.Text.Trim()))
                                        {
                                            dict[item.Text.Trim()] = "CreatedOn";
                                        }
                                        else
                                        {
                                            dict.Add(item.Text.Trim(), "CreatedOn");
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter11)) _filter11 = "(" + _filter11 + ")" + _and;

                                //end

                                Session["Dictionary"] = dict;
                                totalFilter = (_filter) + (_filter1) + (_filter2) + (_filter3) + (_filter4) + (_filter5) + (_filter6) + (_filter7) + (_filter8) + (_filter9) + (_filter10)+(_filter11);
                                if (totalFilter.EndsWith(_and)) totalFilter = totalFilter.Substring(0, totalFilter.Length - _and.Length);
                                lblfilterData.ForeColor = System.Drawing.Color.Black;
                                lblfilterData.Font.Bold = true;
                                if (Session["UserDetails"] == null)
                                {
                                    //gvwncms.DataSource = dtgridview;
                                    //gvwncms.DataBind();
                                    GetUserDetails();
                                    dtgridview = (DataTable)Session["UserDetails"];
                                }
                                else
                                {
                                    dtgridview = (DataTable)Session["UserDetails"];
                                }

                                dtgridview.DefaultView.RowFilter = totalFilter;
                             
                                int child = dtgridview.DefaultView.Count;
                                if(child==0)
                                {
                                    string comment = "(Request filter data not found...!)";
                                    lblfilterData.Text = display + display1 + display2 + display3 + display4 + display5 + display6 + display7 + display8 + display9 + display10 + display11 + comment;
                                    lblfilterData.ForeColor = System.Drawing.Color.Red;
                                }
                                else
                                {
                                    gvwncms.DataSource = dtgridview.DefaultView;
                                    gvwncms.DataBind();
                                }
                                if(gvwncms.Rows.Count>0)
                                {
                                    UpdateDropDownChckBoxBinding();
                                }
                                ViewstateDropdown();
                            }
                        }
                        else
                        {
                            if(fn==0 && ld==0 && sq==0 &&ea==0 && gq==0 && mn==0 && gpa==0 && fsq==0 && fgq==0 && fd==0 && lc==0 && co==0)
                            {

                            }
                            else
                            {
                                fn = 0; ld = 0; sq = 0; ea = 0; gq = 0; gpa = 0; fsq = 0; fgq = 0; fd = 0; lc = 0; co = 0; totalFilter = "";
                                _fullNameArr.Clear(); _degreeArr.Clear(); _CategoryArrays.Clear(); _ScintificQualificationArr.Clear();
                                _GradscientificQualificationArr.Clear(); _mobile.Clear(); _Gpa.Clear();
                                _EmailArr.Clear(); _FSQualification.Clear(); _FGQualification.Clear();
                                _formardegree.Clear(); _lastcumulative.Clear(); _CreatedOn.Clear();

                                DropDownCheckBoxes ddlFullName = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlfullName");
                                string _or = " OR ";
                                string _and = " AND ";
                                foreach (ListItem item in (ddlFullName as ListControl).Items)
                                {
                                    string i = item.Text;
                                    if (item.Selected)
                                    {
                                        _filter = _filter + _or + "FullName Like '" + item.Text.Trim() + "'";
                                        if (_filter.StartsWith(_or))
                                        {
                                            _filter = _filter.Replace(_or, "");
                                            display = display + item.Text.Trim();
                                        }
                                        fn++;
                                        _fullNameArr.Add(item.Value);
                                    }
                                    else
                                    {
                                        if(dict.Contains(item.Text.Trim()))
                                        {
                                            dict.Remove(item.Text.Trim());
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter)) _filter = "(" + _filter + ")" + _and;

                                DropDownCheckBoxes ddlDegree = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlDegree");

                                foreach (ListItem item in (ddlDegree as ListControl).Items)
                                {
                                    if (item.Selected)
                                    {
                                        _filter1 = _filter1 + _or + "lastdegree Like'" + item.Text.Trim() + "'";
                                        if (_filter1.StartsWith(_or))
                                        {
                                            _filter1 = _filter1.Replace(_or, "");
                                            display1 = display1 + item.Text.Trim();
                                        }
                                        ld++;
                                        _degreeArr.Add(item.Value);
                                    }
                                    else
                                    {
                                        if(dict.Contains(item.Text.Trim()))
                                        {
                                            dict.Remove(item.Text.Trim());
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter1)) _filter1 = "(" + _filter1 + ")" + _and;

                                //for Scientific Qualifiucation

                                DropDownCheckBoxes ddlScintificQualification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlSQ");

                                foreach (ListItem item in (ddlScintificQualification as ListControl).Items)
                                {
                                    if (item.Selected)
                                    {
                                        _filter2 = _filter2 + _or + "scientific_qualification Like'" + item.Text.Trim() + "'";
                                        if (_filter2.StartsWith(_or))
                                        {
                                            _filter2 = _filter2.Replace(_or, "");
                                            display2 = display2 + item.Text.Trim();
                                        }
                                        sq++;
                                        _ScintificQualificationArr.Add(item.Value);
                                    }
                                    else
                                    {
                                        if (dict.Contains(item.Text.Trim()))
                                        {
                                            dict.Remove(item.Text.Trim());
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter2)) _filter2 = "(" + _filter2 + ")" + _and;

                                //end

                                //for Email address
                                DropDownCheckBoxes ddlEmail = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlEmail");

                                foreach (ListItem item in (ddlEmail as ListControl).Items)
                                {
                                    if (item.Selected)
                                    {
                                        _filter3 = _filter3 + _or + "EmailAddress Like'" + item.Text.Trim() + "'";
                                        if (_filter3.StartsWith(_or))
                                        {
                                            _filter3 = _filter3.Replace(_or, "");
                                            display3 = display3 + item.Text.Trim();
                                        }
                                        ea++;
                                        _EmailArr.Add(item.Value);
                                    }
                                    else
                                    {
                                        if (dict.Contains(item.Text.Trim()))
                                        {
                                            dict.Remove(item.Text.Trim());
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter3)) _filter3 = "(" + _filter3 + ")" + _and;
                                //end

                                //gor GQualification
                                DropDownCheckBoxes ddlGqualification = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlGQ");

                                foreach (ListItem item in (ddlGqualification as ListControl).Items)
                                {
                                    if (item.Selected)
                                    {
                                        _filter4 = _filter4 + _or + "Gradscientific_qualification Like'" + item.Text.Trim() + "'";
                                        if (_filter4.StartsWith(_or))
                                        {
                                            _filter4 = _filter4.Replace(_or, "");
                                            display4 = display4 + item.Text.Trim();
                                        }
                                        gq++;
                                        _GradscientificQualificationArr.Add(item.Value);
                                    }
                                    else
                                    {
                                        if (dict.Contains(item.Text.Trim()))
                                        {
                                            dict.Remove(item.Text.Trim());
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter4)) _filter4 = "(" + _filter4 + ")" + _and;
                                //end

                                //mobile
                                DropDownCheckBoxes ddlMobile = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlMobileNumber");

                                foreach (ListItem item in (ddlMobile as ListControl).Items)
                                {
                                    if (item.Selected)
                                    {
                                        _filter5 = _filter5 + _or + "MobileNumber Like'" + item.Text.Trim() + "'";
                                        if (_filter5.StartsWith(_or))
                                        {
                                            _filter5 = _filter5.Replace(_or, "");
                                            display5 = display5 + item.Text.Trim();
                                        }
                                        mn++;
                                        _mobile.Add(item.Value);
                                    }
                                    else
                                    {
                                        if (dict.Contains(item.Text.Trim()))
                                        {
                                            dict.Remove(item.Text.Trim());
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter5)) _filter5 = "(" + _filter5 + ")" + _and;
                                //end

                                //gpa
                                DropDownCheckBoxes ddlGpa = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlGpa");

                                foreach (ListItem item in (ddlGpa as ListControl).Items)
                                {
                                    if (item.Selected)
                                    {
                                        _filter6 = _filter6 + _or + "GPA Like'" + item.Text.Trim() + "'";
                                        if (_filter6.StartsWith(_or))
                                        {
                                            _filter6 = _filter6.Replace(_or, "");
                                            display6 = display6 + item.Text.Trim();
                                        }
                                        gpa++;
                                        _Gpa.Add(item.Value);
                                    }
                                    else
                                    {
                                        if (dict.Contains(item.Text.Trim()))
                                        {
                                            dict.Remove(item.Text.Trim());
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter6)) _filter6 = "(" + _filter6 + ")" + _and;
                                //end
                                //for fsq
                                DropDownCheckBoxes ddlfsq = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlfsq");

                                foreach (ListItem item in (ddlfsq as ListControl).Items)
                                {
                                    if (item.Selected)
                                    {
                                        _filter7 = _filter7 + _or + "Formerscientific_qualification Like'" + item.Text.Trim() + "'";
                                        if (_filter7.StartsWith(_or))
                                        {
                                            _filter7 = _filter7.Replace(_or, "");
                                            display7 = display7 + item.Text.Trim();
                                        }
                                        fsq++;
                                        _FSQualification.Add(item.Value);
                                    }
                                    else
                                    {
                                        if (dict.Contains(item.Text.Trim()))
                                        {
                                            dict.Remove(item.Text.Trim());
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter7)) _filter7 = "(" + _filter7 + ")" + _and;
                                //end
                                //for fgq
                                DropDownCheckBoxes ddlfgq = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlfgq");

                                foreach (ListItem item in (ddlfgq as ListControl).Items)
                                {
                                    if (item.Selected)
                                    {
                                        _filter8 = _filter8 + _or + "FormerGradscientific_qualification Like'" + item.Text.Trim() + "'";
                                        if (_filter8.StartsWith(_or))
                                        {
                                            _filter8 = _filter8.Replace(_or, "");
                                            display8 = display8 + item.Text.Trim();
                                        }
                                        fgq++;
                                        _FGQualification.Add(item.Value);
                                    }
                                    else
                                    {
                                        if (dict.Contains(item.Text.Trim()))
                                        {
                                            dict.Remove(item.Text.Trim());
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter8)) _filter8 = "(" + _filter8 + ")" + _and;
                                //end
                                //for formar degree
                                DropDownCheckBoxes ddlFormardegree = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFDegree");

                                foreach (ListItem item in (ddlFormardegree as ListControl).Items)
                                {
                                    if (item.Selected)
                                    {
                                        _filter9 = _filter9 + _or + "formerdegree Like'" + item.Text.Trim() + "'";
                                        if (_filter9.StartsWith(_or))
                                        {
                                            _filter9 = _filter9.Replace(_or, "");
                                            display9 = display9 + item.Text.Trim();
                                        }
                                        fd++;
                                        _formardegree.Add(item.Value);
                                    }
                                    else
                                    {
                                        if (dict.Contains(item.Text.Trim()))
                                        {
                                            dict.Remove(item.Text.Trim());
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter9)) _filter9 = "(" + _filter9 + ")" + _and;
                                //end
                                //last cumulative
                                DropDownCheckBoxes ddllastcumulative = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddlFormarcumulative");

                                foreach (ListItem item in (ddllastcumulative as ListControl).Items)
                                {
                                    if (item.Selected)
                                    {
                                        _filter10 = _filter10 + _or + "lastcumulative Like'" + item.Text.Trim() + "'";
                                        if (_filter10.StartsWith(_or))
                                        {
                                            _filter10 = _filter10.Replace(_or, "");
                                            display10 = display10 + item.Text.Trim();
                                        }
                                        lc++;
                                        _lastcumulative.Add(item.Value);
                                    }
                                    else
                                    {
                                        if (dict.Contains(item.Text.Trim()))
                                        {
                                            dict.Remove(item.Text.Trim());
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter10)) _filter10 = "(" + _filter10 + ")" + _and;
                                //end
                                //created on
                                DropDownCheckBoxes ddlDate = (DropDownCheckBoxes)gvwncms.HeaderRow.FindControl("ddldate");

                                foreach (ListItem item in (ddlDate as ListControl).Items)
                                {
                                    if (item.Selected)
                                    {
                                        _filter11 = _filter11 + _or + "CreatedOn Like'" +String.Format("{0:dd/MM/yyyy}",item.Text.Trim())+"'";
                                        if (_filter11.StartsWith(_or))
                                        {
                                            _filter11 = _filter11.Replace(_or, "");
                                            display11 = display11 + item.Text.Trim();
                                        }
                                        co++;
                                        _CreatedOn.Add(item.Value);
                                    }
                                    else
                                    {
                                        if (dict.Contains(item.Text.Trim()))
                                        {
                                            dict.Remove(item.Text.Trim());
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_filter11)) _filter11 = "(" + _filter11 + ")" + _and;
                                //end
                                Session["Dictionary"] = dict;
                                totalFilter = (_filter) + (_filter1) + (_filter2) + (_filter3) + (_filter4) + (_filter5) + (_filter6) + (_filter7) + (_filter8) + (_filter9) + (_filter10) + (_filter11);
                                if (totalFilter.EndsWith(_and)) totalFilter = totalFilter.Substring(0, totalFilter.Length - _and.Length);
                                lblfilterData.ForeColor = System.Drawing.Color.Black;
                                lblfilterData.Font.Bold = true;

                                if (Session["UserDetails"] == null)
                                {
                                    GetUserDetails();
                                    dtgridview = (DataTable)Session["UserDetails"];
                                }
                                else
                                {
                                    dtgridview = (DataTable)Session["UserDetails"];
                                }
                                dtgridview.DefaultView.RowFilter = totalFilter;
                              
                                int child = dtgridview.DefaultView.Count;
                                if (child == 0)
                                {
                                    string comment = "(Requested Filter Data Not Found)";
                                    lblfilterData.Text = display + display1 + display2 + display3 + display4 + display5 + display6 + display7 + display8 + display9 + display10 + display11 + comment;
                                    lblfilterData.ForeColor = System.Drawing.Color.Red;
                                }
                                else
                                {
                                    gvwncms.DataSource = dtgridview.DefaultView;
                                    gvwncms.DataBind();
                                }
                                if(gvwncms.Rows.Count>0)
                                {
                                    UpdateDropDownChckBoxBinding();
                                }
                                ViewstateDropdown();
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            { 

            }
        }
        protected void gvwncms_PreRender(object sender, EventArgs e)
        {
            if (gvwncms.Rows.Count > 0)
            {
                gvwncms.UseAccessibleHeader = true;
                gvwncms.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
        protected void gvwncms_Sorting(Object sender, GridViewSortEventArgs e)
        {
            ViewState["sortExpr"] = e.SortExpression;

            DataView dv1 = new DataView();
            if (Session["UserDetails"] == null)
            {
                gvwncms.DataSource = GetUserDetails();
                gvwncms.DataBind();
                dtgridResponse = (DataTable)Session["UserDetails"];
            }
            else
            {
                dtgridResponse = (DataTable)Session["UserDetails"];
            }
            //if (Label1.Text != "RESA")
            //{
            //    dv1 = dtgridResponse.DefaultView;
            //}
            //else
            //{
            //    dv1 = dtgridResponse.DefaultView;
            //}


            string DD = GetSortDirection(e.SortExpression).ToString();
            if (DD == "Ascending")
            {
                dv1.Sort = e.SortExpression + " " + "ASC";
            }
            else
            {
                dv1.Sort = e.SortExpression + " " + "DESC";
            }

            gvwncms.DataSource = dv1;
            gvwncms.DataBind();

        }
        protected SortDirection GetSortDirection(string Column)
        {
            SortDirection nextDir = SortDirection.Ascending; // Default next sort expression behaviour.
            if (ViewState["sort"] != null && ViewState["sort"].ToString() == Column)
            {   // Exists... DESC.
                nextDir = SortDirection.Descending;
                ViewState["sort"] = null;
            }
            else
            {   // Doesn't exists, set ViewState.
                ViewState["sort"] = Column;
            }
            return nextDir;

        }
        protected void gvwncms_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {            
            gvwncms.PageIndex = e.NewPageIndex;
            if (Session["UserDetails"] == null)
            {
                GetUserDetails();
                dtgridview = (DataTable)Session["UserDetails"];
            }
            else
            {
                dtgridview = (DataTable)Session["UserDetails"];
            }
            gvwncms.DataSource = dtgridview;
            gvwncms.DataBind();
            if (dtgridview.Rows.Count > 0)
            {
                UpdateDropDownChckBoxBinding();
            }
                //gvwncms.PageIndex = e.NewPageIndex;
                //dtgridview = (DataTable)Session["UserDetails"];
                //gvwncms.DataSource = dtgridResponse;
                //gvwncms.DataBind();
        }
        //protected void btnPrint_Click(object sender, EventArgs e)
        //{
        //    int Id = Convert.ToInt32(Request.QueryString["Id"]);
        //    Response.Redirect("UserDetailsByID?Id=" + Id);
        //}

        protected void btnPrint_Command(object sender, CommandEventArgs e)
        {
            Session["Id"] = e.CommandArgument;
            ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page), "OpenWindow", "window.open('UserDetailsById.Aspx?');", true);
        }      
    }
}