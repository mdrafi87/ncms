﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryCore.DAL
{
    public struct CLS_User
    {
        private string m_Emp_No;
        private string m_Emp_First_Name;
        private string m_Emp_Second_Name;
        private string m_Emp_Last_Name;
        private string m_Emp_Email;
        private string m_Emp_Comp_No;
        private string m_Emp_Type;
        private string m_Emp_Dept_No;
        private string m_Emp_Pos;

        private string m_Emp_PositionId;
        private string m_Emp_DesignationId;

        private string m_Emp_Dept_Id;
        private string m_Emp_Root;
        private string m_Emp_username;
        private string m_Emp_Password;
        private bool m_Emp_Active;
        private string m_Emp_SystemRole;
        private string m_Team_Res;
        private string m_Team_Autho;
        private string m_Team_Informer;
        private string m_Team_Support;
        private string m_Team_Auditor;
        private string m_Team_Coor;
        private bool status;
        private string m_DarManual;
        private string m_searchtype;

        public string Emp_DesignationId
        {
            get { return m_Emp_DesignationId; }
            set { m_Emp_DesignationId = value; }
        }

        public string SearchType
        {
            get { return m_searchtype; }
            set { m_searchtype = value; }
        }
        public string Emp_PositionId
        {
            get { return m_Emp_PositionId; }
            set { m_Emp_PositionId = value; }
        }

        public string Emp_Pos
        {
            get { return m_Emp_Pos; }
            set { m_Emp_Pos = value; }
        }

        public string Team_Support
        {
            get { return m_Team_Support; }
            set { m_Team_Support = value; }
        }
        public string Team_Auditor
        {
            get { return m_Team_Auditor; }
            set { m_Team_Auditor = value; }
        }

        public string DarManual
        {
            get { return m_DarManual; }
            set { m_DarManual = value; }
        }
     public string Emp_No
        {
            get { return m_Emp_No; }
            set { m_Emp_No = value; }
        }
     
     public string Emp_First_Name
     {
         get { return m_Emp_First_Name; }
         set { m_Emp_First_Name = value; }
     }
     
     public string Emp_Second_Name
     {
         get { return m_Emp_Second_Name; }
         set { m_Emp_Second_Name = value; }
     }
     
     public string Emp_Last_Name
     {
         get { return m_Emp_Last_Name; }
         set { m_Emp_Last_Name = value; }
     }
     
     public string Emp_Email
     {
         get { return m_Emp_Email; }
         set { m_Emp_Email = value; }
     }
     
     public string Emp_Comp_No
     {
         get { return m_Emp_Comp_No; }
         set { m_Emp_Comp_No = value; }
     }
     
     public string Emp_Type
     {
         get { return m_Emp_Type; }
         set { m_Emp_Type = value; }
     }
     

     public string Emp_Dept_No
     {
         get { return m_Emp_Dept_No; }
         set { m_Emp_Dept_No = value; }
     }
     public string Emp_Dept_Id
     {
         get { return m_Emp_Dept_Id; }
         set { m_Emp_Dept_Id = value; }
     }

     public string Emp_Root
     {
         get { return m_Emp_Root; }
         set { m_Emp_Root = value; }
     }
     

     public string Emp_username
     {
         get { return m_Emp_username; }
         set { m_Emp_username = value; }
     }
     

     public string Emp_Password
     {
         get { return m_Emp_Password; }
         set { m_Emp_Password = value; }
     }
     

     public bool Emp_Active
     {
         get { return m_Emp_Active; }
         set { m_Emp_Active = value; }
     }
     

     public string Emp_SystemRole
     {
         get { return m_Emp_SystemRole; }
         set { m_Emp_SystemRole = value; }
     }


     public string Team_Res
     {
         get { return m_Team_Res; }
         set { m_Team_Res = value; }
     }


     public string Team_Autho
     {
         get { return m_Team_Autho; }
         set { m_Team_Autho = value; }
     }

     public string Team_Coor
     {
         get { return m_Team_Coor; }
         set { m_Team_Coor = value; }
     }


     public string Team_Informer
     {
         get { return m_Team_Informer; }
         set { m_Team_Informer = value; }
     }


     public bool Status
     {
         get { return status; }
         set { status = value; }
     }


    }


    public class CLS_WB11USER
    {

        private string m_UserName;
        private string m_Password;
        private bool m_Active;
        private string m_Emp_No;
        private string m_Emp_Comp_No;
        private string m_Emp_SystemRole;



        public string UserName
        {
            get { return m_UserName; }
            set { m_UserName = value; }
        }
        

        public string Password
        {
            get { return m_Password; }
            set { m_Password = value; }
        }
        

        public bool Active
        {
            get { return m_Active; }
            set { m_Active = value; }
        }
       

        public string Emp_No
        {
            get { return m_Emp_No; }
            set { m_Emp_No = value; }
        }
        

        public string Emp_Comp_No
        {
            get { return m_Emp_Comp_No; }
            set { m_Emp_Comp_No = value; }
        }
        
        public string Emp_SystemRole
        {
            get { return m_Emp_SystemRole; }
            set { m_Emp_SystemRole = value; }
        }
       

        

    }

}
