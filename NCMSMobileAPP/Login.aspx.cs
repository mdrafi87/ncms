﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SettingsLibrary;
using LibraryCore.DAL;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;
using System.Configuration;
using System.IO;
using System.Net;
using System.Drawing;

namespace NCMSMobileAPP
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                string CreatedOn = (System.DateTime.Now.ToString("dd/MM/yyyy"));
                ltrCopyrightYear.Text = DateTime.Now.Year.ToString();
                Session.Clear();
                lblloginError.Text = string.Empty;
                lblloginError.Text = string.Empty;
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string userName = txtUserName.Text;
            string password = txtPassword.Text;
            try
            {
                if (ValidateUser(ref userName, ref password) == true)
                {
                    Response.Redirect("Default.aspx");
                }
                else
                {
                    lblloginError.Text = "إسم المستخدم أو رمز المرور غير صالح";
                    lblloginError.ForeColor = Color.Red;
                }
            }
            catch(Exception exc)
            {
                lblloginError.Text = exc.Message;
            }
        }
        protected bool ValidateUser(ref string V_Username, ref string V_Password)
        {
            CLS_User LoginUser = new CLS_User();
            DataSet dstLastNo;
            
            LoginUser.Emp_username = V_Username;
            LoginUser.Emp_Password = V_Password;

            Session.Add("User", V_Username.ToString());            
            dstLastNo = DAL_User.getUserByUserName(LoginUser);

            if (dstLastNo.Tables[0].Rows.Count>0)
            {                
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}