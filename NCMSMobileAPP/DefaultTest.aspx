﻿<%@ Page Title="NCMS User Details" Language="C#" MasterPageFile="~/NCMSMobileApp.Master" AutoEventWireup="true" CodeBehind="DefaultTest.aspx.cs" EnableEventValidation="true" Inherits="NCMSMobileAPP.DefaultTest" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="Page-Exit" content="progid:DXImageTransform.Microsoft.Fade(Overlap=1.00,duration=0.2)" />
    <style type="text/css">
        .colVisibility {
            display: none;
        }

        div.dd_chk_select {
            border-color: #344E8B;
            height: 40px;
            padding: 10px 15px;
            font-size: 14px;
        }

            div.dd_chk_select div#caption {
                text-align: right !important;
            }
        /*div.dd_chk_select{
            background-image: url("WebResource.axd?d=8mNM2oY0_MTbltHc0o1ItfBtftnl_Ix-J3XMyAmXe43x2mStvm4W236Ml96RhLEK-F4hjsF3c7O8pkQD-NSzaUrxIaLZm-Eq3OKyNWdRLgdhz_ZKclbD9lkWS-so_HikC1LntSmeQW8cwYVBXF1IaUmZklhJ8Qy6rcrSEpkGWnQ1&t=634974191230236446");
            background-position: center left;
            background-repeat: no-repeat;
        }
        div.dd_chk_select:hover{
                background-image: url("WebResource.axd?d=8mNM2oY0_MTbltHc0o1ItfBtftnl_Ix-J3XMyAmXe43x2mStvm4W236Ml96RhLEK-F4hjsF3c7O8pkQD-NSzaUrxIaLZm-Eq3OKyNWdRLgdhz_ZKclbD9lkWS-so_HikC1LntSmeQW8cwYVBXF1IaUmZklhJ8Qy6rcrSEpkGWnQ1&t=634974191230236446");
                background-position: center left;           
	            background-repeat: no-repeat;
         }*/
        div.dd_chk_drop {
            top: 20px !important;
            text-align: right !important;
            border-color: #344E8B !important;
            left: inherit !important;
            right: -1px;
        }

        div.dd_chk_select {
            border-color: #344E8B !important;
            height: 25px !important;
            padding: 4px 15px !important;
            font-size: 14px;
        }

        .btn-blue {
            min-height: 40px;
        }

        input[type='checkbox'] {
            margin-left: 10px;
        }

        div.dd_chk_drop div#buttons input {
            background-image: none !important;
        }

            div.dd_chk_drop div#buttons input:hover {
                background-image: none !important;
            }

        .dd_chk_drop {
            width: 190px !important;
        }

        #buttons input[type='button'] {
            background: #fff;
            border: 1px solid #344E8B !important;
            border-radius: 5px;
        }

        .AutoExtender {
            display: block;
            border: solid 1px #272725;
            padding: 15px;
            background-color: White;
            margin-left: 0px;
            text-align: left;
            max-width: 100%;
        }

        .AutoExtenderList {
            border-bottom: solid 1px #ccc;
            cursor: pointer;
            margin-bottom: 8px;
            list-style: none;
            padding-bottom: 5px;
        }

        .AutoExtenderHighlight {
            color: White;
            background-color: #57cbc8;
            cursor: pointer;
            margin-bottom: 8px;
            list-style: none;
            padding-bottom: 5px;
        }

        .print-btn {
            padding: 3px 12px;
            margin: 0;
            min-height: 20px;
        }

        .new .form-control {
            height: 40px;
        }

        .new .btn-blue {
            margin-top: -11px;
        }

        .new .input-group-btn .btn {
            padding: 3px 12px;
        }

        table#ContentPlaceHolder1_gvwncms tr:last-child {
            background: #fff;
        }

            table#ContentPlaceHolder1_gvwncms tr:last-child td table tr td {
                padding: 15px;
                padding-left: 0px;
                padding-right: 0;
                margin-left: -2px;
                /*border: 1px solid #344E8B;
            color: #272725*/
            }

                table#ContentPlaceHolder1_gvwncms tr:last-child td table tr td a {
                    color: #272725;
                    padding: 6px 15px;
                    border: 1px solid #344E8B;
                }

                table#ContentPlaceHolder1_gvwncms tr:last-child td table tr td span {
                    background: #344E8B;
                    color: #fff;
                    padding: 6px 15px;
                    border: 1px solid #344E8B;
                }

        .AutoExtenderList, .AutoExtenderHighlight, .AutoExtender {
            text-align: right !important;
        }
    </style>
    <script type="text/javascript">

        function SetContextKey() {
            $find('AutoEx').set_contextKey($get("<%=ddlfilter.ClientID %>").value);
        }
        function acePopulated(sender, e) {


        }
        function aceSelected(sender, e) {


        }
    </script>
    <script type="text/javascript">

        function HeaderClick(CheckBox) {
            var TargetBaseControl = document.getElementById('<%= this.gvwncms.ClientID %>');
            var TargetChildControl = "chkbSelect";
            var counterHead = 0;
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                    Inputs[n].checked = CheckBox.checked;
                    if (Inputs[n].checked)
                        counterHead = counterHead + 1;
                }
            Counter = CheckBox.checked ? TotalChkBx : 0;
            $('#lblSelectCount').text(counterHead);
        }
        function checkValue() {
            var hidControl = document.getElementById('<%= btnClick.ClientID %>');
            if (hidControl != null) hidControl.value = "True";
        }
        function ChildClick(CheckBox, HCheckBox) {

            var counterChild = 0;
            var TargetBaseControl = document.getElementById('<%= this.gvwncms.ClientID %>');
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            var TargetChildControl = "chkbSelect";
            var Counter = 0;

            var HeaderCheckBox = document.getElementById(HCheckBox);


            if (CheckBox.checked && Counter < TotalChkBx)
                Counter++;
            else if (Counter > 0)
                Counter--;
            if (Counter < TotalChkBx)
                HeaderCheckBox.checked = false;
            else if (Counter == TotalChkBx)
                HeaderCheckBox.checked = true;


            for (var n = 0; n < Inputs.length; ++n) {

                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl) != -1) {
                    if (Inputs[n].checked)
                        counterChild = counterChild + 1;
                }
            }
            $('#lblSelectCount').text(counterChild);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="pnlNCMS" ChildrenAsTriggers="true">
        <ContentTemplate>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-horizontal" style="overflow-x:auto;">
                            <%--<asp:Button runat="server" CssClass="btn btn-blue" Text="Apply Filter" />
                            <asp:Button runat="server" CssClass="btn btn-blue" Text="أصدر" ID="btnSubmit"/>
                            <asp:Button runat="server" CssClass="btn btn-blue" Text="طباعة" />--%>
                              <asp:HiddenField Value="False" ID="btnClick" runat="server" />  
                            <asp:Label ID="lblError" runat="server"></asp:Label>
                            <asp:Label ID="lblRequestsCount" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lblfilterData" runat="server" Visible="false"></asp:Label>
                            <div class="table-responsive">
                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <asp:Button runat="server" CssClass="btn btn-blue" Text="Print All" ID="btnPrintAll" OnClick="btnPrintAll_Click" OnClientClick="javascript:checkValue();"/>
                                    </div>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlfilter" runat="server" name="Items" ClientIDMode="Static" CssClass="form-control-new">
                                            <asp:ListItem Value="1">First Name</asp:ListItem>
                                            <asp:ListItem Value="2">Second Name</asp:ListItem>
                                            <asp:ListItem Value="3">Third Name</asp:ListItem>
                                            <asp:ListItem Value="4">Last Name</asp:ListItem>
                                            <asp:ListItem Value="5">MobileNumber</asp:ListItem>
                                            <asp:ListItem Value="6">Email</asp:ListItem>
                                            <asp:ListItem Value="7">University</asp:ListItem>
                                            <asp:ListItem Value="8">Degree</asp:ListItem>
                                            <asp:ListItem Value="9">Speciality</asp:ListItem>
                                            <asp:ListItem Value="10">GPA</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="input-group new">
                                            <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" onkeyup="SetContextKey()" placeholder="search..."></asp:TextBox>
                                            <span class="input-group-btn">
                                                <asp:ImageButton ID="btnSearch" ImageUrl="~/img/search.png" runat="server" CssClass="btn btn-blue" Text="Search" OnClick="btnSearch_Click" />
                                            </span>
                                            <cc1:AutoCompleteExtender BehaviorID="AutoEx" ID="AutoCompleteExtender" runat="server"
                                                TargetControlID="txtSearch" CompletionInterval="250" MinimumPrefixLength="1" UseContextKey="true"
                                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight" OnClientPopulated="acePopulated"
                                                CompletionSetCount="15" OnClientItemSelected="aceSelected"
                                                ServiceMethod="GetSearchgDetails" ServicePath="~/WebServices/SearchAutoSuggest.asmx">
                                            </cc1:AutoCompleteExtender>
                                        </div>
                                        <!-- end input-group -->
                                    </div>
                                    <!-- end col-sm-4 -->
                                    <div class="col-sm-2">
                                        <asp:Button ID="btnRefresh" runat="server" Text="Refresh" OnClick="btnRefresh_Click" CssClass="btn btn-blue" />
                                    </div>
                                </div>
                            </div><br />
                            <div class="form-horizontal">
                                <asp:Label ID="lblprinterror" runat="server"></asp:Label>                              
                            </div>
                            <table class="table table-bordered">
                                <tr>
                                    <asp:GridView ID="gvwncms" runat="server" AutoGenerateColumns="false" OnPageIndexChanging="gvwncms_PageIndexChanging" PageSize="10"
                                        AllowPaging="true" AllowSorting="true" EnableViewState="true" CssClass="table table-bordered" DataKeyNames="ID"
                                        EnableSortingAndPagingCallbacks="false" OnRowCreated="gvwncms_Rowcreated">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkbSelectAll" runat="server" onclick="javascript:HeaderClick(this);" CausesValidation="true" BorderStyle="None" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkbSelect" runat="server" CausesValidation="true" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn btn-blue print-btn" OnCommand="btnPrint_Command" CommandArgument='<%#Eval("ID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="ID" HeaderText="ID" HeaderStyle-CssClass="colVisibility" ItemStyle-CssClass="colVisibility" />
                                           <%-- <asp:TemplateField HeaderText="ID" ShowHeader="true" HeaderStyle-CssClass="colVisibility" ItemStyle-CssClass="colVisibility">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblID" runat="server" Text='<%#Eval("ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="الاسم الكامل" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlFirstName" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="الاسم الاول" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                        <%--<Style DropDownBoxBoxWidth="230px" SelectBoxWidth="120px" />--%>
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblfirstname" runat="server" Text='<%#Eval("First_Name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--second_name--%>
                                            <asp:TemplateField HeaderText="الاسم الثاني" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlSecondName" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="الاسم الثاني" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                        <%--<Style DropDownBoxBoxWidth="230px" SelectBoxWidth="120px" />--%>
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSecondName" runat="server" Text='<%#Eval("Second_Name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--end--%>
                                            <%--third_name--%>
                                            <asp:TemplateField HeaderText="الاسم الثالث" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlThirdName" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="الاسم الثالث" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                        <%--<Style DropDownBoxBoxWidth="230px" SelectBoxWidth="120px" />--%>
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblThirdName" runat="server" Text='<%#Eval("Third_Name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--end--%>
                                            <%--last_name--%>
                                             <asp:TemplateField HeaderText="الكنية" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlLastname" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="الكنية" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                        <%--<Style DropDownBoxBoxWidth="230px" SelectBoxWidth="120px" />--%>
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLastname" runat="server" Text='<%#Eval("Last_Name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--end--%>
                                            <asp:TemplateField HeaderText="رقم الجوال المحلي" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlMobileNumber" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select Mobile number" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="رقم الجوال المحلي" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMobileNumber" runat="server" Text='<%#Eval("MobileNumber") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--end--%>
                                            <asp:TemplateField HeaderText="البريد الالكتروني" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlEmail" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select Email" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="البريد الالكتروني" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmail" runat="server" Text='<%#Eval("EmailAddress") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="الدرجة العلمية الأخيرة" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlDegree" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select Degree" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="الدرجة العلمية الأخيرة" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                        <%--<Style DropDownBoxBoxWidth="230px" SelectBoxWidth="120px" />--%>
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDegree" runat="server" Text='<%#Eval("lastdegree") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="المؤهل العلمي" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlSQ" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select Scientific Qualification" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="المؤهل العلمي" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSq" runat="server" Text='<%#Eval("scientific_qualification") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- for Gradscientific_qualification--%>
                                            <asp:TemplateField HeaderText="درجة التأهيل العلمي" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlGQ" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="درجة التأهيل العلمي" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGQ" runat="server" Text='<%#Eval("Gradscientific_qualification") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--lastcumulative--%>
                                             <asp:TemplateField HeaderText="آخر التراكمي" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddllastcumulative" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="آخر التراكمي" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbllastcumulative" runat="server" Text='<%#Eval("lastcumulative") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--for GPA--%>
                                            <asp:TemplateField HeaderText="GPA" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlGpa" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select GPA" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="GPA" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblgpa" runat="server" Text='<%#Eval("GPA") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- for Formerscientific_qualification--%>
                                            <asp:TemplateField HeaderText="Formerscientific Qualification" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlfsq" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select Formerscientific Qualification" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="التخصص للمؤهل العلمي السابق " OkButton="Apply" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblfsq" runat="server" Text='<%#Eval("Formerscientific_qualification") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- end--%>
                                            <%--for FormerGradscientific_qualification--%>
                                            <asp:TemplateField HeaderText="FGradscientific Qualification" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlfgq" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select FormerGradscientific Qualification" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="FGradscientific Qualification" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblgpaformer" runat="server" Text='<%#Eval("FormerGradscientific_qualification") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--end--%>

                                            <asp:TemplateField HeaderText="Former GPA" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlFormerGPA" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select Former GPA"
                                                        ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="Former GPA" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblfgpa" runat="server" Text='<%#Eval("FormerGPA") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--for formerdegree--%>
                                            <asp:TemplateField HeaderText="Former Degree" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlFDegree" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select Former Degree" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="Former Degree" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFormarDegree" runat="server" Text='<%#Eval("formerdegree") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--end--%>

                                            <%--for FormerCumulative--%>
                                            <asp:TemplateField HeaderText="Former Cumulative" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddlFormarcumulative" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="Former Cumulative" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblformercumulative" runat="server" Text='<%#Eval("formercumulative") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--end--%>
                                            <%--for created on--%>
                                            <asp:TemplateField HeaderText="تاريخ" ShowHeader="true">
                                                <HeaderTemplate>
                                                    <cc2:DropDownCheckBoxes ID="ddldate" runat="server" EnableViewState="true" AddJQueryReference="false"
                                                        UseButtons="true" UseSelectAllNode="true" OnSelectedIndexChanged="fullName_IndexChanged" ToolTip="Select Date" ValidationGroup="p1">
                                                        <Texts SelectBoxCaption="تاريخ" OkButton="Apply" SelectAllNode="اختر الكل" />
                                                        <%--<Style DropDownBoxBoxWidth="230px" SelectBoxWidth="120px" />--%>
                                                    </cc2:DropDownCheckBoxes>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDate" runat="server" Text='<%#Eval("CreatedOn") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--end--%>
                                        </Columns>
                                    </asp:GridView>
                                </tr>                               
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updProgres" runat="server">
        <ProgressTemplate>
            <div id="Loader">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>
