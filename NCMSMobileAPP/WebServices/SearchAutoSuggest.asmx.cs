﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using LibraryCore.DAL;
using SettingsLibrary;
using System.Web.Services;

namespace NCMSMobileAPP.WebServices
{
    /// <summary>
    /// Summary description for SearchAutoSuggest
    /// </summary>
    [WebService(Namespace = "")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    //[System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class SearchAutoSuggest : System.Web.Services.WebService
    {

        //[WebMethod]
        public SearchAutoSuggest()
        {            
        }
        
        [WebMethod(EnableSession=true)]
        public string[] GetSearchgDetails(string prefixText,string contextKey,int count)
       {
            SqlConnection connection = new SqlConnection();
            SqlCommand listCommand = new SqlCommand();
            SqlDataReader listReader;
            DataTable dt = new DataTable();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["NCMSConStr"].ConnectionString;
            if(contextKey=="1")
            {
                listCommand.CommandText = "SET ROWCOUNT @Count " +
                    " SELECT Distinct(LTRIM(RTRIM(First_Name))) " +
                    " FROM NCMS_UserDetails" +
                    " WHERE LTRIM(RTRIM(First_Name)) LIKE '%'+@prefixText+'%' and IsActive='1'";
                listCommand.Parameters.AddWithValue("@prefixText",prefixText.ToLower());
                listCommand.Parameters.AddWithValue("@Count",count);
                listCommand.CommandType = CommandType.Text;
                listCommand.Connection = connection;
            }
            else if (contextKey == "2")
            {
                listCommand.CommandText = "SET ROWCOUNT @Count " +
                    " SELECT Distinct(LTRIM(RTRIM(Second_Name)))" +
                    " FROM NCMS_UserDetails" +
                    " WHERE Second_Name LIKE '%'+@prefixText+'%' and IsActive='1'";
                listCommand.Parameters.AddWithValue("@prefixText", prefixText.ToLower());
                listCommand.Parameters.AddWithValue("@Count", count);
                listCommand.CommandType = CommandType.Text;
                listCommand.Connection = connection;
            }
            else if (contextKey == "3")
            {
                listCommand.CommandText = "SET ROWCOUNT @Count " +
                    " SELECT Distinct(LTRIM(RTRIM(Third_Name)))" +
                    " FROM NCMS_UserDetails" +
                    " WHERE Third_Name LIKE '%'+@prefixText+'%' and IsActive='1'";
                listCommand.Parameters.AddWithValue("@prefixText", prefixText.ToLower());
                listCommand.Parameters.AddWithValue("@Count", count);
                listCommand.CommandType = CommandType.Text;
                listCommand.Connection = connection;
            }
            else if (contextKey == "4")
            {
                listCommand.CommandText = "SET ROWCOUNT @Count " +
                    " SELECT Distinct(LTRIM(RTRIM(Last_Name)))" +
                    " FROM NCMS_UserDetails" +
                    " WHERE Last_Name LIKE '%'+@prefixText+'%' and IsActive='1'";
                listCommand.Parameters.AddWithValue("@prefixText", prefixText.ToLower());
                listCommand.Parameters.AddWithValue("@Count", count);
                listCommand.CommandType = CommandType.Text;
                listCommand.Connection = connection;
            }
            else if(contextKey=="5")
            {
                listCommand.CommandText = "SET ROWCOUNT @Count " +
                    " SELECT Distinct(LTRIM(RTRIM(MobileNumber)))" +
                    " FROM NCMS_UserDetails" +
                    " WHERE MobileNumber LIKE '%'+@prefixText+'%' and IsActive='1'";
                listCommand.Parameters.AddWithValue("@prefixText",prefixText.ToLower());
                listCommand.Parameters.AddWithValue("@Count",count);
                listCommand.CommandType = CommandType.Text;
                listCommand.Connection = connection;
            }
            else if (contextKey == "6")
            {
                listCommand.CommandText = "SET ROWCOUNT @Count " +
                    " SELECT Distinct(LTRIM(RTRIM(EmailAddress)))" +
                    " FROM NCMS_UserDetails" +
                    " WHERE EmailAddress LIKE '%'+@prefixText+'%' and IsActive='1'";
                listCommand.Parameters.AddWithValue("@prefixText", prefixText.ToLower());
                listCommand.Parameters.AddWithValue("@Count", count);
                listCommand.CommandType = CommandType.Text;
                listCommand.Connection = connection;
            }
            else if (contextKey == "7")
            {
                listCommand.CommandText = "SET ROWCOUNT @Count " +
                    " SELECT Distinct(LTRIM(RTRIM(Gradscientific_qualification)))" +
                    " FROM NCMS_UserDetails" +
                    " WHERE Gradscientific_qualification LIKE '%'+@prefixText+'%' and IsActive='1'";
                listCommand.Parameters.AddWithValue("@prefixText", prefixText.ToLower());
                listCommand.Parameters.AddWithValue("@Count", count);
                listCommand.CommandType = CommandType.Text;
                listCommand.Connection = connection;
            }
            else if (contextKey == "8")
            {
                listCommand.CommandText = "SET ROWCOUNT @Count " +
                    " SELECT Distinct(LTRIM(RTRIM(Degree_Name)))" +
                    " FROM Last_degree" +
                    " WHERE Degree_Name LIKE '%'+@prefixText+'%' and IsActive='1'";
                listCommand.Parameters.AddWithValue("@prefixText", prefixText.ToLower());
                listCommand.Parameters.AddWithValue("@Count", count);
                listCommand.CommandType = CommandType.Text;
                listCommand.Connection = connection;
            }
            else if (contextKey == "9")
            {
                listCommand.CommandText = "SET ROWCOUNT @Count " +
                    " SELECT Distinct(LTRIM(RTRIM(scientific_qualification)))" +
                    " FROM NCMS_UserDetails" +
                    " WHERE scientific_qualification LIKE '%'+@prefixText+'%' and IsActive='1'";
                listCommand.Parameters.AddWithValue("@prefixText", prefixText.ToLower());
                listCommand.Parameters.AddWithValue("@Count", count);
                listCommand.CommandType = CommandType.Text;
                listCommand.Connection = connection;
            }
            else if (contextKey == "10")
            {
                listCommand.CommandText = "SET ROWCOUNT @Count " +
                    " SELECT Distinct(LTRIM(RTRIM(GPA)))" +
                    " FROM NCMS_UserDetails" +
                    " WHERE GPA > @prefixText and IsActive='1'";
                listCommand.Parameters.AddWithValue("@prefixText", prefixText.ToLower());
                listCommand.Parameters.AddWithValue("@Count", count);
                listCommand.CommandType = CommandType.Text;
                listCommand.Connection = connection;
            }
            else if (contextKey == "11")
            {
                listCommand.CommandText = "SET ROWCOUNT @Count " +
                    " SELECT Distinct(LTRIM(RTRIM(CityName))) " +
                    " FROM NCMS_UserDetails" +
                    " WHERE LTRIM(RTRIM(CityName)) LIKE '%'+@prefixText+'%' and IsActive='1'";
                listCommand.Parameters.AddWithValue("@prefixText", prefixText.ToLower());
                listCommand.Parameters.AddWithValue("@Count", count);
                listCommand.CommandType = CommandType.Text;
                listCommand.Connection = connection;
            }
            try
            {
                listCommand.Connection.Open();
                listReader = listCommand.ExecuteReader(CommandBehavior.CloseConnection);
                dt.Load(listReader);
            }
            catch { }
            finally
            {
                dt.Dispose();
                listCommand.Dispose();
                connection.Dispose();
            }
            string[] ReturnValues = new string[dt.Rows.Count];
            System.Text.StringBuilder highlightedText = new System.Text.StringBuilder();
            System.Text.StringBuilder textToHeighlighted = new System.Text.StringBuilder();
            for(int i=0;i<dt.Rows.Count;i++)
                ReturnValues[i]=dt.Rows[i][0].ToString();
            return ReturnValues;
        }
    }
}
