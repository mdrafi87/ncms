﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;


namespace LibraryCore.DAL
{
    public sealed class DAL_User
    {      
        public static DataSet getUserByUserName(CLS_User newCLS_User)
        {
            //return DBEngin.ExecuteDataset(connection, null, "ValidateUser",
            return DBEngine.ExecuteDataset("ValidateUser_New",
            new SqlParameter("@user", newCLS_User.Emp_username),
             new SqlParameter("@pswd", newCLS_User.Emp_Password));
        }
        public static DataSet getUserDetails()
        {
            return DBEngine.ExecuteDataset("Select_NCMSUserDetails");                
        }
        public static DataTable UserDetailsById(int Id)
        {
            return DBEngine.ExecuteDataTable("UserDetailsById",
                new SqlParameter("@Id", Id));
        }
        public static DataSet GetUserDetailsById(string Id)
        {
            return DBEngine.ExecuteDataset("UserDetailsById",
                new SqlParameter("@ID", Id));
        }
        public static DataTable SearchUserDetails()
        {
            return DBEngine.ExecuteDataTable("SearchUserDetails");
        }
        public static DataSet GetSearchText(int SearchType,string SearchText)
        {
            return DBEngine.ExecuteDataset("SearchGrid",
                new SqlParameter("@SearchType", SearchType),
                new SqlParameter("@SearchText",SearchText));
        }
    }
}
